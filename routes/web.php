<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 *Auth Routes
 */

Route::get('/', function () {
    return view('welcome');
});

// examples pages --start--------------------------
Route::get('personal', function () {
    return view('personal/firstpage');
});

Route::get('personal2', function () {
    return 'Hello World';
});

// Auth::routes();
//. authentication routes
Route::group(['middleware'=> ['auth']], function() {
    
    // Route::resource('home', 'HomeController');
    // Route::resource('contacts', 'ContactController');
    // Route::resource('pages', 'PagesController');
    
    /* Start Portfolio */
    Route::get('/portfolio', 'Portfolio@portfolioView');
    Route::get('/portfolioadd', 'Portfolio@portfolioAdd');
    Route::post('/portfolio', 'Portfolio@portfolioCreate');
    Route::get('portfolio/{id}', 'Portfolio@portfolioEdit');
    Route::post('/portfolioupdate/{id}', 'Portfolio@portfolioUpdate');
    Route::post('/portfoliodelete','Portfolio@portfolioDelete');
    /* End Portfolio */

    /*Start Product*/
    Route::get('/product', 'Product@productView');
    Route::get('/productadd', 'Product@productAdd');
    Route::post('/product', 'Product@productCreate');
    Route::get('product/{id}', 'Product@productEdit');
    Route::post('/productupdate/{id}', 'Product@productUpdate');
    Route::post('/productdelete','Product@productDelete');    
    /*End Product*/

    /*Start Case Study*/
    Route::get('/casestudy', 'CaseStudy@casestudyView');
    Route::get('/casestudyadd', 'CaseStudy@casestudyAdd');
    Route::post('/casestudy', 'CaseStudy@casestudyCreate');
    Route::get('/casestudy/{id}', 'CaseStudy@casestudyEdit');
    Route::post('/casestudyupdate/{id}', 'CaseStudy@casestudyUpdate');
    Route::post('/casestudydelete','CaseStudy@casestudyDelete');
    /*End Case Study*/

    /*Start About US*/
    Route::get('/aboutus', 'AboutUs@aboutusView');
    Route::get('/aboutusadd', 'AboutUs@aboutusAdd');
    Route::post('/aboutus', 'AboutUs@aboutusCreate');
    Route::get('/aboutus/{id}', 'AboutUs@aboutusEdit');
    Route::post('/aboutusupdate/{id}', 'AboutUs@aboutusUpdate');
    Route::post('/aboutusdelete','AboutUs@aboutusDelete');
    /*End About Us*/

    /*Start Terms And Condition*/
    Route::get('/termscondition', 'TermsAndCondition@termsconditionView');
    Route::get('/termsconditionadd', 'TermsAndCondition@termsconditionAdd');
    Route::post('/termscondition', 'TermsAndCondition@termsconditionCreate');
    Route::get('/termscondition/{id}', 'TermsAndCondition@termsconditionEdit');
    Route::post('/termsconditionupdate/{id}', 'TermsAndCondition@termsconditionUpdate');
    Route::post('/termsconditiondelete','TermsAndCondition@termsconditionDelete');
    /*End Terms And Condition*/

    /*Start Privacy Policy*/
    Route::get('/privacypolicy', 'PrivacyPolicy@privacypolicyView');
    Route::get('/privacypolicyadd', 'PrivacyPolicy@privacypolicyAdd');
    Route::post('/privacypolicy', 'PrivacyPolicy@privacypolicyCreate');
    Route::get('/privacypolicy/{id}', 'PrivacyPolicy@privacypolicyEdit');
    Route::post('/privacypolicyupdate/{id}', 'PrivacyPolicy@privacypolicyUpdate');
    Route::post('/privacypolicydelete','PrivacyPolicy@privacypolicyDelete');
    /*End Privacy Policy*/ 

    /*Start Cookie Policy*/
    Route::get('/cookiepolicy', 'CookiePolicyController@cookiepolicyView');
    Route::get('/cookiepolicyadd', 'CookiePolicyController@cookiepolicyAdd');
    Route::post('/cookiepolicy', 'CookiePolicyController@cookiepolicyCreate');
    Route::get('/cookiepolicy/{id}', 'CookiePolicyController@cookiepolicyEdit');
    Route::post('/cookiepolicyupdate/{id}', 'CookiePolicyController@cookiepolicyUpdate');
    Route::post('/cookiepolicydelete','CookiePolicyController@cookiepolicyDelete');
    /*End Cookie Policy*/ 

    /*Start Social Link*/
    Route::get('/sociallink', 'SocialLink@sociallinkView');
    Route::get('/sociallinkadd', 'SocialLink@sociallinkAdd');
    Route::post('/sociallink', 'SocialLink@sociallinkCreate');
    Route::post('/sociallinkdelete','SocialLink@sociallinkDelete');
    /*End Social Link*/

    /*Start Contact us*/
    Route::get('/contactus', 'Contactus@contactusView');
    Route::get('/contactusadd', 'Contactus@contactusAdd');
    Route::post('/contactus', 'Contactus@contactusCreate');
    Route::post('/contactusdelete','Contactus@contactusDelete');
    /*End Contact us*/

    /*Start Services*/
    Route::get('/services', 'ServicesController@servicesView');
    Route::get('/servicesadd', 'ServicesController@servicesAdd');
    Route::post('/services', 'ServicesController@servicesCreate');
    Route::get('/services/{id}', 'ServicesController@servicesEdit');
    Route::post('/servicesupdate/{id}', 'ServicesController@servicesUpdate');
    Route::post('/servicesdelete','ServicesController@servicesDelete');
    /*End Services*/    
     
    /*Start Blogs*/
    Route::get('/blogs', 'BlogController@blogView');
    Route::get('/blogsadd', 'BlogController@blogAdd');   
    Route::post('/blogs', 'BlogController@blogCreate'); 
    Route::get('/blogs/{id}', 'BlogController@blogEdit');
    Route::post('/blogsupdate/{id}', 'BlogController@blogUpdate');
    Route::post('/blogsdelete','BlogController@blogDelete');
    /*End Blogs*/ 

    /*Start Settings*/
    Route::get('/setting', 'SettingController@settingView');
    Route::get('/settingadd', 'SettingController@settingAdd');
    Route::post('/setting', 'SettingController@settingCreate');
    Route::get('setting/{id}', 'SettingController@settingEdit');
    Route::post('/settingupdate/{id}', 'SettingController@settingUpdate');
    Route::post('/settingdelete','SettingController@settingDelete');
    /*End Settings*/

    /*Start Lead Generation*/
    Route::get('/leadgenerate', 'LeadGenerateController@leadgenerateView');
    Route::get('/leadgenerateadd', 'LeadGenerateController@leadgenerateAdd');
    Route::post('/leadgenerate', 'LeadGenerateController@leadgenerateCreate');
    Route::get('leadgenerate/{id}', 'LeadGenerateController@leadgenerateEdit');
    Route::post('/leadgenerateupdate/{id}', 'LeadGenerateController@leadgenerateUpdate');
    Route::post('/leadgeneratedelete','LeadGenerateController@leadgenerateDelete');   
    /*End Lead Generation*/

    /* Start Servicepage */
    Route::get('/servicepage', 'ServicepageController@servicepageView');
    Route::get('/servicepageadd', 'ServicepageController@servicepageAdd');
    Route::post('/servicepage', 'ServicepageController@servicepageCreate');
    Route::get('servicepage/{id}', 'ServicepageController@servicepageEdit');
    Route::post('/servicepageupdate/{id}', 'ServicepageController@servicepageUpdate');
    Route::post('/servicepagedelete','ServicepageController@servicepageDelete');
    /* End Servicepage */

    /* Start Menu */
    Route::get('/menu', 'MenuController@index');
    Route::get('/menuadd', 'MenuController@create');
    Route::post('/menu', 'MenuController@store');
    Route::get('menu/{id}', 'MenuController@edit');
    Route::post('/menuupdate/{id}', 'MenuController@update');
    Route::post('/menudelete','MenuController@destroy');
    /* End Menu */

    /*Start Leads*/
    Route::get('/leads', 'LeadsController@getLeadsData');   
    /*End Leads*/

    /*Start Ico Services*/
    Route::get('/ico-service/list', 'IcoserviceController@index');
    /*End Ico Services*/

    /* Start Email Template */
    Route::get('/emailtemplate/list', 'EmailtemplateController@index');
    Route::get('emailtemplate/edit/{id}', 'EmailtemplateController@edit');
    Route::post('/emailtemplate/update/{id}', 'EmailtemplateController@update');
    Route::post('/emailtemplate/delete','EmailtemplateController@destroy');
    /* End Email Template */


/* revinfotech additional modules start here */
    
    /* Start Enviornment Settings */
    Route::get('envsettings/form','EnvsettingsController@createForm')->name('envsettings.createform');
    Route::post('envsettings/','EnvsettingsController@addForm')->name('envsettings.addForm');
    Route::get('envsettings/index','EnvsettingsController@index')->name('envsettings.index');
    Route::get('envsettings/edit/{id}', 'EnvsettingsController@edit')->name('envsettings.edit');
    Route::post('envsettings/update/{id}', 'EnvsettingsController@update')->name('envsettings.update');
    Route::post('envsettings/destroy/{id}', 'EnvsettingsController@destroy')->name('envsettings.destroy');

    Route::get('envsettings/varaccess','EnvsettingsController@varaccess')->name('envsettings.varaccess');
    /* End Enviornment Settings */

    /*Start Frontend Industries*/
    Route::get('/industries-index', 'IndustriesController@industriesIndex');
    Route::get('/industriesadd', 'IndustriesController@industriesAdd');   
    Route::post('/industriescreate', 'IndustriesController@industriesCreate'); 
    Route::get('/industries/{id}', 'IndustriesController@industriesEdit');
    Route::post('/industriesupdate/{id}', 'IndustriesController@industriesUpdate');
    Route::post('/industriesdelete','IndustriesController@industriesDelete');
    /*End Frontend Industries*/ 
    
    /*Start Frontend Pages module*/
    Route::get('/frontendpages-index', 'FrontendPagesController@frontendpages_Index');
    Route::get('/frontendpagesadd', 'FrontendPagesController@frontendpagesAdd');   
    Route::post('/frontendpagescreate', 'FrontendPagesController@frontendpagesCreate'); 
    Route::get('/frontendpages/{id}', 'FrontendPagesController@frontendpagesEdit');
    Route::post('/frontendpagesupdate/{id}', 'FrontendPagesController@frontendpagesUpdate');
    Route::post('/frontendpagesdelete','FrontendPagesController@frontendpagesDelete');
    /*End Frontend Pages module*/ 

    // /*Start Frontend Sections module*/
    // Route::get('/sections/index', 'CustomSectionsController@section_Index')->name('section.Index');
    // Route::get('/sections/add', 'CustomSectionsController@section_Add')->name('section.add');   
    // Route::post('/sections/create', 'CustomSectionsController@section_Create')->name('section.Create'); 
    // Route::get('/sections/{id}', 'CustomSectionsController@section_Edit');
    // Route::post('/sectionsupdate/{id}', 'CustomSectionsController@section_Update');
    // Route::post('/sectionsdelete','CustomSectionsController@section_Delete');
    // /*End Frontend Pages module*/ 

    // /*Start Frontend Fields module*/
    // Route::get('/fields/index', 'CustomFieldsController@fields_Index')->name('fields.Index');
    // Route::get('/fields/add', 'CustomFieldsController@fields_Add')->name('fields.add');   
    // Route::post('/fields/create', 'CustomFieldsController@fields_Create')->name('fields.Create'); 
    // Route::get('/fields/{id}', 'CustomFieldsController@fields_Edit');
    // Route::post('/fieldsupdate/{id}', 'CustomFieldsController@fields_Update');
    // Route::post('/fieldsdelete','CustomFieldsController@fields_Delete');
    // Route::get('/myform/{id}','CustomFieldsController@myformAjax');
    // Route::get('/myformsection/{id}','CustomFieldsController@myformAjaxSection');
    // /*End Frontend Fields module*/ 

    /*Start Frontend Template module*/
    Route::get('templates', 'TemplatesController@index')->name('templates.index');
    Route::post('templates', 'TemplatesController@store')->name('templates.store');   
    Route::get('templates/create', 'TemplatesController@create')->name('templates.create'); 
    Route::get('templates/{template}', 'TemplatesController@show')->name('templates.show');
    Route::post('templates/{template}/update', 'TemplatesController@update')->name('templates.update');
    Route::post('templates/{template}/delete','TemplatesController@destroy')->name('templates.destroy');
    Route::get('templates/{template}/edit','TemplatesController@edit')->name('templates.edit');
    Route::get('templates/{template}/developeredit','TemplatesController@developeredit')->name('templates.developeredit');
    Route::get('templates/{template}/show','TemplatesController@show')->name('templates.show');
    /*End Frontend Fields module*/ 

    /*Start Frontend Identifire module*/
    Route::get('identifiers', 'IdentifierController@index')->name('identifiers.index');
    Route::get('identifiers/templatewise', 'IdentifierController@templatewiseIndex')->name('identifiers.templatewise_index');
    Route::post('identifiers', 'IdentifierController@store')->name('identifiers.store');   
    Route::get('identifiers/create', 'IdentifierController@create')->name('identifiers.create'); 
    Route::get('identifiers/{identifier}', 'IdentifierController@show')->name('identifiers.show');
    Route::post('identifiers/{identifier}/update', 'IdentifierController@update')->name('identifiers.update');
    Route::post('identifiers/{identifier}/templatewise-update', 'IdentifierController@templatewiseUpdate')->name('identifiers.templatewiseUpdate');
    Route::post('identifiers/{identifier}/delete','IdentifierController@destroy')->name('identifiers.destroy');
    Route::get('identifiers/{identifier}/edit','IdentifierController@edit')->name('identifiers.edit');
    Route::get('identifiers/{identifier}/templatewise-edit','IdentifierController@templatewiseEdit')->name('identifiers.templatewise_edit');
    Route::get('identifiers/{identifier}/show','IdentifierController@show')->name('identifiers.show');
    Route::get('identifiers/{identifier_id}/templatewise-show','IdentifierController@templatewiseShow')->name('identifiers.templatewiseShow');
    Route::post('dynamic-field/insert', 'IdentifierController@insert')->name('dynamic-field.insert');
    /*End Frontend Fields module*/ 

    /*Start Frontend Job Opening module*/
    Route::get('job-openings', 'JobOpeningsController@index')->name('jobopenings.index');
    Route::post('job-openings', 'JobOpeningsController@store')->name('jobopenings.store');   
    Route::get('job-openings/create', 'JobOpeningsController@create')->name('jobopenings.create'); 
    Route::get('identifiers/{identifier}', 'JobOpeningsController@show')->name('jobopenings.show');
    Route::post('job-openings/{id}/update', 'JobOpeningsController@update')->name('jobopenings.update');
    Route::post('job-openings/{id}/delete','JobOpeningsController@destroy')->name('jobopenings.destroy');
    Route::get('job-openings/{id}/edit','JobOpeningsController@edit')->name('jobopenings.edit');
    // Route::get('identifiers/{identifier}/show','JobOpeningsController@show')->name('jobopenings.show');
    // Route::post('dynamic-field/insert', 'JobOpeningsController@insert')->name('dynamic-field.insert');
    /*End Frontend Job Opening module*/ 
   

    /*Start Frontend Contactrequest module*/
    Route::get('contact-list', 'ContactRequestController@index')->name('contactlist.index');
    // Route::post('identifiers', 'ContactRequestController@store')->name('identifiers.store');   
    // Route::get('identifiers/create', 'ContactRequestController@create')->name('identifiers.create'); 
    // Route::get('identifiers/{identifier}', 'ContactRequestController@show')->name('identifiers.show');
    // Route::post('identifiers/{identifier}/update', 'ContactRequestController@update')->name('identifiers.update');
    // Route::post('identifiers/{identifier}/delete','ContactRequestController@destroy')->name('identifiers.destroy');
    // Route::get('identifiers/{identifier}/edit','ContactRequestController@edit')->name('identifiers.edit');
    // Route::get('identifiers/{identifier}/show','ContactRequestController@show')->name('identifiers.show');
    // Route::post('dynamic-field/insert', 'ContactRequestController@insert')->name('dynamic-field.insert');
    /*End Frontend Fields module*/ 


});

/* Start Frontend Routes */
Route::get('/', 'Frontend\HomePageController@getAllData');
Route::get('/front-industries/{page}', 'Frontend\HomePageController@Industries');
Route::get('/front-services/services', 'Frontend\HomePageController@Services');
Route::get('/front-aboutus/aboutus', 'Frontend\HomePageController@Aboutus');
Route::get('/front-aboutus/leadership', 'Frontend\HomePageController@Leadership');
Route::get('/front-blogs/blog', 'Frontend\HomePageController@Blog');
Route::get('/front-blogs/blogdetail', 'Frontend\HomePageController@BlogDetails');
Route::get('/front-casestudy/casestudy', 'Frontend\HomePageController@CaseStudy');
Route::get('/front-news/news', 'Frontend\HomePageController@News');
Route::get('/front-news/newsdetail', 'Frontend\HomePageController@NewsDetails');
Route::get('/front-casestudy/casestudydetails', 'Frontend\HomePageController@CaseStudyDetails');
Route::get('/front-career/career', 'Frontend\HomePageController@Career');
Route::get('/front-career/careerform', 'Frontend\HomePageController@CareerForm');
Route::get('/front-career/jobOpenings', 'Frontend\HomePageController@JobOpenings');
Route::get('/front-career/jobOpeningsExplore/{id}', 'Frontend\HomePageController@JobOpeningsExplore');
Route::get('/front-career/jobOpeningsExploreApply/{id}', 'Frontend\HomePageController@JobOpeningsExploreApply');
Route::post('/front-career/careerformstore', 'Frontend\HomePageController@careerFormStore');
Route::get('/front-contactus/contactus', 'Frontend\HomePageController@contactus');
Route::post('/front-contactus/contact-request', 'Frontend\HomePageController@getContactRequest');
Route::get('/front-gallery/gallery', 'Frontend\HomePageController@getGallery');
Route::get('/front-tokenlauncher/tokenlauncher', 'Frontend\HomePageController@tokenlauncher');

//gallery routes-----------------------------------------------------
Route::get('image-gallery', 'ImageGalleryController@index');
Route::post('image-gallery', 'ImageGalleryController@upload');
Route::delete('image-gallery/{id}', 'ImageGalleryController@destroy');

//test routes-------------------
Route::get('/upload-form', 'Frontend\HomePageController@uploadForm');
Route::post('/upload-document', 'Frontend\HomePageController@uploadDocument');
Route::post('/upload-document', 'Frontend\HomePageController@uploadDocument');
Route::get('/zohotestform', 'Frontend\HomePageController@zohotestform');
Route::get('/syncrobit', function(){
    return view('/frontend/syncbit');
});

/* End Fontend Routes */

Route::get('personal2', function () {
    return 'Hello World';
});
// examples pages --end----------------------------------

Auth::routes();

//custom register forms
Route::post('register' ,'Auth\RegisterController@custregister')->name('custregister');
Route::get('register' ,'Auth\RegisterController@custshowregistrationForm')->name('custregister');

//custom login forms
Route::post('login' ,'Auth\LoginController@custlogin')->name('custlogin');
Route::get('login' ,'Auth\LoginController@custshowLoginForm')->name('custlogin');
Route::post('logout' ,'Auth\LoginController@logout')->name('logout');

//password reset forms
Route::post('password/email' ,'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset' ,'Auth\ResetPasswordController@reset')->name('password.update');
Route::post('password/reset/{token}' ,'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('password/reset' ,'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('password/change' ,'Auth\ForgotPasswordController@changepass')->name('password.change');

Route::group(['middleware'=> ['auth']], function() {
      
    Route::resource('contacts', 'ContactController');
    Route::get('pages', 'PagesController@index')->name('pages');
    Route::get('pages/create', 'PagesController@create')->name('pages.create');
    Route::post('pages/store', 'PagesController@store')->name('pages.store');
    Route::get('pages/{id}/edit', 'PagesController@edit')->name('pages.edit');
    Route::post('pages/{id}/update', 'PagesController@update')->name('pages.update');
    Route::get('pages/{id}/destroy', 'PagesController@destroy')->name('pages.destroy');
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('/changePassword','HomeController@showChangePasswordForm')->name('changePassword');
    Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
});
