<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactusModel extends Model
{
    protected $table = "rev_contactus";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "address", "phone1", "phone2", "email"];

}
