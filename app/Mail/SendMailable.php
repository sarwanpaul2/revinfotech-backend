<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
Use PDF;
use File;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd(File::make($this->data[7]->getClientOriginalExtension()));
        // $pdf = PDF::loadView('mails.mail', $this->data);
        // dd($pdf);
        $user = DB::table('emailtemplates')->where('name', 'career_form_user')->first();
        if(!empty($user))
        {
            return $this->from('info@revolutions.ai')
                        ->subject('Job Applicant')
                        ->markdown('email.name')
                        ->with([
                            'first_name' => $this->data['first_name'],
                            'last_name' => $this->data['last_name'],
                            'email' => $this->data['email'],
                            'contact_num' => $this->data['contact_num'],
                            'qualification' => $this->data['qualification'],
                            'experience' => $this->data['experience'],
                            'position' => $this->data['position'],
                            'user_template' => $user->template,
                            ])->attach($this->data['document']->getRealPath(),
                                                [   'as' => $this->data['document']->getClientOriginalName(),
                                                    'mime' => $this->data['document']->getClientMimeType(),
                                                ]
                        );
        }
        else
        {
            return $this->from('info@revolutions.ai')
                        ->subject('Job Applicant')
                        ->markdown('email.name')
                        ->with([
                            'first_name' => $this->data['first_name'],
                            'last_name' => $this->data['last_name'],
                            'email' => $this->data['email'],
                            'contact_num' => $this->data['contact_num'],
                            'qualification' => $this->data['qualification'],
                            'experience' => $this->data['experience'],
                            'position' => $this->data['position'],
                            'user_template' => '',
                        ])->attachData($this->data['document']->getRealPath(),
                                                [   'as' => $this->data['document']->getClientOriginalName(),
                                                    'mime' => $this->data['document']->getClientMimeType(),
                                                ]
                    );
        }
    }
}
