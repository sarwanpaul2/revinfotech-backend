<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class AdminSendMailable extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
       
        $this->data = $data;
        
      //  $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd('here admin email');
        $user = DB::table('emailtemplates')->where('name', 'career_form_admin')->first();
        if(!empty($user))
        {
            return $this->from('info@revolutions.ai')->subject('Job Applicant')->markdown('email.admin_name')
                        ->with([
                            'first_name' => $this->data['first_name'],
                            'last_name' => $this->data['last_name'],
                            'email' => $this->data['email'],
                            'contact_num' => $this->data['contact_num'],
                            'qualification' => $this->data['qualification'],
                            'experience' => $this->data['experience'],
                            'position' => $this->data['position'],
                            'user_template' => $user->template,
                            ])->attach($this->data['document']->getRealPath(),
                                                [   'as' => $this->data['document']->getClientOriginalName(),
                                                    'mime' => $this->data['document']->getClientMimeType(),
                                                ]
                        );
        }
        else
        {
            return $this->from('info@revolutions.ai')->subject('Contact Us')->markdown('email.admin_name')
                        ->with([
                            'first_name' => $this->data['first_name'],
                            'last_name' => $this->data['last_name'],
                            'email' => $this->data['email'],
                            'contact_num' => $this->data['contact_num'],
                            'qualification' => $this->data['qualification'],
                            'experience' => $this->data['experience'],
                            'position' => $this->data['position'],
                            'user_template' => '',
                            ])->attach($this->data['document']->getRealPath(),
                                                [   'as' => $this->data['document']->getClientOriginalName(),
                                                    'mime' => $this->data['document']->getClientMimeType(),
                                                ]
                        );
        }
    }
}
