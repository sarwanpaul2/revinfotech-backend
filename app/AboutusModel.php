<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutusModel extends Model
{
    protected $table = "rev_aboutus";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description", "image_1", "image_2", "image_3"];
}
