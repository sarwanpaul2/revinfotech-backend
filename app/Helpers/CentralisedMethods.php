<?php

namespace App\Helpers;

use PDO;
use Exception;
use App\CustomSections;
use App\CustomFields;
use App\Identifiers;


class CentralisedMethods 
{
    /*
    Centralised Methods 
     */
    /* getting data from Array table with selected fields */
    public function TableToArray($arrlop,$field1,$field2)
    {
        $arr1 = array();
        $arr2 = array();
        $f_arr = array();
        foreach ($arrlop as $key => $value) 
        {
            $arr1[] = $value->$field1;
        }
        foreach ($arrlop as $key => $value) 
        {
            $arr2[] = $value->$field2;
        }
        $f_arr = array_combine($arr1,$arr2);
        $f_arr[0] = 'Default';
        return $f_arr;
    }

    public function getFieldsData($field_name)
    {
        $section_data = CustomSections::all()->where('field_name',$field_name);  
        // dd($section_data);
        if (($section_data->isNotEmpty())) {
            $id =  $this->TableToArray($section_data, "field_name", "id")[$field_name];
            // dd($id);
            $custom_field_data = CustomFields::where('section_id', $id)->first();
            // dd($custom_field_data);
            return $custom_field_data;
        }
        return null;
    }
    
    public function getIdentifiersData($name,$template_id)
    {
            
            $identifier = Identifiers::where(['name'=> $name,'template_id'=> $template_id])->first();
        // dd($identifier);
            if (!empty($identifier)) {
                return $identifier;
            }else{
                return null;
            }
	}
       
}
