<?php

namespace App\Helpers;

use PDO;
use Exception;

class Db 
{
    /**
     * 
     */
    private $_DNS;
    private $_DB_USERNAME;
    private $_DB_PASSWORD;
    private $_CHARSET;

    CONST OPTIONS = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    /**
     * @var PDO
     */
    private $_pdo;
    private $_setting;

    public function __construct()
    {
        try
        {
            if ($this->_pdo === null) {
                $this->setAttributes();
                $this->_pdo = new PDO($this->_DNS, $this->_DB_USERNAME, $this->_DB_PASSWORD, self::OPTIONS);
            }
        } catch (Exception $e) {
            die($e->getMessage());
        } finally { 	
           return false;
        }
    }

    /**
     * @var $table
     * Return boolean
     */
    private function checkIfTableExists($table)
    {
        try {
            // Try a select statement against the table
            // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
            $result = $this->_pdo->query("SELECT 1 FROM $table LIMIT 1");
        } catch (PDOException $e) {
            // We got an exception == table not found
            return false;
        } finally {
            return isset($result) !== false;
        }
    }

    /**
     * 
     */
    private function setAttributes()
    {
        $this->_DNS = 'mysql:host=' . env('DB_HOST', 'homestead') .';dbname=' . env('DB_DATABASE', 'homestead');
        $this->_DB_USERNAME = env('DB_USERNAME', 'root');
        $this->_DB_PASSWORD = env('DB_PASSWORD', '');
        $this->_CHARSET = 'utf8';
    }

    /**
     * Get the setting from settins table.
     *
     * @return void
     */
    public function getSetting($setting_name)
    {
        try {
            throw_if(!$this->_pdo, new Exception('Unauthorized access'));
            throw_if(!$this->checkIfTableExists('env_settings'), new Exception('Setting Table not found.'));

            $stmt = $this->_pdo->prepare("SELECT * FROM env_settings WHERE setting_name = :setting_name");
            $stmt->execute(['setting_name' => $setting_name]);
            $this->_setting = $stmt->fetch();
        }
        catch(PDOException $e) {
            return null;
        } finally {
            return $this->_setting !== false ? $this->_setting : null;
        }
    }    
}

