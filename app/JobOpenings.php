<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOpenings extends Model
{
    protected $table = "job_openings";
    protected $fillable = [
        
         "position",
         "experience",
          "location",
           "sharp_skills",
            "skills",
             "remark",
              "job_desc",
              "date"
               ];
}
