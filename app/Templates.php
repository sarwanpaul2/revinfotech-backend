<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Templates extends Model
{
    const TYPE_PAGE = 'page';
    const TYPE_EMAIL = 'email';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rev_templates';
    protected $fillable = ["layout", "type",'external_css'];  
    

    
}

