<?php

namespace App\Console\Commands;

use DB;
use File;
use Exception;
use App\cities;
use App\states;
use App\Pages;
use App\PageContent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;

class CreatePages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:pages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create All cities pages';

    /**
     * @var
     */
    private $accumulator;

    /**
     * @var object
     */
    private $queue_instance;

    /**
     * 
     */
    private $limit = 10000;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Scheduler run command');
        DB::beginTransaction();
        try{
            throw_if(!$this->setQueueInstance(), new Exception('OOPS! No title to process.'));
            if($this->setAccumulator() && !$this->accumulator) $this->createTable();
            //$this->setAccumulator();
            Log::info('create table = '.$this->createTable());
            Log::info('set accumu fun = '.$this->setAccumulator());
            Log::info('queue_instance = '.$this->queue_instance);
            Log::info('accumulator = '.$this->accumulator);


            $content = json_decode($this->queue_instance->content);
            Log::info("id = ".$this->queue_instance['id']);


            $cities = cities::with('state')->limit($this->limit)->offset($this->accumulator)->get();
            $this->accumulator += $cities->count();
            $zI = 1;
            if(!$cities->count()) throw new Exception('Job already completed for title:: ' .  $this->queue_instance->title);
            $oldState='';
            foreach($cities as $key => $city){
                if($city->state['name']){
                    $oldState = $city->state['name'];
                }else{
                    $oldState = 'services';
                }
                $oldCity = $city['name'];
                $state = str_replace(' ', '-', $oldState);
                $city = str_replace(' ', '-', $oldCity);
                $url = strtolower(str_replace(array('[city]','[state]'), array($city,$state), $content->url));
               
                $title = strtolower($this->queue_instance->title);

                DB::table(str_replace(' ', '_', env('PAGE_SLUG_TABLE_PREFIX', 'page_slug_') . $title))->insert([
                    'page_content_id' => $this->queue_instance['id'],
                    'url' => $url,
                    'title' => $content->title,
                    'meta_title' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState),$content->meta_title)),
                    'meta_description' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState),$content->meta_description)),
                    'keyword' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState), $content->keyword)),
                    'desc1' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState), $content->desc1)),
                    'desc2' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState), $content->desc2)),
                    'desc3' => strtolower(str_replace(array('[city]','[state]'),array($oldCity,$oldState), $content->desc3)),
                    'image1' => $this->queue_instance->image1,
                    'image2' => $this->queue_instance->image2,
                    'image3' => $this->queue_instance->image3,
                    'city' => $oldCity,
                    'state' => $oldState
                ]);
                Log::info('suceesfully committed'.$zI."<br>");
                $zI++;
            }
            $this->queue_instance->update([
                'accumlator' => $this->accumulator,
                'job_status' => ($cities->count()%$this->limit > 1) ? 1 : 0
            ]);
            if($this->queue_instance->job_status) {
                Log::info("in job status");
                $this->generateSiteMap();
            } 
            DB::commit();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
        }
    }

    private function setAccumulator()
    {
        $this->accumulator = $this->queue_instance->accumlator;
        return true;
    }

    private function setQueueInstance()
    {
        if(($instance = PageContent::where(['job_status' => false])->first())) {
            $this->queue_instance = $instance;
            return true;
        }
        return false;
    }

    private function generateSiteMap()
    {
        $title = str_replace(' ', '-', strtolower($this->queue_instance->title));
        $content = json_decode($this->queue_instance->content);

        $file = base_path('/public/seo/sitemaps/') . $title . '.xml';

        $xml_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $xml_data .="<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n
                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n
                xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n";

        File::append($file, $xml_data);

        $cities = cities::with('state')->get();
        $baseUrl = config('baseurl.baseurl');
        $oldState = "";
        foreach($cities as $key => $city) {
            if($city->state['name']){
                $oldState = $city->state['name'];
            }else{
                $oldState = 'services';
            }
            $oldCity = $city['name'];
            $state = str_replace(' ', '-', $oldState);
            $city = str_replace(' ', '-', $oldCity);
            $url = strtolower(str_replace(array('[city]','[state]'), array($city,$state), $content->url));
           
            $url = strtolower(str_replace(' ', '-', $baseUrl.$url));
            $xml_data = str_pad("",4)."<url>\n";
            $xml_data .=str_pad("",8)."<loc>".$url."</loc>\n";
            $xml_data .=str_pad("",8)."<lastmod>".$this->queue_instance->updated_at."</lastmod>\n";
            $xml_data .=str_pad("",8)."<priority>1.00</priority>\n";
            $xml_data .=str_pad("",4)."</url>\n";
            File::append($file, $xml_data);
        }
        $xml_data = '</urlset>';
        File::append($file, $xml_data);
        return true;
    }


    private function createTable()
    {   
        $title = strtolower($this->queue_instance->title);

        if(Schema::hasTable(str_replace(' ', '_', env('PAGE_SLUG_TABLE_PREFIX', 'page_slug_') . $title))) {
            echo 'Table for slug:: ' . $title . ' already exists. Proceeding...';
            return true;
        }
           
        Schema::create(str_replace(' ', '_', env('PAGE_SLUG_TABLE_PREFIX', 'page_slug_') . $title), function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_content_id')->nullable();
            $table->string('url')->collation('utf8_unicode_ci');
            $table->string('title')->nullable();
            $table->string('meta_title')->nullable();
            $table->longtext('meta_description')->nullable();
            $table->longtext('keyword')->nullable();
            $table->longtext('desc1')->nullable();;
            $table->longtext('desc2')->nullable();;
            $table->longtext('desc3')->nullable();;
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();;
            $table->string('image3')->nullable();;
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
        });

        echo 'New table has been created for:: ' . $this->queue_instance->slug;
        return true;
    }
}
