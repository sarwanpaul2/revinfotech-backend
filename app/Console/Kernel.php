<?php

namespace App\Console;

use App\Console\Commands\CreatePages;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreatePages::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(CreatePages::class)->everyMinute();
        // $schedule->call(function(){
        //     $states = DB::select('select * from states where country_id = 101 ORDER BY id ASC' );
        //     $totalstates = count($states);
        //     // $users = DB::select('select * from cities where state_id = 1');
        //     // dd($totalstates);die;
            
        //     foreach($states as $state){
        //             $cities = DB::select('select * from cities where state_id = '.$state->id.' ORDER BY id ASC');
        //             foreach ($cities as $city) {
        //                 $pages = DB::select('select * from pages where city = "'.$city->name.'" ORDER BY city'); 
        //                 if(Count($pages) >= 1){
        //                     //
        //                 }else{
        //                     // var_dump($city->name);die;
        //                     DB::table('pages')->insert(
        //                             ['title' => $city->name,'meta_title' => 'meta', 'keyword' => 0 , 'desc1' => 'hello1' , 'desc2' => 'hello2' , 'desc3' => 'hello3',
        //                             'image1' => 'img1' , 'image2' => 'img2' , 'image3' => 'img3', 'city' => $city->name , 'state' => $state->name ,'slug1' => 'slug1' ,
        //                              'slug2' => 'slug2', 'created_at' => date('y-m-d h:i:s'), 'updated_at' => Null]
        //                         );
        //                 }
        //             }
        //     }
        // })->everyMinute();
        
        // $schedule->call(function(){
        //     DB::table('pages')->insert(
        //         ['title' => 'title1','meta_title' => 'meta', 'keyword' => 0 , 'desc1' => 'hello1' , 'desc2' => 'hello2' , 'desc3' => 'hello3',
        //         'image1' => 'img1' , 'image2' => 'img2' , 'image3' => 'img3', 'city' => 'city' , 'state' => 'state' , 
        //         'slug1' => 'slug1' , 'slug2' => 'slug2']
        //     );
        // })->everyMinute();
                
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
