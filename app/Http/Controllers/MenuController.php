<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use App\FrontendPages;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menuData = Menu::orderBy('id','DESC')->get();
        $pages_data = FrontendPages::get(); 
        $pages_data = $this->TableToArray($pages_data,"id","slug");
        
        $menu_list = $this->TableToArray($menuData,"id","menu_name");
        // dd($pages_data);
        return view('/menu/menulist')->with(['data' => $menuData,'menu_list'=>$menu_list,'pages_data' => $pages_data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menuData = Menu::all();
        $pages_data = FrontendPages::where('status',1)->get(); 
        return view('/menu/menu')->with(['menu_list' => $menuData, 'pages_data' => $pages_data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu([
            "menu_name" => $request->menu_name, "menu_alias" => $request->menu_alias, "parent_id" => $request->parent_id, "status" => $request->status
        ]);
            // dd($menu);
        $menu->save();
        return redirect('/menu')->with('success', 'Menu Saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $menuEditData = Menu::find($id);
        $menuData = Menu::all();
        $pages_data = FrontendPages::where('status',1)->get(); 
        return view('/menu/menuedit')->with(['data' => $menuEditData,'menu_list' => $menuData,'pages_data' => $pages_data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menuUpdate = Menu::find($id);
        $menuUpdate->menu_name = $request->menu_name;
        $menuUpdate->menu_alias = $request->menu_alias;
        $menuUpdate->parent_id = $request->parent_id;
        $menuUpdate->status = $request->status;

        $menuUpdate->update();
        return redirect('/menu')->with('success', 'Menu Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $menuDelete = Menu::find($request->delid);
        $menuDelete->delete();
        return redirect('/menu')->with('delete', 'Menu Delete!');
    }


    public function TableToArray($arrlop,$field1,$field2)
    {
        $arr1 = array();
        $arr2 = array();
        $f_arr = array();
        foreach ($arrlop as $key => $value) 
        {
            $arr1[] = $value->$field1;
        }
        foreach ($arrlop as $key => $value) 
        {
            $arr2[] = $value->$field2;
        }
        $f_arr = array_combine($arr1,$arr2);
        $f_arr[0] = 'Default';
        return $f_arr;
    }
}
