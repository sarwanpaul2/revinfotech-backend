<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageGallery;
use Illuminate\Support\Facades\Storage;

class ImageGalleryController extends Controller
{
        /**

     * Listing Of images gallery

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
    	$images = ImageGallery::orderBy('id','DESC')->paginate(10);
    	return view('gallery/image-gallery',compact('images'));
    }

    /**
     * Upload image function
     *
     * @return \Illuminate\Http\Response
     */
     
    /** function for uploading image in page create action through function */
   
    public function upload(Request $request)
    {
        $images_array = $request->image;
        
    $this->validate($request, [
        // 'title' => 'required',
        'image' => 'required',
    ]);
    foreach($images_array as $value){
        $allowedfileExtension=['pdf','jpg','png','docx','jpeg'];
        $filename = $value->getClientOriginalName();
        $extension = $value->getClientOriginalExtension();
        $filesize = $value->getClientSize();
        $extcheck=in_array($extension,$allowedfileExtension);
        $sizecheck = ($filesize <= 2000000) ? true : false;
    // dd($value);
    if($sizecheck == false){ 
        $errors[] = 'Error in size '.$value->getClientSize();
    }
    
    if ($extcheck) {
        // dd(file_get_contents($value));
            $file = time().'.'.$value->getClientOriginalName();
            $filePath = 'images/' . $file;
            $resp =Storage::disk('s3')->put($filePath, file_get_contents($value));
            $input['image'] = $file;
        // $input['image'] = time().'.'.$value->getClientOriginalName();
        // $value->move(('images'), $input['image']);
            $input['title'] = 'gallery-image'.'.'.time();
            ImageGallery::create($input);
        // dd($filename);
        // echo $input['image']."<br>";
    }else{
        $errors[] = 'Error in extension '.$value->getClientOriginalName();
    }
}
if(isset($errors)){
    return back()
    ->with('failure',implode(" ",$errors)); 
}
// dd($result);
    	// $this->validate($request, [
    		// 'title' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);
            // dd($request->image->getClientOriginalName());
        // $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        // $request->image->move(public_path('images'), $input['image']);
        // $input['title'] = $request->title;
        // ImageGallery::create($input);
    	return back()
    		->with('success','Image Uploaded successfully.');
    }

    /**
     * Remove Image function
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery_image_delete=ImageGallery::find($id);
        $resp=Storage::disk('s3')->delete('images/'.$gallery_image_delete->image);
        $gallery_image_delete->delete();
    	return back()
    		->with('success','Image removed successfully.');	
    }
}
