<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactusModel;

class Contactus extends Controller
{
    public function contactusView(){
        $contactusData = ContactusModel::all();
    	return view('/contactus/contactuslist')->with(['data' => $contactusData]);
    }
    public function contactusAdd(){
        return view('/contactus/contactus');
    }
    public function contactusCreate(Request $request){
    	$contactus = new ContactusModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "address" => $request->address, "phone1" => $request->phone1, "phone2" =>$request->phone2, "email" => $request->email
        ]);

        $contactus->save();
        return redirect('/contactus')->with('success', 'Contactus Saved!');
    }

    public function contactusDelete(Request $request){
        $contactusDelete = ContactusModel::find($request->delid);
        $contactusDelete->delete();
        return redirect('/contactus')->with('delete', 'Contactus Delete!');
    }
}
