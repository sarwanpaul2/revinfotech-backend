<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialLinkModel;

class SocialLink extends Controller
{
    public function sociallinkView(){

        $socialLinkData = SocialLinkModel::all();
    	return view('/sociallink/sociallinklist')->with(['data' => $socialLinkData]);
        //return view('/portfolio/portfolio');
    }

    public function sociallinkAdd(){
        return view('/sociallink/sociallink');
    }

    public function sociallinkCreate(Request $request){	
    	$sociallink = new SocialLinkModel(["facebook_url" => $request->facebookurl, "twitter_url" => $request->twitterurl, "youtube_url" => $request->youtubeurl, "instagram_url" => $request->instagramurl, "google_url" => $request->googleurl, "linkedin_url" => $request->linkedinurl, "pinterest_url" => $request->pinteresturl]);
        
        $sociallink->save();
        return redirect('/sociallink')->with('success', 'Social Link Saved!');
    }

    public function sociallinkDelete(Request $request){
        $sociallinkDelete = SocialLinkModel::find($request->delid);
        $sociallinkDelete->delete();
        return redirect('/sociallink')->with('delete', 'Social Link Delete!');
    }
}
