<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeadGenerate;
use Illuminate\Support\Facades\Storage;

class LeadGenerateController extends Controller
{
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$leadgenerate,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $leadgenerate->$imagename = $name;

    }

    public function leadgenerateView(){
        $leadgenerateData = LeadGenerate::all();
    	return view('/leadgenerate/leadgeneratelist')->with(['data' => $leadgenerateData]);
    }

    public function leadgenerateAdd(){
        return view('/leadgenerate/leadgenerate');
    }

    public function leadgenerateCreate(Request $request){
    	
    	$leadgenerate = new LeadGenerate([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description_1" => $request->description_1, "descritpion_2" => $request->description_2, "description_3" => $request->descritpion_3, "url" => $request->url
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1',$leadgenerate,$request);
	    }else{
            $leadgenerate->image_1 = NULL;
        }

        if($request->hasFile('image_2')){
            $this::createImage('image_2',$leadgenerate,$request);
        }else{
            $leadgenerate->image_2 = NULL;
        }

        if($request->hasFile('image_3')){
            $this::createImage('image_3',$leadgenerate,$request);
        }else{
            $leadgenerate->image_3 = NULL;
        }

        $leadgenerate->save();
        return redirect('/leadgenerate')->with('success', 'Lead Generate Saved!');
    }

    public function leadgenerateEdit(Request $request, $id){
        $leadgenerateEditData = LeadGenerate::find($id);
        return view('/leadgenerate/leadgenerateedit')->with(['data' => $leadgenerateEditData]);
    }

    public function leadgenerateUpdate(Request $request, $id){
        //dd($request->all());
       $leadgenerateUpdate = LeadGenerate::find($id);
       $leadgenerateUpdate->title = $request->title;
       $leadgenerateUpdate->meta_title = $request->meta_title;
       $leadgenerateUpdate->meta_description = $request->meta_description;
       $leadgenerateUpdate->keyword = $request->keyword;
       $leadgenerateUpdate->description_1 = $request->description_1;
       $leadgenerateUpdate->descritpion_2 = $request->description_2;
       $leadgenerateUpdate->description_3 = $request->descritpion_3;
       $leadgenerateUpdate->url = $request->url;

       if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$leadgenerateUpdate,$request);
        }

        if($request->hasFile('image_2')){
            $this::imageUpload('image_2',$leadgenerateUpdate,$request);
        }

        if($request->hasFile('image_3')){
            $this::imageUpload('image_3',$leadgenerateUpdate,$request);
        }

        $leadgenerateUpdate->save();
        return redirect('/leadgenerate')->with('success', 'Lead Generate Update!');
    }

    public function leadgenerateDelete(Request $request){
        $leadgenerateDelete = LeadGenerate::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$leadgenerateDelete->image_1,'images/'.$leadgenerateDelete->image_2,'images/'.$leadgenerateDelete->image_3]);
        $leadgenerateDelete->delete();
        return redirect('/leadgenerate')->with('delete', 'Lead Generate Delete!');
    }
}
