<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Mail\ContactRequestMailable;
use App\Mail\AdminSendMailable;
use App\Mail\ContactRequestAdminSendMailable;
use App\ContactusModel;
use App\SocialLinkModel;
use App\Services;
use App\CaseStudyModel;
use App\AboutusModel;
use App\PrivacyPolicyModel;
use App\TermsAndConditionModel;
use App\ContactRequest;
use App\Industries;
use App\Blog;
use App\Leads;
use App\Menu;
use App\ImageGallery;
use App\FrontendPages;
use App\CustomSections;
use App\CustomFields;
use App\Identifiers;
use App\JobOpenings;
use App\Helpers\CentralisedMethods as CmHelper;
use DB;


class HomePageController extends Controller
{	

	public function staticPage(){
		$data = array();
		$data[] = ContactusModel::all();
		$data[] = SocialLinkModel::all();
		$data[] = Services::all();
		$data[] = CaseStudyModel::all(); 
		$data[] = AboutusModel::all();
		$data[] = Blog::select('*')->orderBy('id','DESC')->limit(3)->get();
		$data[] = PrivacyPolicyModel::all();
		$data[] = TermsAndConditionModel::all();
		$data[] = Industries::all();
	 	$data[] = Menu::all(); 						/*09 */
		$data[] = FrontendPages::all();
		$data[] = CustomSections::all();
		$data[] = CustomFields::get();

		return $data;
	}

	public function getAllData(){
		$arrayData = $this->staticPage();
		$segment1 = request()->segment(1);
		$segment2 = request()->segment(2);
		$page_slug = '/';
		$page = FrontendPages::where('slug',$page_slug)->first();
		if(!empty($page)){
			$data = \App\Templates::where('id',$page->template_id)->first();
			$identifiers = Identifiers::where('template_id',$page->template_id)->get();
        }else{
			$data = null;
			$identifiers  = null;
			
		}
		// $pages_data = FrontendPages::all(); 
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$helper = new CmHelper;
		return view('/frontend/home')->with([
					"identifiers" => $identifiers,
					"helper" => $helper,
					"page" => $page,
					"data" => $data ,
					"contactdata" => $arrayData[0], 
					"socialdata" => $arrayData[1], "servicesdata" => $arrayData[2], 
					"casestudydata" => $arrayData[3], "blogdata" => $arrayData[5],
					"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
					"menu_slugs" => $menu_slugs]);    
	}

	public function getFrontPages(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$custom_field = $arrayData[11];
		return view('frontend/industries')
		->with([
			"socialdata" => $arrayData[1], "servicesdata" => $arrayData[2], 
			"casestudydata" => $arrayData[3], "blogdata" => $arrayData[5],
			"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			"menu_slugs" => $menu_slugs,"industries" => $arrayData[8],
			'custom_field' => $custom_field
		]);
	}
	public function getContactUs(){
		$arrayData = $this->staticPage();
		return view('/frontend/contact')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}


// indstries and service are of same layout
	public function Industries(){
		// echo url()->current();
		
		$arrayData = $this->staticPage();
		$pages = $arrayData[10];
// 		dd($pages);
// die;
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$custom_field = $arrayData[11];
		return view('frontend/industries')
		->with([
			"socialdata" => $arrayData[1], "servicesdata" => $arrayData[2], 
			"casestudydata" => $arrayData[3], "blogdata" => $arrayData[5],
			"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			"menu_slugs" => $menu_slugs,"industries" => $arrayData[8],
			'custom_field' => $custom_field
		]);
	}


	public function Services(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('/frontend/industries')
		->with([
			"socialdata" => $arrayData[1], "servicesdata" => $arrayData[2], 
			"casestudydata" => $arrayData[3], "blogdata" => $arrayData[5],
			"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			"menu_slugs" => $menu_slugs,"industries" => $arrayData[8]

		]);
	}

	public function Career(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/career')
		->with([
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}

	public function CareerForm(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/careerform')
		->with([
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}

	public function careerFormStore(Request $request){

		$validatedData = $request->validate([
			'first_name' => 'required|max:25|regex:/^[a-zA-Z\s]*$/',
			'last_name' => 'required|max:25|regex:/^[a-zA-Z\s]*$/',
	        'email' => 'required|max:25|regex:/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/',
	        'contact_num' => 'required|numeric',
	        'qualification' => 'required',
			'experience' => 'required',
			'position' => 'required',
			// 'file' => 'required|mimes:doc,docx,pdf|max:5000'
		]);
		// dd($validatedData);
		// Check if uploaded file size was greater than 
		// maximum allowed file size
		$document = $request->file;

		if ($document->getError() == 1) { 
			$max_size = $document->getMaxFileSize() / 1024 / 1024;  // Get size in Mb
			$error = 'The document size must be less than ' . $max_size . 'Mb.';
			return redirect()->back()->with('flash_danger', $error);
		}
		// dd($request->file('file'));

        $captcha_string = $request['g-recaptcha-response'];
        $captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LcOFMIUAAAAAKSkhWSWp3gCa1cwKAXZpb6jc0yb&response=' . $captcha_string);
        $captcha_json = json_decode($captcha_response);
        // if ($captcha_json->success) {
			
            // if ($request->email && $request->name && $request->phone && $request->details) {
                // ContactRequest::create(["skype_id" => $request->skype_id, "name" => $request->name, "email" => $request->email, "phone" => $request->phone, "details" => $request->details]);
                $to_email = $request->email;
                $data = array();
				$data['first_name'] = $request->first_name;
				$data['last_name'] = $request->last_name;
                $data['email'] = $request->email;
                $data['contact_num'] = $request->contact_num;
                $data['qualification'] = $request->qualification;
				$data['experience'] = $request->experience;
				$data['position'] = $request->position;
				$data['document'] = $request->file;
				
				$user_mail = Mail::to($to_email)->send(new SendMailable($data));
				$admin_mail = Mail::to('visual333@gmail.com')->send(new AdminSendMailable($data));
				// $emailJob = new \App\Jobs\SendContactEmail($data);
                // $this->dispatch($emailJob);
                return back()->with('success', 'Your request successfully submitted we contact with you as soon as possible.');
            // }else{
			// 	return back()->with('danger', 'All fields are required');
			// }
		// }
		// else
		// {
		// 	return back()->with('danger', 'Please verify that you are not a robot.');
		// }

		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/careerform')
		->with([
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}
	
	public function JobOpenings(){
		$job_openings = JobOpenings::orderBy('id','DESC')->get();
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/job-openings')
		->with([
		"job_openings" => $job_openings,	
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}
	public function JobOpeningsExplore($id){
		// dd($id);
		$job_openings = JobOpenings::where('id',$id)->first();
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/job-openings-explore')
		->with([
		"job_openings" => $job_openings,	
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}

	public function JobOpeningsExploreApply($id){
		// dd($id);
		$job_openings = JobOpenings::where('id',$id)->first();
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/job-openings-explore-apply')
		->with([
		"job_openings" => $job_openings,	
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}
	

	public function AboutUs(Request $request){
		$segment1 = request()->segment(1);
		$segment2 = request()->segment(2);
		$page_slug = '/'.$segment1.'/'.$segment2;
		
		$page = FrontendPages::where('slug',$page_slug)->first();
		if($page != null){
            $data = \App\Templates::where('id',$page->template_id)->first();
        }else{
			$data = null;
		}
		// $data = \App\Templates::where('id',$page->template_id)->first();  
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		// $data = CustomFields::first();
		// dd($data->id);
		return view('frontend.aboutus')->with([
					'segment2'=>$segment2,
					'data' => $data,"contactdata" => $arrayData[0],
					"socialdata" => $arrayData[1], "servicesdata" => $arrayData[2], 
					"casestudydata" => $arrayData[3], "blogdata" => $arrayData[5],
					"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
					"menu_slugs" => $menu_slugs,"socialdata" => $arrayData[1], 
					'about' => $arrayData[4],
					'page' => $page
		
		
		]);
	}

	public function Leadership(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");

		return view('/frontend/leadership')->with(["contactdata" => $arrayData[0], 
					"socialdata" => $arrayData[1], 'about' => $arrayData[4],
					"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
					"menu_slugs" => $menu_slugs,"socialdata" => $arrayData[1], 
		]);
	}

	

	

	public function Blog(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();
		return view('/frontend/blog')->with(["contactdata" => $arrayData[0], 
					"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
					"blogdata" => $blogData,"menu_slugs" => $menu_slugs,
					"socialdata" => $arrayData[1]] 
		);
	}

	public function BlogDetails(Request $request){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('/frontend/blog-details')->with(["contactdata" => $arrayData[0], 
			"socialdata" => $arrayData[1],
			"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			"menu_slugs" => $menu_slugs]
		 );
	}


	public function News(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();
		return view('/frontend/blog')->with(["contactdata" => $arrayData[0], 
					"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
					"blogdata" => $blogData,"menu_slugs" => $menu_slugs,
					"socialdata" => $arrayData[1]] 
		);
	}

	public function NewsDetails(Request $request){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('/frontend/blog-details')->with(["contactdata" => $arrayData[0], 
			"socialdata" => $arrayData[1],
			"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			"menu_slugs" => $menu_slugs]
		 );
	}

	public function CaseStudy(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();
		
		return view('/frontend/caseStudy')->with(["contactdata" => $arrayData[0], 
		"socialdata" => $arrayData[1], "blogdata" => $blogData,
		"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
		"menu_slugs" => $menu_slugs]);
	}

	public function CaseStudyDetails(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();

		return view('/frontend/caseStudyDetails')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1],
			 "blogdata" => $blogData,"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			 "menu_slugs" => $menu_slugs]);
	}

	public function ContactUs(){
        $arrayData = $this->staticPage();
        $menu_slugs = (new CmHelper)->TableToArray($arrayData[10], "id", "slug");
        $blogData = Blog::select('*')->where('status', 'publish')->get();
        return view('/frontend/contactus')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1],
             "blogdata" => $blogData,"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
             "menu_slugs" => $menu_slugs]);
	}
	
	public function getContactRequest(Request $request){
		// $imageSlug = config('bucket.test');
		// $arrayData = $this->staticPage();
		// $validatedData = $request->validate([
		// 	'first_name'  => 'required',
		// 	'last_name'  => 'required',
		// 	'email' => 'required|max:50|regex:/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/',
			// //'skype_id' => 'required',
		// 	'phone' => 'required|numeric',
			// // 'website'  => 'required',
			// // 'company'  => 'required',
	    //     'details' => 'required',
	    // ]);
        $captcha_string = $request['g-recaptcha-response'];
        $captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LcOFMIUAAAAAKSkhWSWp3gCa1cwKAXZpb6jc0yb&response=' . $captcha_string);
		$captcha_json = json_decode($captcha_response);
		
		
        if ($captcha_json->success) {
            if ($request->email && $request->first_name && $request->last_name && $request->phone) {
				// dd($request->request);
				ContactRequest::create([
				 "first_name" => $request->first_name,
				 "last_name" => $request->last_name, 
				 "email" => $request->email,
				 "website" => ($request->website)?$request->website:'N/A',  
				 "company" => ($request->company)?$request->company:'N/A', 
				 "phone" => $request->phone, 
				 "skype_id" => ($request->skype_id)?$request->skype_id:'N/A',
				 "details" => ($request->details)?$request->details:'N/A',
				 "page_name" => $request->page_name,
				 "system_ip" => $request->ip()
				 ]);
                $to_email = $request->email;
                $data = array();
				$data['first_name'] = $request->first_name;
				$data['last_name'] = $request->last_name;
				$data['email'] = $request->email;
				$data['website'] = $request->website;
				$data['company'] = $request->company;
				$data['phone'] = $request->phone;
				$data['skype_id'] = $request->skype_id;
				$data['details'] = $request->details;
				$data['page_name'] = $request->page_name;
				
                // dd($data);
				$user = Mail::to($to_email)->send(new ContactRequestMailable($data));
				$admin = Mail::to('visual333@gmail.com')->send(new ContactRequestAdminSendMailable($data));
                // $emailJob = new \App\Jobs\SendContactEmail($data);
                // $this->dispatch($emailJob);

                return back()->with('success', 'Your request successfully submitted.');
            }else{
				return back()->with('danger', 'All fields are required');
			}
		}
		else
		{
			return back()->with('danger', 'Please verify that you are not a robot.');
		}
	}

	public function getGallery(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();
		$gallery = ImageGallery::simplePaginate(10);

		return view('/frontend/gallery')->with([
				"gallery" => $gallery,
				"contactdata" => $arrayData[0],
				"socialdata" => $arrayData[1],
				"blogdata" => $blogData,
				"menudata" => $arrayData[9],
				"frontendpagesdata" => $arrayData[10],
				"menu_slugs" => $menu_slugs
			]);
	}
	public function TokenLauncher(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		$blogData = Blog::select('*')->where('status','publish')->get();
		return view('/frontend/tokenlauncher')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1],
			 "blogdata" => $blogData,"menudata" => $arrayData[9],"frontendpagesdata" => $arrayData[10],
			 "menu_slugs" => $menu_slugs]);
	}
	







	public function getPrivacyPolicy(){
		$arrayData = $this->staticPage();
		return view('/frontend/privacy-policy')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1],'privacy' => $arrayData[6]]);
	}

	public function getTermCondition(){
		$arrayData = $this->staticPage();
		return view('/frontend/term-condition')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1],'term' => $arrayData[7]]);
	}
	public function getDigitalAssets(Request $request, $slug1, $slug2){	
		$arrayData = $this->staticPage();
		$slug='/'.$slug1.'/'.$slug2;
		$titledata = DB::table('page_slug_white_label_exchange')->select('*')->where("url",$slug)->get();

		return view('/frontend/digital-assets')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1], 'data' => $titledata]);
	}

	public function getIcoDevelopment(){
		$arrayData = $this->staticPage();
		return view('/frontend/ico-development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIcoSto(){
		$arrayData = $this->staticPage();
		return view('/frontend/ico-sto')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIcoBounty(){
		$arrayData = $this->staticPage();
		return view('/frontend/ico-bounty')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIcoListingServices(){
		$arrayData = $this->staticPage();
		return view('/frontend/listing-services')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIcoMarketing(){
		$arrayData = $this->staticPage();
		return view('/frontend/ico-marketing')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIcoMarketingGuide(){
		$arrayData = $this->staticPage();
		return view('/frontend/ico-marketing')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getIeoDevelopment(){
		$arrayData = $this->staticPage();
		return view('/frontend/ieo-development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getWalletDevelopment(){
		$arrayData = $this->staticPage();
		return view('/frontend/wallet-development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getExchangeSoftware(){
		$arrayData = $this->staticPage();
		return view('/frontend/exchange-software')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getCryptomlmsoftware(){
		$arrayData = $this->staticPage();
		return view('/frontend/crypto-mlm-software')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getCryptoatmsoftware(){
		$arrayData = $this->staticPage();
		return view('/frontend/crypto-atm-software')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getExchangeListingService(){
		$arrayData = $this->staticPage();
		return view('/frontend/exchange-listing-services')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainConsulting(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-consulting')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainFinance(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-finance')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainCyberSecurity(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-cybersecurity')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainSmartContract(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-smart-contract')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainIotDevelopment(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-iot-development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	public function getBlockchainGameDevelopment(){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain-game-development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}

	

	public function getLeads(Request $request){		
		
		if($request->name && $request->email && $request->skypeid && $request->phone && $request->description){
			Leads::create(["name" => $request->name, "email" => $request->email, "skypeid" => $request->skypeid, "phone" => $request->phone, "description" => $request->description]);
			return back()->with('success', 'Your request successfully submitted');	
		
		}else{
			return back()->with('danger', 'All fields are required');
		}
		
	}

	public function getIcoService(Request $request){
		$checkEmail = DB::table('rev_icoservice')->select('*')->where("email", $request->email)->get();
		if($request->email){
			if($checkEmail->isEmpty()){
				DB::table('rev_icoservice')->insert(["email" => $request->email]);
				return back()->with('success', 'Your request successfully submitted');
			}	
			else{
				return back()->with('danger', 'Email is already exist');
			}
		}else{
			return back()->with('danger', 'fields are required');
		}
	}

	

	public function getCryptocurrecyDevelopment(Request $request){
		$arrayData = $this->staticPage();
		return view('/frontend/cryptocurrency')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);	
	}

	public function getBlockchainDevelopment(Request $request){
		$arrayData = $this->staticPage();
		return view('/frontend/blockchain')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}
    public function getWebDesignServices()
    {
    	$contactData = ContactusModel::all();
    	$socialLinkData = SocialLinkModel::all();
        return view('frontend.web-design-service')->with(["contactdata" => $contactData, "socialdata" => $socialLinkData]);
    }

    public function getDigitalMarketingServices()
    {
    	$contactData = ContactusModel::all();
    	$socialLinkData = SocialLinkModel::all();
        return view('frontend.digital-marketing-service')->with(["contactdata" => $contactData, "socialdata" => $socialLinkData]);
    }

    public function getDecentralizeApplication()
    {
    	$contactData = ContactusModel::all();
    	$socialLinkData = SocialLinkModel::all();
        return view('frontend.decentralize-application')->with(["contactdata" => $contactData, "socialdata" => $socialLinkData]);
	}
	
	public function getPhpDevelopment(Request $request){
		$arrayData = $this->staticPage();
		return view('/frontend/php_development')->with(["contactdata" => $arrayData[0], "socialdata" => $arrayData[1]]);
	}
	
	//----------------------------------------- test actions remove when task complete 
	public function uploadForm(){
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		return view('frontend/testuploadform')
		->with([
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
	}


	public function uploadDocument(Request $request) {
		$title = $request->file('title');
		// Get the uploades file with name document
		$document = $request->file('document');
		// Required validation
		$request->validate([
			'title' => 'required|max:255',
			'document' => 'required'
		]);
		// Check if uploaded file size was greater than 
		// maximum allowed file size
		if ($document->getError() == 1) {
			$max_size = $document->getMaxFileSize() / 1024 / 1024;  // Get size in Mb
			$error = 'The document size must be less than ' . $max_size . 'Mb.';
			return redirect()->back()->with('flash_danger', $error);
		}
		$data = [
			'document' => $document
		];
		// If upload was successful
		// send the email
		$to_email = 'visual333@gmail.com';
		// dd($to_email);
		 Mail::to($to_email)->send(new \App\Mail\TestUpload($data));
		return redirect()->back()->with('flash_success', 'Your document has been uploaded.');
	}
	
	public function zohotestform(Request $request) {
		$arrayData = $this->staticPage();
		$menu_slugs = (new CmHelper)->TableToArray($arrayData[10],"id","slug");
		
		return view('frontend.zohocontactus')
		->with([
		"contactdata" => $arrayData[0],"socialdata" => $arrayData[1], 
		"about" => $arrayData[4],"menudata" => $arrayData[9],
		"frontendpagesdata" => $arrayData[10],"menu_slugs" => $menu_slugs
		]);
		
		
	}

	//------------------ end 


	/*public function getUserContactEmailTemplate()
	{
		$user = DB::table('emailtemplates')->where('name', 'contact_form_user')->first();
		return view('/email/name',compact('user'));
	}

	public function getAdminContactEmailTemplate()
	{
		$admin = DB::table('emailtemplates')->where('name', 'contact_form_admin')->first();
		return view('/email/adminname',compact('admin'));
	}*/
}	
