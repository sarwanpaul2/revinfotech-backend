<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomSections;
use App\FrontendPages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CentralisedMethods;

class CustomSectionsController extends Controller
{
    public function __construct()
    {
		// Config::set('adminlte.skin', 'blue');
    }
    
    public function section_Index(){
        $pages = FrontendPages::all();
        $data = CustomSections::all();
        $page_name = (new CentralisedMethods)->TableToArray($pages,'id','name');
		return view('customsections/customsectionslist')->with(array('page_name'=> $page_name,'data' => $data, 'action' => 'view'));
		// return view('customsections/customsectionslist');
    }
    
    public function section_Add(){
        //this pages_data for dropdown in form
        $pages_data = FrontendPages::all();
		return view('customsections.customsections')->with(array('pages_data'=> $pages_data,));
    }
       
	public function section_Create(){
        // $pages_data = FrontendPages::all();
        
		// get the form data for the user 
        $sectionFormData = Input::post();
        // write the validation rules for your form
        $rules = [
            'section_name' => 'required',
            
        ]; 
        // validate the user form data
        $validation = Validator::make($sectionFormData, $rules);   
        
        // if validation fails
        if($validation->fails())
        {
            // dd($validation);
            $messages = $validation->messages();
            // dd($messages->messages());
            return redirect('sections/add')->with(array('type' => 'error', 'danger' => $messages));
        }
        // if validation passes
        // save the user to the database
       
        $user = CustomSections::create(array(
							'section_name' => Input::post('section_name'),
							'section_description' => (Input::get('section_description') !== null)?Input::get('section_description'):'N/A',
							'field_name' => Input::post('field_name'),
							'field_type' => Input::post('field_type'),
							'field_description' => (Input::get('field_description')!== null)?Input::get('field_description'):'N/A',
							'page_id' => Input::post('page_id'),
							'status' => Input::post('status'),
					));
        return redirect('/sections/index')->with(array('type' => 'success', 'message' => 'Section created successfully!'));
		
    }

    public function section_Edit($id){
        $data = CustomSections::find($id);
        $pages_data = FrontendPages::all();
		return view('customsections/customsectionsedit')->with(array('pages_data' => $pages_data,'data' => $data, 'action' => 'view'));
	}
    
    public function section_Update($id = '', $action = 'update'){
        $custom_section = CustomSections::find($id);
        $custom_section->section_name = Input::get('section_name');
        $custom_section->section_description = (Input::get('section_description') !== null)?Input::get('section_description'):'N/A';
        $custom_section->field_name = Input::get('field_name');
        $custom_section->field_type = Input::get('field_type');
        $custom_section->field_description = (Input::get('field_description')!== null)?Input::get('field_description'):'N/A';
        $custom_section->page_id = Input::get('page_id');
        $custom_section->status = Input::get('status');
        
        $success = $custom_section->save();
         if($success){
             $message = $custom_section->id.' updated successfully!';
             $type = 'success';
         }
         else{
             $message = 'Error!';
             $type = 'error';
         }

        return redirect('sections/index')->with(array('success' => $message));
    } 
    
    public function section_delete(Request $request){
		$custom_section = CustomSections::find($request->delid)->delete();
		// if($custom_section){
		// 	if($custom_section->is_deleted == 0){
		// 		$custom_section->is_deleted = 1;
		// 		$custom_section->save();
		// 		$type = 'success';
		// 		$message = 'custom Section deleted successfully!';
		// 	}
		// 	else{
		// 		$type = 'error';
		// 		$message = 'Custom Section already deleted.';
		// 	}
		// }
		// else{
		// 	$message = 'Custom Section does not exists.';
		// 	$type = 'error';
        // }
        $message = 'Succesfully deleted.';

		return redirect('sections/index')->with(array('success' => $message));
	}
}
