<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\PortfolioModel;

class Portfolio extends Controller
{   

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->$imagename = $name;

    }



    public function portfolioView(){

        $portfolioData = PortfolioModel::all();
    	return view('/portfolio/portfoliolist')->with(['data' => $portfolioData]);
        //return view('/portfolio/portfolio');
    }
    public function portfolioAdd(){
        return view('/portfolio/portfolio');
    }
    public function portfolioCreate(Request $request){
    	
    	$portfolio = new PortfolioModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description_1" => $request->description_1, "descritpion_2" => $request->description_2, "description_3" => $request->descritpion_3
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1',$portfolio,$request);
	    }else{
            $portfolio->image_1 = NULL;
        }

        if($request->hasFile('image_2')){
            $this::createImage('image_2',$portfolio,$request);
        }else{
            $portfolio->image_2 = NULL;
        }

        if($request->hasFile('image_3')){
            $this::createImage('image_3',$portfolio,$request);
        }else{
            $portfolio->image_3 = NULL;
        }

        $portfolio->save();
        return redirect('/portfolio')->with('success', 'Portfolio Saved!');
    }

    public function portfolioEdit(Request $request, $id){
        $portfolioEditData = PortfolioModel::find($id);
        return view('/portfolio/portfolioedit')->with(['data' => $portfolioEditData]);
    }

    public function portfolioUpdate(Request $request, $id){
        //dd($request->all());
       $portfolioUpdate = PortfolioModel::find($id);
       $portfolioUpdate->title = $request->title;
       $portfolioUpdate->meta_title = $request->meta_title;
       $portfolioUpdate->meta_description = $request->meta_description;
       $portfolioUpdate->keyword = $request->keyword;
       $portfolioUpdate->description_1 = $request->description_1;
       $portfolioUpdate->descritpion_2 = $request->description_2;
       $portfolioUpdate->description_3 = $request->descritpion_3;

       if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$portfolioUpdate,$request);
        }

        if($request->hasFile('image_2')){
            $this::imageUpload('image_2',$portfolioUpdate,$request);
        }

        if($request->hasFile('image_3')){
            $this::imageUpload('image_3',$portfolioUpdate,$request);
        }

        $portfolioUpdate->save();
        return redirect('/portfolio')->with('success', 'Portfolio Update!');
    }

    public function portfolioDelete(Request $request){
        $portfolioDelete = PortfolioModel::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$portfolioDelete->image_1,'images/'.$portfolioDelete->image_2,'images/'.$portfolioDelete->image_3]);
        $portfolioDelete->delete();
        return redirect('/portfolio')->with('delete', 'Portfolio Delete!');
    }
}

