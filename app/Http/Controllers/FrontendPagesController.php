<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\FrontendPages;
use App\Templates;


class FrontendPagesController extends Controller
{

    //
    public function imageUpload($imagename,$frontendpages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$frontendpages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $frontendpages->image = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$frontendpages,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        
        $frontendpages->image = $name;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function frontendpages_Index()
    {
        $frontend_pages = FrontendPages::orderBy('id','DESC')->get();
        return view('/frontendpages/frontendpageslist')->with(['data' => $frontend_pages]);
    }

    public function frontendpagesAdd(){
        $template = Templates::all();
        return view('/frontendpages/frontendpages' ,compact(['template']));
        // return view('templates.view', compact(['helper','identifiers','page','data','action' => 'view']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function frontendpagesCreate(Request $request)
    {

// std code example--start
        // get the form data for the user 
        // $frontendpages = Input::post();
        

        // $frontendpages = FrontendPages::create(array(
        //     'name' => Input::post('name'),
        //     'description' => Input::post('name'),
        //     "image" => Input::post('name'),
        //     'slug' => Input::post('name'),
        //     'status' => Input::post('name'),
            
        //  ));

        // if ($request->hasFile('image')) {
        //     $this::createImage('image', $frontendpages, $request);
	    // }else{
        //     $frontendpages->image = NULL;
        // }
// std code example--end

        $frontendpages = new FrontendPages([
            "name" => $request->name, 
            "description" => $request->description, 
            "image" => $request->image, 
            "slug" => $request->slug, 
            "status" => $request->status, 
            "sections" => 10, 
            "template_id" => $request->template_id, 
        ]);
       
        // $validatedData = $request->validate([
		// 	 'image' => 'required|mimes:doc,omdoc,pdf|max:5000'
        // ]);
        $this->validate($request, ['image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        
        
        if ($request->hasFile('image')) {
            $this::createImage('image', $frontendpages, $request);
	    }else{
            $frontendpages->image = NULL;
        }
        // dd($industries);
        $frontendpages->save();
        return redirect('/frontendpages-index')->with('success', 'Frontend Page Saved!');
    }

    public function frontendpagesEdit(Request $request, $id){
        
        $template = Templates::all();
        $data = FrontendPages::find($id);
        return view('/frontendpages/frontendpagesedit',compact(['template','data']));
    }

    public function frontendpagesUpdate(Request $request, $id){
        // dd($request->all());
       $frontendpages = FrontendPages::find($id);
       $frontendpages->name = $request->name;
       $frontendpages->description = $request->description;
       $frontendpages->slug = $request->slug;
       $frontendpages->status = $request->status;
       $frontendpages->template_id= $request->template_id; 

       if ($request->hasFile('image')) {
            $this::imageUpload('image',$frontendpages,$request);
        }

        $frontendpages->save();
        return redirect('/frontendpages-index')->with('success', 'frontendpages Update!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function frontendpagesDelete(Request $request){
        $frontendpagesDelete = FrontendPages::find($request->delid);
        $resp=Storage::disk('s3')->delete('images/'.$frontendpagesDelete->image);
        $frontendpagesDelete->delete();
        return redirect('/frontendpages-index')->with('delete', 'Frontend Pages Delete!');
    }
}
