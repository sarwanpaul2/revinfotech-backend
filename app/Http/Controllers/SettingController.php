<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;

class SettingController extends Controller
{
    public function settingView(){
        $settingsData = Settings::all();
    	return view('/settings/settingslist')->with(['data' => $settingsData]);
    }

    public function settingAdd(){
        return view('/settings/settings');
    }

    public function settingCreate(Request $request){
        //dd($request->all());
    	Settings::create(["slug" => $request->slug, "label" => $request->label, "value" => $request->value, "helpertext" => $request->helpertext, "encrypted" => $request->encrypt]);
    	
        return redirect('/setting')->with('success', 'Settings Saved!');
    }

    public function settingEdit(Request $request, $id){
        $settingsEditData = Settings::find($id);
        return view('/settings/settingsedit')->with(['data' => $settingsEditData]);
    }

    public function settingUpdate(Request $request, $id){
        $settingsUpdate = Settings::find($id);
        $settingsUpdate->slug = $request->slug;
        $settingsUpdate->label = $request->label;
        $settingsUpdate->value = $request->value;
        $settingsUpdate->helpertext = $request->helpertext;
        $settingsUpdate->encrypted = $request->encrypted;
        
        $settingsUpdate->save();
        return redirect('/setting')->with('success', 'Settings Update!');
    }

    public function settingDelete(Request $request){
        $settingsDelete = Settings::find($request->delid);
        $settingsDelete->delete();
        return redirect('/setting')->with('delete', 'Settings Delete!');
    }
}
