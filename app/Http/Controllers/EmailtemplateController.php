<?php

namespace App\Http\Controllers;

use App\Emailtemplate;
use Illuminate\Http\Request;
use DB;

class EmailtemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $emailtemplates = DB::table('emailtemplates')
        $emailtemplates = Emailtemplate::orderBy('id','DESC')->get();
        return view('emailtemplate.list', ['emailtemplates' => $emailtemplates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emailtemplate  $emailtemplate
     * @return \Illuminate\Http\Response
     */
    public function show(Emailtemplate $emailtemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emailtemplate  $emailtemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(Emailtemplate $emailtemplate, $id)
    {
        $templateEditData = Emailtemplate::find($id);
        $templateData = Emailtemplate::all();
        return view('/emailtemplate/edit')->with(['data' => $templateEditData,'template_list' => $templateData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emailtemplate  $emailtemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $templateUpdate = Emailtemplate::find($id);
        $templateUpdate->template = $request->template;

        $templateUpdate->update();
        return redirect('/emailtemplate/list')->with('success', 'Email Template Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emailtemplate  $emailtemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emailtemplate $emailtemplate)
    {
        $menuDelete = Emailtemplate::find($request->delid);
        $menuDelete->delete();
        return redirect('/emailtemplate')->with('delete', 'Email Template Delete!');
    }
}
