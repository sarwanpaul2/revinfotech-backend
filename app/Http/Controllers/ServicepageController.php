<?php

namespace App\Http\Controllers;

use App\Servicepage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ServicepageController extends Controller
{
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$servicepage,$request){

        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        dd($resp);
        $servicepage->$imagename = $name;

    }



    public function servicepageView(){

        $servicepageData = Servicepage::all();
        return view('/servicepage/servicepagelist')->with(['data' => $servicepageData]);
    }
    public function servicepageAdd(){
        return view('/servicepage/servicepage');
    }
    public function servicepageCreate(Request $request){
        
        $servicepage = new Servicepage([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description_1" => $request->description_1, "descritpion_2" => $request->description_2, "description_3" => $request->descritpion_3, "url" => $request->url
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1',$servicepage,$request);
        }else{
            $servicepage->image_1 = NULL;
        }

        if($request->hasFile('image_2')){
            $this::createImage('image_2',$servicepage,$request);
        }else{
            $servicepage->image_2 = NULL;
        }

        if($request->hasFile('image_3')){
            $this::createImage('image_3',$servicepage,$request);
        }else{
            $servicepage->image_3 = NULL;
        }

        $servicepage->save();
        return redirect('/servicepage')->with('success', 'Service Page Saved!');
    }

    public function servicepageEdit(Request $request, $id){
        $servicepageEditData = Servicepage::find($id);
        return view('/servicepage/servicepageedit')->with(['data' => $servicepageEditData]);
    }

    public function servicepageUpdate(Request $request, $id){
        //dd($request->all());
       $servicepageUpdate = Servicepage::find($id);
       $servicepageUpdate->title = $request->title;
       $servicepageUpdate->meta_title = $request->meta_title;
       $servicepageUpdate->meta_description = $request->meta_description;
       $servicepageUpdate->keyword = $request->keyword;
       $servicepageUpdate->description_1 = $request->description_1;
       $servicepageUpdate->descritpion_2 = $request->description_2;
       $servicepageUpdate->description_3 = $request->descritpion_3;
       $servicepageUpdate->url = $request->url;

       if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$servicepageUpdate,$request);
        }

        if($request->hasFile('image_2')){
            $this::imageUpload('image_2',$servicepageUpdate,$request);
        }

        if($request->hasFile('image_3')){
            $this::imageUpload('image_3',$servicepageUpdate,$request);
        }

        $servicepageUpdate->save();
        return redirect('/servicepage')->with('success', 'Service Page Update!');
    }

    public function servicepageDelete(Request $request){
        $servicepageDelete = Servicepage::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$servicepageDelete->image_1,'images/'.$servicepageDelete->image_2,'images/'.$servicepageDelete->image_3]);
        $servicepageDelete->delete();
        return redirect('/servicepage')->with('delete', 'Service Page Delete!');
    }
}
