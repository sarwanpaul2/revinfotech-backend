<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TermsAndConditionModel;

class TermsAndCondition extends Controller
{
    public function termsconditionView(){

        $termsconditionData = TermsAndConditionModel::all();
        //dd($termsconditionData);
    	return view('/termsandcondition/termsandconditionlist')->with(['data' => $termsconditionData]);
    }

    public function termsconditionAdd(){
        return view('/termsandcondition/termsandcondition');
    }

    public function termsconditionCreate(Request $request){
    	$termsAndConditions = new TermsAndConditionModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keywords" => $request->keywords, "description" => $request->description_1
        ]);
        $termsAndConditions->save();
        return redirect('/termscondition')->with('success', 'Terrms And Conditions Saved!');
    }

    public function termsconditionEdit(Request $request, $id){
        $termsconditionEditData = TermsAndConditionModel::find($id);
        return view('/termsandcondition/termsandconditionedit')->with(['data' => $termsconditionEditData]);
    }

    public function termsconditionUpdate(Request $request, $id){
		$termsAndConditionUpdate = TermsAndConditionModel::find($id);
		$termsAndConditionUpdate->title = $request->title;
		$termsAndConditionUpdate->meta_title = $request->meta_title;
		$termsAndConditionUpdate->meta_description = $request->meta_description;
		$termsAndConditionUpdate->keywords = $request->keyword;
		$termsAndConditionUpdate->description = $request->description_1;

    	$termsAndConditionUpdate->save();
        return redirect('/termscondition')->with('success', 'Terms And Conditions Update!');
    }

    public function termsconditionDelete(Request $request){
        $termsconditionDelete = TermsAndConditionModel::find($request->delid);
        $termsconditionDelete->delete();
        return redirect('/termscondition')->with('delete', 'Terms And Conditions Delete!');
    }
}
