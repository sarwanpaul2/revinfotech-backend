<?php

namespace App\Http\Controllers;

use App\Templates;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\FrontendPages;
use App\Identifiers;
use App\Menu;
use App\Helpers\CentralisedMethods as CmHelper;

class TemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Templates::orderBy('id','DESC')->get();
        return view('templates.index')->with(array('action' => 'index','data' => $data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages_data = FrontendPages::all();
        return view('templates.create', compact(['pages_data','action' => 'create']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        // dd($request);
        $request->validate( [
            'layout' => 'required',
            'external_css' => 'required',
            'type' => [
                'required',
                Rule::in([ Templates::TYPE_PAGE, Templates::TYPE_EMAIL ]),
            ],
        ]);
        // if validation passes
        // save the user to the database
            
            $template = Templates::create(array(
                'layout' => $request->input('layout'),
                'type' => $request->input('type'),
                'external_css' => $request->input('external_css')
            ));
        return redirect('templates')->with(array('type' => 'success', 'message' => 'Tempate saved successfully!'));
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function show(Templates $templates, $id)
    {
        
        $data = Templates::where('id',$id)->first(); 
        $page = FrontendPages::where('id',$data->page_id)->first();
        $identifiers = Identifiers::where('template_id',$id)->get();
        $helper = new CmHelper;
        $menudata = Menu::all();
        $front_pages = FrontendPages::all();
        $menu_slugs = (new CmHelper)->TableToArray($front_pages,"id","slug"); 

        // dd($identifiers);   
        return view('templates.view', compact(['menu_slugs','menudata','helper','identifiers','page','data','action' => 'view']));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function edit(Templates $templates, $id)
    {
        $pages_data = FrontendPages::all();
        
        $data = Templates::where('id',$id)->first(); 
        // dd($data->layout);
        return view('templates.edit', compact(['data','id','pages_data','action' => 'edit']));
    }

    public function developeredit(Templates $templates, $id)
    {
        $pages_data = FrontendPages::all();
        
        $data = Templates::where('id',$id)->first(); 
        // dd($data->layout);
        return view('templates.developeredit', compact(['data','id','pages_data','action' => 'developeredit']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->action);
        if($request->action == "developeredit")
        {
            $template = Templates::find($id);
            $template->layout = $request->input('layout');
            $template->type = $request->input('type');
            $template->external_css = $request->input('external_css');
            $success = $template->save();
             if($success){
                 $message = $template->id.' updated successfully!';
                 $type = 'success';
             }
             else{
                 $message = 'Error!';
                 $type = 'error';
             }
            $pages_data = FrontendPages::all();
            $data = Templates::where('id',$id)->first(); 
            return view('templates.developeredit', compact(['data','id','pages_data','action' => 'developeredit']));
        }

        $template = Templates::find($id);
        $template->layout = $request->input('layout');
        $template->type = $request->input('type');
        $template->external_css = $request->input('external_css');
        $success = $template->save();
         if($success){
             $message = $template->id.' updated successfully!';
             $type = 'success';
         }
         else{
             $message = 'Error!';
             $type = 'error';
         }

        return redirect('templates')->with(array('success' => $message));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function destroy(Templates $templates,Request $request )
    {
        // dd($request->delid);
        $custom_section = Templates::find($request->delid)->delete();
        $message = 'Succesfully deleted.';

		return redirect('templates')->with(array('success' => $message));
    }
}
