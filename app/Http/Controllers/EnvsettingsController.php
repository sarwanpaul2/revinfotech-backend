<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Envsettings;
use Exception;
use Illuminate\Support\Facades\DB;

class EnvsettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function index()
    {

        $settings_data = DB::table('env_settings')->orderBy('id','desc')->get();
        $data = $settings_data;
        return view('envsettings/settingsIndex',compact('data'));      
    }

     public function createForm()
    {
        return view('envsettings/settingsform');
    }

    public function addForm(Request $request)
    {
        try {throw_if(!isset($request), new Exception('Please fill the form and submit'));
            
            $setting_data = new Envsettings([
                'setting_name' => $request->setting_name,
                'setting_description' => $request->setting_description,
                'setting_value' => $request->setting_value,
               
            ]);
            $setting_data->save();
        }catch(Exception $e){
            die($e->getMessage());
        }
        
        return redirect('envsettings/index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $settings = Envsettings::find($id);

        return view('envsettings/settingsEdit', compact('settings'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'setting_name'=>'required',
            'setting_description'=>'required',
            'setting_value'=>'required'
        ]);
        
        $settings = Envsettings::find($id);
        $settings->setting_name =  $request->get('setting_name');
        $settings->setting_description = $request->get('setting_description');
        $settings->setting_value = $request->get('setting_value');
        $settings->save();

        return redirect('envsettings/index')->with('success', 'Setting updated!');
    }

    public function destroy($id)
    {
        $settings = Envsettings::where('id',$id);
        $settings->delete();
 
        return redirect('envsettings/settings/index')->with('success', 'Pages deleted!');
    }

    
}
