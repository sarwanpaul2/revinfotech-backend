<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\PrivacyPolicyModel;

class PrivacyPolicy extends Controller
{   

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
       
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->image = $name;

    }

    public function privacypolicyView(){

        $privacyPolicyData = PrivacyPolicyModel::all();
    	return view('/privacypolicy/privacypolicylist')->with(['data' => $privacyPolicyData]);
    }

    public function privacypolicyAdd(){
        return view('/privacypolicy/privacypolicy');
    }

    public function privacypolicyCreate(Request $request){
    	$privacypolicy = new PrivacyPolicyModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1
        ]);

        if ($request->hasFile('image_1')) {
	        $this::createImage('image_1',$privacypolicy,$request);

	    }else{
            $privacypolicy->image = NULL;
        }

        $privacypolicy->save();
        return redirect('/privacypolicy')->with('success', 'Privacy Policy Saved!');
    }

    public function privacypolicyEdit(Request $request, $id){
        $privacyPolicyEditData = PrivacyPolicyModel::find($id);
        return view('/privacypolicy/privacypolicyedit')->with(['data' => $privacyPolicyEditData]);
    }

    public function privacypolicyUpdate(Request $request, $id){
       $privacyPolicyUpdate = PrivacyPolicyModel::find($id);
       $privacyPolicyUpdate->title = $request->title;
       $privacyPolicyUpdate->meta_title = $request->meta_title;
       $privacyPolicyUpdate->meta_description = $request->meta_description;
       $privacyPolicyUpdate->keyword = $request->keyword;
       $privacyPolicyUpdate->description = $request->description_1;
       
       if ($request->hasFile('image')) {
            $this::imageUpload('image',$privacyPolicyUpdate,$request);
        }

        $privacyPolicyUpdate->save();
        return redirect('/privacypolicy')->with('success', 'Privacy Policy Update!');
    }

    public function privacypolicyDelete(Request $request){
        $privacyPolicyDelete = PrivacyPolicyModel::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$privacyPolicyDelete->image_1]);
        $privacyPolicyDelete->delete();
        return redirect('/privacypolicy')->with('delete', 'Privacy Policy Delete!');
    }

}
