<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomFields;
use App\CustomSections;
use App\FrontendPages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CentralisedMethods;
use DB;
use Storage;    
class CustomFieldsController extends Controller
{
    public function __construct()
    {
		// Config::set('adminlte.skin', 'blue');
    }
    public function fields_Index(){
        $data = CustomFields::all();
        $section = CustomSections::all();
        $pages = FrontendPages::all();
        $page_name = (new CentralisedMethods)->TableToArray($pages,'id','name');
        $section_result = (new CentralisedMethods)->TableToArray($section,'id','section_name');
       
		return view('customfields/customfieldslist')->with(array('section_result'=> $section_result,'page_name'=> $page_name,'pages'=>$pages,'data' => $data, 'action' => 'view'));
		// return view('customfields/customfieldslist');
    }
    
    public function fields_Add(){
        //this pages_data for dropdown in form
        $pages = FrontendPages::all();
        $custom_sections = CustomSections::all()->unique('page_id');
        $result = (new CentralisedMethods)->TableToArray($pages,'id','name');
        
		return view('customfields.customfields')->with(array('result' => $result,'pages'=>$pages,'custom_sections'=> $custom_sections,));
    }
    
   
	public function fields_Create(){
        $pages_data = FrontendPages::all();
        
		// get the form data for the user 
        // $sectionFormData = Input::post();
        $page_id = Input::post('page_id');
        $status = Input::post('status');
        $custom_sections = CustomSections::all()->where('page_id',$page_id);
       
        foreach($custom_sections as $result){
            if($result->field_type == 'image'){
                $file = Input::file($result->field_name);
                if ($file !== null) {
                    // $file = $request->file($imagename);
                    // dd($file);
                    $name = 'image_'.time().$file->getClientOriginalName();
                    $filePath = 'images/' . $name   ;
                    $resp =Storage::disk('public')->put($filePath, file_get_contents($file));
                    // dd($resp);
                    // $frontendpages->image = $name;
                    $field_data = $name;
                    $section_id = $result->id;
                    $status = Input::post('status');
                    $insert = array(
                    'section_id' => $section_id,
                    'page_id' => $page_id,
                    'status' => $status,
                    'field_data' => $field_data,
                    );
                }else{
                    $custom_field=CustomFields::where(array('section_id' => $result->id,'page_id' => $page_id))->first();
                    // dd($custom_field->section_id);
                   
                    $insert = array(
                    'section_id' => $custom_field->section_id,
                    'page_id' => $custom_field->page_id,
                    'status' => $custom_field->status,
                    'field_data' => $custom_field->field_data,
                    );
                }
            
            }else{
                $field_data = (Input::post($result->field_name) !== " ")?Input::post($result->field_name):"N/A";
                // dd($field_data);
                $section_id = $result->id;
                $status = Input::post('status');

                $insert = array( 
                    'section_id' => $section_id,
                    'page_id' => $page_id,
                    'status' => $status,
                    'field_data' => $field_data,

                );


            }
            // $fdata = CustomFields::where(array('section_id' => $result->id,'page_id' => $page_id))->first();
            
            $save_data = CustomFields::updateOrCreate(array('section_id' => $result->id,'page_id' => $page_id) ,$insert);
            
        }

        // dd($insert);

        // write the validation rules for your form
        $rules = [
            // 'section_name' => 'required',
            
        ]; 
        // validate the user form data
        // $validation = Validator::make($sectionFormData, $rules);   
        
        // if validation fails
        // if($validation->fails())
        // {
        //     // dd($validation);
		// 	$messages = $validation->messages();
        //     return redirect('sections/index')->with(array('type' => 'error', 'success' => $messages));
        // }
        // if validation passes
        // save the user to the database
       
        
        return redirect('/fields/index')->with(array('type' => 'success', 'message' => 'Data added successfully!'));
		
    }

    public function section_Edit($id){
		$data = CustomSections::find($id);
		return view('customsections/customsectionsedit')->with(array('data' => $data, 'action' => 'view'));
	}
    
    public function section_Update($id = '', $action = 'update'){
        $custom_section = CustomSections::find($id);
        $custom_section->section_name = Input::get('section_name');
        $custom_section->section_description = Input::get('section_description');
        $custom_section->field_name = Input::get('field_name');
        $custom_section->field_type = Input::get('field_type');
        $custom_section->field_description = Input::get('field_description');
        $custom_section->page_id = Input::get('page_id');
        $custom_section->status = Input::get('status');
        
        $success = $custom_section->save();
         if($success){
             $message = $custom_section->id.' updated successfully!';
             $type = 'success';
         }
         else{
             $message = 'Error!';
             $type = 'error';
         }

        return redirect('sections/index')->with(array('success' => $message));
    } 
    
    public function section_delete(Request $request){
		$custom_section = CustomSections::find($request->delid)->delete();
		// if($custom_section){
		// 	if($custom_section->is_deleted == 0){
		// 		$custom_section->is_deleted = 1;
		// 		$custom_section->save();
		// 		$type = 'success';
		// 		$message = 'custom Section deleted successfully!';
		// 	}
		// 	else{
		// 		$type = 'error';
		// 		$message = 'Custom Section already deleted.';
		// 	}
		// }
		// else{
		// 	$message = 'Custom Section does not exists.';
		// 	$type = 'error';
        // }
        $message = 'Succesfully deleted.';

		return redirect('sections/index')->with(array('success' => $message));
    }
    
    public function myformAjax($id)

    { 
        $cities = DB::table("custom_sections")
                    ->where("page_id",$id)
                    ->select("*")->get();
            $count = 1;        
            foreach($cities as $data){
                $fdata = CustomFields::where(array('section_id' => $data->id,'page_id' => $data->page_id))->first();
                $fdata = ($fdata!== null)? $fdata->field_data :" ";
                if($fdata!=null)
                {

                }
                
                
                // $response[] = '
                // <option value = "'.$data->id.'">'.$data->section_name.'</option>';
                // dd($data);
                    if($data->field_type == 'text'){
                        $response[] = '<strong><h3>Text Section '.$count++.'</h3></strong>
                        <div class="form-group">
                        <label for="section_id">Section Name : </label>
                        <input type="text" value="'.$data->section_name.'" readonly name="'.$data->section_name.'" >
                    </div>
                    <div class="form-group">
                        <label for="field_data">Field Data</label>
                        <input type="text" class="form-control" name="'.$data->field_name.'" value = "'.$fdata.'" required ="true" />
                    </div>';
                }else if($data->field_type == 'image'){
                    $response[] ='<strong><h3>Image Section '.$count++.'</h3></strong>
                    <div class="form-group">
                    <label for="section_id">Section Name : </label>
                    <input type="text"  value="'.$data->section_name.'" readonly name="'.$data->section_name.'">
                </div>
                <div class="form-group">
                    <label for="field_data">Field Data</label>
                    <input type="file"  name="'.$data->field_name.'" value = "">
                    
                </div>';
                }else if($data->field_type == 'textarea'){
                    $response[] ='<strong><h3>Textarea Section '.$count++.'</h3></strong>
                        <div class="form-group">
                        <label for="section_id">Section Name : </label>
                        <input type="text" value="'.$data->section_name.'" readonly name="'.$data->section_name.'">
                    </div>
                    <div class="form-group">
                        <label for="field_data">Field Data</label>
                        <textarea class="form-control" name="'.$data->field_name.'"  required="true" rows="3">'.$fdata.'</textarea>
                    </div>';
                }
            }        
                   
        return $response;
        // return json_encode($cities);


    }
    
}
