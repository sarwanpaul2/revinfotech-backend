<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Blog;

class BlogController extends Controller
{
    //
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->image = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$blog,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $blog->image = $name;

    }

    public function blogView(){
        $blogData = Blog::all();
        return view('/blog/bloglist')->with(['data' => $blogData]);
    }
    public function blogAdd(){
        return view('/blog/blog');
    }
    public function blogCreate(Request $request){
    	
    	$blog = new Blog([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1, "status" => $request->status, "slug" => $request->slug
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1', $blog, $request);
	    }else{
            $blog->image = NULL;
        }
        
        $blog->save();
        return redirect('/blogs')->with('success', 'Blog Saved!');
    }

    public function blogEdit(Request $request, $id){
        $blogEditData = Blog::find($id);
        return view('/blog/blogedit')->with(['data' => $blogEditData]);
    }

    public function blogUpdate(Request $request, $id){
        //dd($request->all());
       $blogUpdate = Blog::find($id);
       $blogUpdate->title = $request->title;
       $blogUpdate->meta_title = $request->meta_title;
       $blogUpdate->meta_description = $request->meta_description;
       $blogUpdate->keyword = $request->keyword;
       $blogUpdate->description = $request->description_1;
       $blogUpdate->status = $request->status;
       $blogUpdate->slug = $request->slug;

       if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$blogUpdate,$request);
        }

        $blogUpdate->save();
        return redirect('/blogs')->with('success', 'Blog Update!');
    }

    public function blogDelete(Request $request){
        $blogDelete = Blog::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$blogDelete->image_1,'images/'.$blogDelete->image_2,'images/'.$blogDelete->image_3]);
        $blogDelete->delete();
        return redirect('/blogs')->with('delete', 'Blog Delete!');
    }
}
