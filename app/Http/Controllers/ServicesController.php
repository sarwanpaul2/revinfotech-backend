<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services;

class ServicesController extends Controller
{
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->image = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->image = $name;

    }    
    
	public function servicesView(){
		$servicesData = Services::all();
		return view('/services/serviceslist')->with(['data' => $servicesData]);
	}

    public function servicesAdd(){
        return view('/services/services');
    }

    public function servicesCreate(Request $request){	
    	$services = new Services([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1
        ]);

        if ($request->hasFile('image_1')) {
	        $this::createImage('image_1',$services,$request);

	    }else{
            $services->image = NULL;
        }

        $services->save();
        return redirect('/services')->with('success', 'Services Saved!');
    }

    public function servicesEdit(Request $request, $id){
        $servicesEditData = Services::find($id);
        return view('/services/servicesedit')->with(['data' => $servicesEditData]);
    }

    public function servicesUpdate(Request $request, $id){
        $servicesUpdate = Services::find($id);
        $servicesUpdate->title = $request->title;
        $servicesUpdate->meta_title = $request->meta_title;
        $servicesUpdate->meta_description = $request->meta_description;
        $servicesUpdate->keyword = $request->keyword;
        $servicesUpdate->description = $request->description_1;
         
        if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$servicesUpdate,$request);
        }

        $servicesUpdate->save();
        return redirect('/services')->with('success', 'Services Update!');
     }
    
    public function servicesDelete(Request $request){
        $servicesDelete = Services::find($request->delid);
        $servicesDelete->delete();
        return redirect('/services')->with('delete', 'Service Delete!');
    }
}
