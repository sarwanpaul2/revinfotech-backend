<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Pages;
use App\PageContent;
use Exception;
use Illuminate\Support\Str;
use App\PortfolioModel;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** function for uploading image in page update action through function */ 
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$pages,$request){
            $file = $request->file($imagename);
            $name = $imagename.time().$file->getClientOriginalName();
            $filePath = 'images/' . $name;
            $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
            $pages->$imagename = $name;

    }

    public function index()
    {
        $pages = Pages::all()->take(500);
        //$portfolioData = PortfolioModel::all();
        return view('pages/index')->with('data',$pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
        $request->validate([
            'title'=>'required'
        ]);
      
       
        if(PageContent::where(['title' => $request->get('title')])->count()){
            return redirect('/pages')->with('error', 'OOPS! Title already there.'); 
        }else{
            $dataTitle = PageContent::select('title')->get();
            foreach($dataTitle as $value){                   
                if (strpos(strtolower($value->title), strtolower($request->get('title'))) !== false) {
                    return redirect('/pages')->with('error', 'OOPS! Title already there.'); 
                }
            }   
        }

        $slug = strtolower(str_replace(' ', '-', $request->get('title')));
        $slug = $slug.'-in-[city]';
        $arr = array(
           'url' => '/[state]/'.$slug,
           'title' => $request->get('title'),
           'meta_title' => $request->get('meta_title'),
           'meta_description' => $request->get('meta_description'),
           'keyword' => $request->get('keyword'),
           'desc1' => $request->input('desc1'),
           'desc2' => $request->input('desc2'),
           'desc3' => $request->input('desc3')
       );
 
        $pages = new PageContent([
            'title' => $request->get('title'),
            'content' => json_encode($arr)
        ]);
       
        if ($request->hasFile('image1')) {
            $this::createImage('image1',$pages,$request);
	    }else{
            $pages->image1 = NULL;
        }

        if($request->hasFile('image2')){
            $this::createImage('image2',$pages,$request);
        }else{
            $pages->image2 = NULL;
        }

        if($request->hasFile('image3')){
            $this::createImage('image3',$pages,$request);
        }else{
            $pages->image3 = NULL;
        }

        $pages->save();
        return redirect('/pages')->with('success', 'Page saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pages = pages::find($id);
        return view('pages/edit', compact('pages'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'meta_title'=>'required',
            'keyword'=>'required',
            'desc1' => 'required',
            'desc2' => 'required',
            'desc3' => 'required',
            // 'image1' => 'required',
            // 'image2' => 'required',
            // 'image3' => 'required',
            'city' => 'required',
            'state' => 'required',
            'slug1' => 'required',
            'slug2' => 'required'
        ]);
 
        $pages = pages::find($id);
        $pages->title = $request->get('title');
        $pages->meta_title = $request->get('meta_title');
        $pages->keyword = $request->get('keyword');
        $pages->desc1 = $request->get('desc1');
        $pages->desc2 = $request->get('desc2');
        $pages->desc3 = $request->get('desc3');
        
        // we use imageupload function here for updating images with parameters for image uploading  
        if($request->hasFile('image1')){
            $this::imageUpload('image1',$pages,$request);
        }
        if($request->hasFile('image2')){ 
            $this::imageUpload('image2',$pages,$request);
        }
        if($request->hasFile('image3')){
            $this::imageUpload('image3',$pages,$request);
        }
        
        $pages->city = $request->get('city');
        $pages->state = $request->get('state');
        $pages->slug1 = $request->get('slug1');
        $pages->slug2 = $request->get('slug2');
        $pages->save();
        return redirect('/pages')->with('success', 'Page saved!');
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pagesDelete = Pages::where('page_content_id',$id);
        $pagesDelete->delete();

        $pages = PageContent::find($id);
        $resp=Storage::disk('s3')->delete(['images/'.$pages->image1,'images/'.$pages->image2,'images/'.$pages->image3]);
        $pages->delete();
        return redirect('/pages')->with('success', 'Pages deleted!');
    }
}
