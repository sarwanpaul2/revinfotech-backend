<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leads;

class LeadsController extends Controller
{
    public function getLeadsData(Request $request){
        $data = Leads::all();
        return view('leads/leadslist')->with('data',$data);
    }
}
