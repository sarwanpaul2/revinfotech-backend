<?php

namespace App\Http\Controllers;

use App\Icoservice;
use Illuminate\Http\Request;

class IcoserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icoserviceData = Icoservice::all();
        return view('/icoservice/icoservicelist')->with(['data' => $icoserviceData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Icoservice  $icoservice
     * @return \Illuminate\Http\Response
     */
    public function show(Icoservice $icoservice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Icoservice  $icoservice
     * @return \Illuminate\Http\Response
     */
    public function edit(Icoservice $icoservice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Icoservice  $icoservice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icoservice $icoservice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Icoservice  $icoservice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icoservice $icoservice)
    {
        //
    }
}
