<?php

namespace App\Http\Controllers;
use App\Templates;
use App\Identifiers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\FrontendPages;
use App\Helpers\CentralisedMethods;

class IdentifierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Identifiers::orderBy('id','DESC')->get();
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
			
        return view('identifiers.index')->with(array('action' => 'index','data' => $data,
                    'page_data' => $pages_data,
                    'templates' => $templates));
    }

    public function templatewiseIndex()
    {
        $data = Identifiers::groupBy('template_id')->get();
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
			
        return view('identifiers.templatewise_index')->with(array('action' => 'index','data' => $data,
                    'page_data' => $pages_data,
                    'templates' => $templates));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
        // dd($templates);
        $page_name = (new CentralisedMethods)->TableToArray($pages_data, 'id', 'name');
        return view('identifiers.create', compact(['page_name','templates','pages_data','action' => 'create']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $request_data = $request;
          for($i=0; $i < count($request->name); $i++){
                echo $request->name[$i]. "<br>";
                echo $request->value[$i]. "<br>";
                echo $request->type[$i]. "<br>";
                echo $request->template_id[$i]. "<br>";

                $identifier = Identifiers::create(array(
                    'name' => $request->name[$i],
                    'value' => $request->value[$i],
                    'type' => $request->type[$i],
                    'template_id' =>  $request->template_id[$i]
                    
                ));
            }
    
// single data entry start-----------------------------       
        // if validation passes
        // save the user to the database
            
        // $template = Identifiers::create(array(
        //         'name' => $request->input('name'),
        //         'value' => $request->input('value'),
        //         'type' => $request->input('type'),
        //         'template_id' => $request->input('template_id')
                
        //     ));
// single data entry end-----------------------------       
        $template_id = $identifier->template_id;

        // return redirect('identifiers')->with(array('type' => 'success', 'message' => 'Identifiers saved successfully!'));
        return redirect('identifiers/'.$template_id.'/templatewise-show')->with(array('type' => 'success', 'message' => 'Identifiers saved successfully!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Identifier  $identifier
     * @return \Illuminate\Http\Response
     */
    public function show(Identifier $identifier)
    {
        //
    }
    public function templatewiseShow($identifier_id)
    {
        // dd($identifier_id);
        $data = Identifiers::where('template_id',$identifier_id)->orderBy('id','DESC')->get();
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
			
        return view('identifiers.indexTemp')->with(array('action' => 'index','data' => $data,
                    'page_data' => $pages_data,
                    'templates' => $templates));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Identifiers  $identifier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
        $data = Identifiers::where('id', $id)->first();
        $page_name = (new CentralisedMethods)->TableToArray($pages_data, 'id', 'page_id');
        // dd($data->layout);
        return view('identifiers.edit', compact(['page_name','templates','data','id','pages_data','action' => 'edit']));
    }
    public function templatewiseEdit($id)
    {
        $pages_data = FrontendPages::all();
        $templates = Templates::all();
        $data = Identifiers::where('id', $id)->first();
        $page_name = (new CentralisedMethods)->TableToArray($pages_data, 'id', 'page_id');
        // dd($data->layout);
        return view('identifiers.templatewise_edit', compact(['page_name','templates','data','id','pages_data','action' => 'edit']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Identifiers  $identifier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        
        if($request->submit){
            $identifier = Identifiers::find($id);
            $identifier->name = $request->name;
            $identifier->value = $request->value;
           
        }else{
            // dd($request->value);
            $identifier = Identifiers::find($id);
            $identifier->name = $request->input('name');
            $identifier->type = $request->input('type');
            $identifier->value = $request->input('value');
            $identifier->template_id = $request->input('template_id');
        }    
        $success = $identifier->save();
        if ($success) {
            $message = $identifier->id.' updated successfully!';
            $type = 'success';
        } else {
            $message = 'Error!';
            $type = 'error';
        }

        $template_id = $identifier->template_id;
        return redirect('identifiers/'.$template_id.'/templatewise-show')->with(array('success' => $message));
    }

    public function templatewiseUpdate(Request $request,$id)
    {
        
        if($request->submit){
            $identifier = Identifiers::find($id);
            $identifier->name = $request->name;
            $identifier->value = $request->value;
           
        }else{
            // dd($request->value);
            $identifier = Identifiers::find($id);
            $identifier->name = $request->input('name');
            $identifier->type = $request->input('type');
            $identifier->value = $request->input('value');
            $identifier->template_id = $request->input('template_id');
        }    
        $success = $identifier->save();
        if ($success) {
            $message = $identifier->id.' updated successfully!';
            $type = 'success';
        } else {
            $message = 'Error!';
            $type = 'error';
        }

        return redirect('identifiers')->with(array('success' => $message));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Identifiers  $identifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Identifiers $identifiers,Request $request)
    {
        
        // dd($request->delid);
        $identifiers = Identifiers::where('id' ,$request->delid)->first('template_id');
        // dd($template_id);
        if (!empty($identifiers)){
            $template_id = $identifiers->template_id;
            // dd($template_id);
            $custom_section = Identifiers::find($request->delid)->delete();
            $message = 'Identifiers Deleted successfully!';
            return redirect('identifiers/'.$template_id.'/templatewise-show')->with(array('success' => $message ));
        } else {
            return redirect('identifiers.templatewise_index')->with(array('success' => $message));
        }
    }    
    function insert(Request $request)
    {
     if($request->ajax())
     {
      $rules = array(
       'first_name.*'  => 'required',
       'last_name.*'  => 'required'
      );
      $error = Validator::make($request->all(), $rules);
      if($error->fails())
      {
       return response()->json([
        'error'  => $error->errors()->all()
       ]);
      }

      $first_name = $request->first_name;
      $last_name = $request->last_name;
      for($count = 0; $count < count($first_name); $count++)
      {
       $data = array(
        'first_name' => $first_name[$count],
        'last_name'  => $last_name[$count]
       );
       $insert_data[] = $data; 
      }

      DynamicField::insert($insert_data);
      return response()->json([
       'success'  => 'Data Added successfully.'
      ]);
     }
    }
}
