<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactRequest;

class ContactRequestController extends Controller
{
    public function index()
    {
        $data = ContactRequest::orderBy('id','DESC')->get();
        return view('contactrequest.index')->with(array('action' => 'index','data' => $data));
    }
}
