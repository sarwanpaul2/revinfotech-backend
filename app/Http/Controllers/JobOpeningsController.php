<?php

namespace App\Http\Controllers;

use App\JobOpenings;
use Illuminate\Http\Request;

class JobOpeningsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JobOpenings::orderBy('id','DESC')->get();
        return view('jobopenings.index')->with(array('action' => 'index','data' => $data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $pages_data = JobOpenings::all();
        return view('jobopenings.create', compact(['action' => 'create']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate( [
            'position' => 'required',
            'experience' => 'required',
            'location' => 'required',
            'sharp_skills' => 'required',
            'skills' => 'required',
            // 'remarks' => 'required',
            'job_desc' => 'required',
            
        ]);
        // if validation passes
        // save the user to the database
            
            $template = JobOpenings::create(array(
                'position' => $request->input('position'),
                'experience' => $request->input('experience'),
                'location' => $request->input('location'),
                'sharp_skills' => $request->input('sharp_skills'),
                'skills' => $request->input('skills'),
                'remark' => ($request->input('remark') != NUll)?$request->input('remark'):'N/A',
                'job_desc' => $request->input('job_desc'),
                'date' => ($request->input('date') != NUll)?$request->input('date'):date('Y-m-d'),
            ));
        return redirect('job-openings')->with(array('type' => 'success', 'message' => 'Jobs saved successfully!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOpenings  $jobOpenings
     * @return \Illuminate\Http\Response
     */
    public function show(JobOpenings $jobOpenings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOpenings  $jobOpenings
     * @return \Illuminate\Http\Response
     */
    public function edit(JobOpenings $jobOpenings ,$id)
    {
        // $pages_data = FrontendPages::all();
        $data = JobOpenings::where('id',$id)->first(); 
        return view('jobopenings.edit', compact(['data','id','action' => 'developeredit']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobOpenings  $jobOpenings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $job_openings = JobOpenings::find($id);
            $job_openings->position = $request->input('position');
            $job_openings->experience = $request->input('experience');
            $job_openings->location = $request->input('location');
            $job_openings->sharp_skills = $request->input('sharp_skills');
            $job_openings->skills = $request->input('skills');
            $job_openings->remark = $request->input('remark');
            $job_openings->job_desc = $request->input('job_desc');
            $success = $job_openings->save();
             if($success){
                 $message = $job_openings->id.' updated successfully!';
                 $type = 'success';
             }
             else{
                 $message = 'Error!';
                 $type = 'error';
             }
            // $pages_data = FrontendPages::all();
            $data = JobOpenings::where('id',$id)->first(); 
            return redirect('job-openings')->with(array('success' => $message));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOpenings  $jobOpenings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $job_openings = JobOpenings::find($request->delid);
        $job_openings->delete();
        return redirect('/job-openings')->with('delete', 'Job Deleted!');
    }
}
