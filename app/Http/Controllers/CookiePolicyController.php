<?php

namespace App\Http\Controllers;

use App\CookiePolicy;
use Illuminate\Http\Request;

class CookiePolicyController extends Controller
{
    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
       
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->image = $name;

    }

    public function cookiepolicyView(){

        $cookiePolicyData = CookiePolicy::all();
        return view('/cookiepolicy/cookiepolicylist')->with(['data' => $cookiePolicyData]);
    }

    public function cookiepolicyAdd(){
        return view('/cookiepolicy/cookiepolicy');
    }

    public function cookiepolicyCreate(Request $request){
        $cookiepolicy = new CookiePolicy([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1',$cookiepolicy,$request);

        }else{
            $cookiepolicy->image = NULL;
        }

        $cookiepolicy->save();
        return redirect('/cookiepolicy')->with('success', 'Cookie Policy Saved!');
    }

    public function cookiepolicyEdit(Request $request, $id){
        $cookiePolicyEditData = CookiePolicy::find($id);
        return view('/cookiepolicy/cookiepolicyedit')->with(['data' => $cookiePolicyEditData]);
    }

    public function cookiepolicyUpdate(Request $request, $id){
       $cookiePolicyUpdate = CookiePolicy::find($id);
       $cookiePolicyUpdate->title = $request->title;
       $cookiePolicyUpdate->meta_title = $request->meta_title;
       $cookiePolicyUpdate->meta_description = $request->meta_description;
       $cookiePolicyUpdate->keyword = $request->keyword;
       $cookiePolicyUpdate->description = $request->description_1;
       
       if ($request->hasFile('image')) {
            $this::imageUpload('image',$cookiePolicyUpdate,$request);
        }

        $cookiePolicyUpdate->save();
        return redirect('/cookiepolicy')->with('success', 'Cookie Policy Update!');
    }

    public function cookiepolicyDelete(Request $request){
        $cookiePolicyDelete = CookiePolicy::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$cookiePolicyDelete->image_1]);
        $cookiePolicyDelete->delete();
        return redirect('/cookiepolicy')->with('delete', 'Cookie Policy Delete!');
    }
}
