<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ProductModel;

class Product extends Controller
{   

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->$imagename = $name;

    }

    public function productView(){

        $productData = ProductModel::all();
    	return view('/product/productlist')->with(['data' => $productData]);
        //return view('/product/product');
    }
    public function productAdd(){
        return view('/product/product');
    }
    public function productCreate(Request $request){
    	
    	$product = new ProductModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description_1" => $request->description_1, "descritpion_2" => $request->description_2, "description_3" => $request->descritpion_3
        ]);

        if ($request->hasFile('image_1')) {
	        $this::createImage('image_1',$product,$request);
	    }else{
            $product->image_1 = NULL;
        }

        if($request->hasFile('image_2')){
            $this::createImage('image_2',$product,$request);
        }else{
            $product->image_2 = NULL;
        }

        if($request->hasFile('image_3')){
            $this::createImage('image_3',$product,$request);
        }else{
            $product->image_3 = NULL;
        }

        $product->save();
        return redirect('/product')->with('success', 'Product Saved!');
    }

    public function productEdit(Request $request, $id){
        $productEditData = ProductModel::find($id);
        return view('/product/productedit')->with(['data' => $productEditData]);
    }

    public function productUpdate(Request $request, $id){
       $productUpdate = ProductModel::find($id);
       $productUpdate->title = $request->title;
       $productUpdate->meta_title = $request->meta_title;
       $productUpdate->meta_description = $request->meta_description;
       $productUpdate->keyword = $request->keyword;
       $productUpdate->description_1 = $request->description_1;
       $productUpdate->descritpion_2 = $request->description_2;
       $productUpdate->description_3 = $request->descritpion_3;

        if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$productUpdate,$request);
        }

        if($request->hasFile('image_2')){
            $this::imageUpload('image_2',$productUpdate,$request);
        }

        if($request->hasFile('image_3')){
            $this::imageUpload('image_3',$productUpdate,$request);
        }

        $productUpdate->save();
        return redirect('/product')->with('success', 'Product Update!');
    }

    public function productDelete(Request $request){
        $productDelete = ProductModel::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$productDelete->image_1,'images/'.$productDelete->image_2,'images/'.$productDelete->image_3]);
        $productDelete->delete();
        return redirect('/product')->with('delete', 'Product Delete!');
    }
}
