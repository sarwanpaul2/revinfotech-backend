<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Industries;
use Illuminate\Support\Facades\Storage;

class IndustriesController extends Controller
{


    //
    public function imageUpload($imagename,$industries,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$industries->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $industries->bg_image = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$industries,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $industries->bg_image = $name;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function industriesIndex()
    {
        $industryData = Industries::all();
        return view('/industries/industrieslist')->with(['data' => $industryData]);
    }

    public function industriesAdd(){
        return view('/industries/industries');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function industriesCreate(Request $request)
    {
    // server side validation
        // $request->validate([
        //     'bg_image'=>'required',
        //     'title'=>'required',
        //     'description_first'=>'required',
        //     'link_first'=>'required',
        //     'title_second'=>'required',
        //     'description_second'=>'required',
        //     'link_second'=>'required'
        // ]);
        $industries = new Industries([
            "bg_image" => $request->bg_image, 
            "title" => $request->title, 
            "description_first" => $request->description_first, 
            "link_first" => $request->link_first, 
            "title_second" => $request->title_second, 
            "description_second" => $request->description_second, 
            "link_second" => $request->link_second, 
            
        ]);
        
        if ($request->hasFile('bg_image')) {
            $this::createImage('bg_image', $industries, $request);
	    }else{
            $industries->bg_image = NULL;
        }
        // dd($industries);
        $industries->save();
        return redirect('/industries-index')->with('success', 'Blog Saved!');
    }

    public function industriesEdit(Request $request, $id){
        $blogEditData = Industries::find($id);
        return view('/industries/industriesedit')->with(['data' => $blogEditData]);
    }

    public function industriesUpdate(Request $request, $id){
        // dd($request->all());
       $industries = Industries::find($id);

       $industries->title = $request->title;
       $industries->description_first = $request->description_first;
       $industries->link_first = $request->link_first;
       $industries->title_second = $request->title_second;
       $industries->link_second = $request->link_second;
      

       if ($request->hasFile('bg_image')) {
            $this::imageUpload('bg_image',$industries,$request);
        }

        $industries->save();
        return redirect('/industries_index')->with('success', 'Industries Update!');
    }
    

    

    
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function industriesDelete(Request $request){
        $industriesDelete = Industries::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$industriesDelete->bg_image,'images/'.$industriesDelete->bg_image,'images/'.$industriesDelete->bg_image]);
        $industriesDelete->delete();
        return redirect('/industries_index')->with('delete', 'Industries Delete!');
    }
}
