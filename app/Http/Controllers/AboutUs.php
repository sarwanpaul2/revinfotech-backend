<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\AboutusModel;

class AboutUs extends Controller
{   

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->$imagename = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->$imagename = $name;

    }


    public function aboutusView(){
        $aboutusData = AboutusModel::all();
    	return view('/aboutus/aboutuslist')->with(['data' => $aboutusData]);
    }
    public function aboutusAdd(){
        return view('/aboutus/aboutus');
    }
    public function aboutusCreate(Request $request){
    	
    	$aboutus = new AboutusModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1
        ]);

        if ($request->hasFile('image_1')) {
            $this::createImage('image_1',$aboutus,$request);
	    }else{
            $aboutus->image_1 = NULL;
        }

        if($request->hasFile('image_2')){
            $this::createImage('image_2',$aboutus,$request);
        }else{
            $aboutus->image_2 = NULL;
        }

        if($request->hasFile('image_3')){
            $this::createImage('image_3',$aboutus,$request);
        }else{
            $aboutus->image_3 = NULL;
        }

        $aboutus->save();
        return redirect('/aboutus')->with('success', 'Aboutus Saved!');
    }

    public function aboutusEdit(Request $request, $id){
        $aboutusEditData = AboutusModel::find($id);
        return view('/aboutus/aboutusedit')->with(['data' => $aboutusEditData]);
    }

    public function aboutusUpdate(Request $request, $id){
       $aboutusUpdate = AboutusModel::find($id);
       $aboutusUpdate->title = $request->title;
       $aboutusUpdate->meta_title = $request->meta_title;
       $aboutusUpdate->meta_description = $request->meta_description;
       $aboutusUpdate->keyword = $request->keyword;
       $aboutusUpdate->description = $request->description_1;
       

        if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$aboutusUpdate,$request);

        }

        if($request->hasFile('image_2')){
            $this::imageUpload('image_2',$aboutusUpdate,$request);
        }

        if($request->hasFile('image_3')){
            $this::imageUpload('image_3',$aboutusUpdate,$request);
        }

        $aboutusUpdate->save();
        return redirect('/aboutus')->with('success', 'Aboutus Update!');
    }

    public function aboutusDelete(Request $request){
        $aboutusDelete = AboutusModel::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$aboutusDelete->image_1,'images/'.$aboutusDelete->image_2,'images/'.$aboutusDelete->image_3]);
        $aboutusDelete->delete();
        return redirect('/aboutus')->with('delete', 'Aboutus Delete!');
    }
}
