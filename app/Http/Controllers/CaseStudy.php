<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\CaseStudyModel;

class CaseStudy extends Controller
{	

    public function imageUpload($imagename,$pages,$request)
    {
        // first delete old image
        $delete1=Storage::disk('s3')->delete('images/'.$pages->$imagename);
        $file1 = $request->file($imagename);
        $name1 = $imagename.time().$file1->getClientOriginalName();
        $filePath1 = 'images/' . $name1;
        // Insert new image
        $resp1 = Storage::disk('s3')->put($filePath1, file_get_contents($file1));
        $pages->image = $name1;
    }
    
    /** function for uploading image in page create action through function */
    public function createImage($imagename,$portfolio,$request){
        $file = $request->file($imagename);
        $name = $imagename.time().$file->getClientOriginalName();
        $filePath = 'images/' . $name;
        $resp =Storage::disk('s3')->put($filePath, file_get_contents($file));
        $portfolio->image = $name;

    }

	public function casestudyView(){
	   	$casestudyData = CaseStudyModel::all();
		return view('/casestudy/casestudylist')->with(['data' => $casestudyData]);
	}
	public function casestudyAdd(){
        return view('/casestudy/casestudy');
    }
     public function casestudyCreate(Request $request){
    	
    	$casestudy = new CaseStudyModel([
            "title" => $request->title, "meta_title" => $request->meta_title, "meta_description" => $request->meta_description, "keyword" => $request->keyword, "description" => $request->description_1
        ]);

        if ($request->hasFile('image_1')) {
	        $this::createImage('image_1',$casestudy,$request);
	    }else{
            $casestudy->image = NULL;
        }

        $casestudy->save();
        return redirect('/casestudy')->with('success', 'Case Study Saved!');
    }

    public function casestudyEdit(Request $request, $id){
        $casestudyEditData = CaseStudyModel::find($id);
        return view('/casestudy/casestudyedit')->with(['data' => $casestudyEditData]);
    }

    public function casestudyUpdate(Request $request, $id){
       $casestudyUpdate = CaseStudyModel::find($id);
       $casestudyUpdate->title = $request->title;
       $casestudyUpdate->meta_title = $request->meta_title;
       $casestudyUpdate->meta_description = $request->meta_description;
       $casestudyUpdate->keyword = $request->keyword;
       $casestudyUpdate->description = $request->description_1;
       
       if ($request->hasFile('image_1')) {
            $this::imageUpload('image_1',$casestudyUpdate,$request);
        }
        $casestudyUpdate->save();
        return redirect('/casestudy')->with('success', 'Case Study Update!');
    }

    public function casestudyDelete(Request $request){
        $casestudyDelete = CaseStudyModel::find($request->delid);
        $resp=Storage::disk('s3')->delete(['images/'.$casestudyDelete->image]);
        $casestudyDelete->delete();
        return redirect('/casestudy')->with('delete', 'Case Study Delete!');
    }
}
