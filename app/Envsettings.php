<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envsettings extends Model
{
    protected $table = 'env_settings';
    protected $fillable = 
    [
    'id',
    'setting_name',
    'setting_description',
    'setting_value'
    ];
}
