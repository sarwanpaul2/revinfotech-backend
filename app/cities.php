<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cities extends Model
{
    protected $table = 'cities';
 
	public function state()
	{
		return $this->belongsTo('App\states');
	}
}
