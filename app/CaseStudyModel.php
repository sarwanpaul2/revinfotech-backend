<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseStudyModel extends Model
{
   	protected $table = "rev_casestudy";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description", "image"];
}
