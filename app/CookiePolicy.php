<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CookiePolicy extends Model
{
    protected $table="cookie_policies";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "image", "description"];
}
