<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industries extends Model
{
    protected $table = "industries";
    protected $fillable = [
        
         "bg_image",
         "title",
          "description_first",
           "link_first",
            "title_second",
             "description_second",
              "link_second"
               ];
}
