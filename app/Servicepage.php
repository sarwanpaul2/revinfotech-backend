<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicepage extends Model
{
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description_1", "descritpion_2", "description_3", "image_1", "image_2", "image_3", "url"];
}
