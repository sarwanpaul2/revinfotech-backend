<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\SendMailable;
use App\Mail\AdminSendMailable;
use Illuminate\Support\Facades\Log;

class SendContactEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Log::debug('handle');
        $email = new SendMailable($this->data);
        $mail = Mail::to($this->data[1])->send($email);
        $email1 = new \App\Mail\AdminSendMailable($this->data);
        Mail::to('vijay@gmail.com')->send($email1);
    }
}
