<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $table = "rev_leads";
    protected $fillable = ["name", "email", "skypeid", "phone", "description"];
}
