<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identifiers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'identifiers';
    protected $fillable = ["name", "value", "template_id"];  
}
