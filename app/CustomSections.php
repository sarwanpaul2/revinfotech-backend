<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomSections extends Model
{
    protected $table="custom_sections";
    protected $fillable = ["section_name", "section_description", "field_name", "field_type", "field_description", "page_id","status"];  
}
