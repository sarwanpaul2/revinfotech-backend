<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    protected $table = "rev_contactrequest";
    protected $fillable = ["first_name", "last_name", "email", "website", "company", "phone", "details", "skype_id", "page_name", "system_ip"];
}
