<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontendPages extends Model
{
    protected $table = 'frontend_pages';
    protected $fillable = 
    [
    'name',
    'description',
    'image',
    'slug',
    'status',
    'sections',
    'template_id'
    ];
}
