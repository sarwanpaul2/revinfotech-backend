<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icoservice extends Model
{
    protected $table = "rev_icoservice";
    protected $fillable = ["email"];
}
