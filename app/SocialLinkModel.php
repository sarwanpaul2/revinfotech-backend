<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLinkModel extends Model
{
    protected $table = "rev_sociallink";
    protected $fillable = ["facebook_url", "twitter_url", "youtube_url", "instagram_url", "google_url", "linkedin_url", "pinterest_url"];

}
