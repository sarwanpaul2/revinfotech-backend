<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
  /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'page_contents';
    
    /**
     * The attributes that have default values.
     *
     * @var array
     */
    protected $attributes = [
        'accumlator' => 0,
        'job_status' => false,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'accumlator', 'job_status', 'image1', 'image2', 'image3'];
  
}
