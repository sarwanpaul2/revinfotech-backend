<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //

    protected $table = "rev_blogs";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description", "status", "image", "slug"];
}
