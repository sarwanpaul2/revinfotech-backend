<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFields extends Model
{   
    protected $table="custom_fields";
    protected $fillable = [
        'section_id',
        'page_id',
        'field_data',
        'status'
              
    ];

     public function frontendpage()
    {
        return $this->belongsTo('App\FrontendPages','page_id');
    }
}
