<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicyModel extends Model
{
    protected $table="rev_privacypolicy";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "image", "description"];
}
