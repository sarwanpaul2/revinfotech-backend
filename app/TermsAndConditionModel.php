<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermsAndConditionModel extends Model
{
    protected $table = "rev_terms_condition";
    protected $fillable = ["title", "meta_title", "meta_description", "keywords", "description"];
}
