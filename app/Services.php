<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    //
    protected $table = "rev_services";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description", "image"];
}
