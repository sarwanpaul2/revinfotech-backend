<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = "rev_product";
    protected $fillable = ["title", "meta_title", "meta_description", "keyword", "description_1", "descritpion_2", "description_3", "image_1", "image_2", "image_3", "url"];
}
