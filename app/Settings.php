<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = "rev_settings";
    protected $fillable = ["slug", "label", "value", "helpertext", "encrypted"];
}
