<?php

use Illuminate\Database\Seeder;

class EnvSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::table('env_settings')->get();
        if (count($data)<1) {
            $Insert = DB::select(DB::raw("INSERT INTO `env_settings` ( `setting_name`,`setting_description`, `setting_value`) VALUES
            ('MAIL_HOST', 'hosting server', 'smtp.mailgun.org'),
            ('MAIL_DRIVER', 'simple mail transfer protocol', 'smtp'),
            ('MAIL_PORT', 'port', 587),
            ('MAIL_USERNAME', 'username of mailler', 'Null'),
            ('MAIL_PASSWORD', 'password of mailer', 'Null'),
            ('MAIL_ENCRYPTION', 'type encryption', 'tls')
            "));
        }    

    $result = DB::select( DB::raw("SELECT * FROM env_settings" ));
    dd($result);
    }
}
