<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRevContactrequestColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rev_contactrequest', function (Blueprint $table) {
            $table->string('first_name',191);
            $table->string('last_name',191);
            $table->string('page_name')->nullable();
            $table->string('system_ip');
            $table->string('website');
            $table->string('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
