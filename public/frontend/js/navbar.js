var sideNav = document.getElementById("right-top-cross-menu");
var humburger = document.getElementById("hamburger");
var menuItem = document.getElementsByClassName("menuItem");
var navigationMenuText = document.getElementsByClassName("navigationMenu");
var navitemImg = document.getElementsByClassName("navigationImg");
var navLink = document.getElementsByClassName("navitemLink");
var companyLogo = document.getElementById("companyLogo");
var socialfabIcon = document.getElementById("fabIcon");
var documentBody = document.querySelector("body");

sideNav.classList.add("d-none");
var flag = false;

let sideNavBar = function sideNavBar() {

flag = !flag;

if(flag) {
documentBody.style.overflow = "hidden";
removeAll();
sideNav.classList.add("d-block");
humburger.classList.add("is-active");
companyLogo.classList.add("animated", "zoomIn");
socialfabIcon.classList.add("animated", "fadeInUp");

for (var i = 0; i < menuItem.length; i++) {
    menuItem[i].classList.add("d-none");
}

for (var _i = 0; _i < navLink.length; _i++) {
    navLink[_i].classList.add("animated", "fadeInDown");
}

textAnimation();
navitemImgInner();
} else {
documentBody.style.overflow = "auto";
humburger.classList.remove("is-active");
sideNav.classList.remove("d-block");
companyLogo.classList.remove("animated", "zoomIn");

for (var _i2 = 0; _i2 < navLink.length; _i2++) {
    navLink[_i2].classList.remove("animated", "fadeInDown");
}
}
};

for (var i = 0; i < menuItem.length; i++) {
menuItem[i].classList.add("d-none");
}

function removeAll() {
for (var _i3 = 0; _i3 < menuItem.length; _i3++) {
menuItem[_i3].classList.remove("d-block", "animated", "fadeInDown");
}
}

var showMenu = function showMenu(menuId) {
removeAll();
document.getElementById(menuId).classList.add("d-block", "animated", "fadeInDown");
};

var textAnimation = function textAnimation() {
for (var _i4 = 0; _i4 < navigationMenuText.length; _i4++) {
navigationMenuText[_i4].classList.add("animated", "fadeInUp");
}
};

var navitemImgInner = function navitemImgInner() {
for (var _i5 = 0; _i5 < navitemImg.length; _i5++) {
navitemImg[_i5].classList.add("animated", "fadeInDown");
}
};