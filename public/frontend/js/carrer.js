        var locations = document.getElementById("locations");
        var main = document.getElementById("main-d");
        var heading = document.getElementById("hetch1");
        var docBody = document.querySelector('body');
        var col1 = document.getElementById("col-1");
        var col2 = document.getElementById("col-2");
        var col3 = document.getElementById("col-3");
        var float1 = document.getElementById("float-1");
        var float2 = document.getElementById("float-2");
        var float3 = document.getElementById("float-3");
        var floattext1 = document.getElementById("float-text-1");
        var floattext2 = document.getElementById("float-text-2");
        var floattext3 = document.getElementById("float-text-3");
    

        var isInViewport = function (elem) {
        var bounding = elem.getBoundingClientRect();
        return (
            bounding.top >= 0 &&
            bounding.left >= 0 &&
            bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        };


        window.onscroll = function() {
            if (isInViewport(heading)){
            heading.classList.remove("d-none");
            heading.classList.add("animated","flipInY");
            }

            if (isInViewport(col1)){
            col1.classList.remove("d-none");       
            col1.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(col2)){
            col2.classList.remove("d-none");    
            col2.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(col3)){
            col3.classList.remove("d-none");
            col3.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(float1)){
            float1.classList.remove("invisible");    
            float1.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(float2)){
            float2.classList.remove("invisible");
            float2.classList.add("animated","fadeInRight");
            }

            if (isInViewport(float3)){
            float3.classList.remove("invisible");
            float3.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(floattext1)){
            floattext1.classList.remove("invisible");
            floattext1.classList.add("animated","fadeInRight");
            }

            if (isInViewport(floattext2)){
            floattext2.classList.remove("invisible");
            floattext2.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(floattext3)){
            floattext3.classList.remove("invisible");
            floattext3.classList.add("animated","fadeInRight");
            }
            };
 