        //Customers Logo Slider

        $(document).ready(function() {
          $(".customer-logos").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 4
                }
              },
              {
                breakpoint: 520,
                settings: {
                  slidesToShow: 2
                }
              }
            ]
          });
        });

        //Customers Logo Slider End


        //Testimonial  Slider
          $(document).ready(function() {0.
            $(".clientTestimonial").slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              arrows: false,
              dots: false,
              pauseOnHover: true,
            });
          });
    
        //Testimonial Slider End 


                //about Logo Slider

              $(document).ready(function() {
                  $(".about-logos").slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 1500,
                    arrows: false,
                    dots: false,
                    pauseOnHover: false,
                    responsive: [
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 4
                        }
                      },
                      {
                        breakpoint: 520,
                        settings: {
                          slidesToShow: 2
                        }
                      }
                    ]
                  });
                });
        
        //about Logo Slider End


        //Career Slider
        $(document).ready(function() {
          $(".careerSlider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
          });
        });
        //Career Slider End



    //  caseStudy Heading

    var caseHeading = document.getElementById("caseHead");
    var caseBanner = document.getElementById("caseBanner");
    
    function changeText(event,bgcolor){
    console.log(event.target);
    caseHeading.innerHTML = event.target.innerHTML;
    caseBanner.style.background = bgcolor;
    }