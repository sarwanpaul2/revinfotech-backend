
$('.make-partner-slider').slick({
   centerMode: true,
   centerPadding: '0',
   slidesToShow: 5,
   dots: false,
   arrows: false,
   autoplay: true,
   responsive: [
      {
         breakpoint: 1199,
         settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3
         }
      },
      {
         breakpoint: 480,
         settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
         }
      }
   ]
});