<?php

use App\Helpers\Db as DbHelper;

return [

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "sendmail", "mailgun", "mandrill", "ses",
    |            "sparkpost", "postmark", "log", "array"
    |
    */
    'driver' => env('MAIL_DRIVER', 'smtp'),
    // 'driver' =>  (new DbHelper)->getSetting('MAIL_DRIVER'),
    
    /*
    |--------------------------------------------------------------------------
    | SMTP Host Address
    |--------------------------------------------------------------------------
    |
    | Here you may provide the host address of the SMTP server used by your
    | applications. A default option is provided that is compatible with
    | the Mailgun mail service which will provide reliable deliveries.
    |
    */
    'host' => env('MAIL_HOST', 'smtp.mailgun.org'),
    // 'host' => is_null($setting = (new DbHelper)->getSetting('MAIL_HOST')) ? $setting : $setting->setting_name,

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Port
    |--------------------------------------------------------------------------
    |
    | This is the SMTP port used by your application to deliver e-mails to
    | users of the application. Like the host we have set this value to
    | stay compatible with the Mailgun e-mail application by default.
    |
    */

    'port' => env('MAIL_PORT', 587),
    // 'port' => is_null($setting = (new DbHelper)->getSetting('MAIL_PORT')) ? $setting : $setting->setting_name,

    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all e-mails sent by your application to be sent from
    | the same address. Here, you may specify a name and address that is
    | used globally for all e-mails that are sent by your application.
    |
    */

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name' => env('MAIL_FROM_NAME', 'Example'),
    ],

    /*
    |--------------------------------------------------------------------------
    | E-Mail Encryption Protocol
    |--------------------------------------------------------------------------
    |
    | Here you may specify the encryption protocol that should be used when
    | the application send e-mail messages. A sensible default using the
    | transport layer security protocol should provide great security.
    |
    */

     'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    // 'encryption' => is_null($setting = (new DbHelper)->getSetting('MAIL_ENCRYPTION')) ? $setting : $setting->setting_name,

    /*
    |--------------------------------------------------------------------------
    | SMTP Server Username
    |--------------------------------------------------------------------------
    |
    | If your SMTP server requires a username for authentication, you should
    | set it here. This will get used to authenticate with your server on
    | connection. You may also set the "password" value below this one.
    |
    */
    'username' => env('MAIL_USERNAME'),
    'password' => env('MAIL_PASSWORD'),

    // 'username' => is_null($setting = (new DbHelper)->getSetting('MAIL_USERNAME')) ? $setting : $setting->setting_name,
    // 'password' => is_null($setting = (new DbHelper)->getSetting('MAIL_PASSWORD')) ? $setting : $setting->setting_name,

    /*
    |--------------------------------------------------------------------------
    | Sendmail System Path
    |--------------------------------------------------------------------------
    |
    | When using the "sendmail" driver to send e-mails, we will need to know
    | the path to where Sendmail lives on this server. A default path has
    | been provided here, which will work well on most of your systems.
    |
    */

    'sendmail' => '/usr/sbin/sendmail -bs',

    /*
    |--------------------------------------------------------------------------
    | Markdown Mail Settings
    |--------------------------------------------------------------------------
    |
    | If you are using Markdown based email rendering, you may configure your
    | theme and component paths here, allowing you to customize the design
    | of the emails. Or, you may simply stick with the Laravel defaults!
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Channel
    |--------------------------------------------------------------------------
    |
    | If you are using the "log" driver, you may specify the logging channel
    | if you prefer to keep mail messages separate from other log entries
    | for simpler reading. Otherwise, the default channel will be used.
    |
    */

    'ses' => [
        'key' => 'AKIAXSK5VBSDDL4ZNPUA',
        'secret' => 'BEFnmAJd6p9RUE31lzIR+v1yNy46k1etyUvVheDH9OUY',
        'region' => 'us-east-1',  // e.g. us-east-1
    ],

    'log_channel' => env('MAIL_LOG_CHANNEL'),
    'pretend' => false,

    'stream' => [
        'ssl' => [
          'allow_self_signed' => true,
          'verify_peer' => false,
          'verify_peer_name' => false,
        ],
    ],

];
