### About Revinfotech-website

Revinfotech-website is the Laravel PHP Based web application, providing an marketing platform to deal with Users.

[![Build Status](https://travis-ci.org/jeremykenedy/laravel-auth.svg?branch=master)](https://travis-ci.org/jeremykenedy/laravel-auth)
[![StyleCI](https://styleci.io/repos/44714043/shield?branch=master)](https://styleci.io/repos/44714043)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/jeremykenedy/laravel-auth/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/jeremykenedy/laravel-auth/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/jeremykenedy/laravel-auth/badges/build.png?b=master)](https://scrutinizer-ci.com/g/jeremykenedy/laravel-auth/build-status/master)
[![License](http://jeremykenedy.com/license-mit.svg)](https://raw.githubusercontent.com/jeremykenedy/laravel-auth/LICENSE)


## Installation instructions for developers
#### Configure your .env file for Laravel
````
# cp .env.example .env
# vi .env
````

## Check the .env 
````
````

#### Install the Laravel packages
````
# composer update
````
#### Install the npm packages (frontend)
Update the npm packages
````
# npm Install
````
#### Install the NodeJS 10 and NPM 5.6 packages (backend)
````
# sudo apt-get install python-software-properties
# curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
# sudo apt-get install nodejs
# sudo apt-get install build-essential
````
#### Install Laravel Echo server (broadcasting)
````
# npm install -g laravel-echo-server
# laravel-echo-server init
# laravel-echo-server start
````
#### Install Laravel Passport (Auth API)
````
# composer require laravel/passport
# php artisan migrate
# php artisan passport:install
# php artisan passport:keys
# php artisan vendor:publish --tag=passport-components
# php artisan passport:client --personal
````
#### Installing the cronjob (tasks)
````
# crontab -e
````
Put this content in the crontab file:
````
* * * * * /usr/bin/php /app/www/laravel/artisan schedule:run >> /dev/null 2>&1
````
#### Migrate the database (MYSQL)
````
#### before this command please import setting table in db otherwise migration will give error, if environment variable is configured according to setting table.
# php artisan migrate
````
#### Seed the database (MYSQL)
````
# php artisan db:seed
````
````
# php artisan db:seed --class=FeeSeeder (edit this class with required values)
````
#### Enable and Disable Coins, Enable withdrawal limit
````
# Table: Coins
# Fields: 'coin_enabled' & 'wallet_enabled' => 1 (default '0')
````
````
# Table: Coins
# Fields: 'coin_max_withdraw' & 'coin_min_withdraw' => <Required Value> (default '0')
# coin_max_withdraw => -1 for unlimited maximum value
# coin_min_withdraw => 0 means no minimum limit
````
#### Compile the application
````
# npm run dev
````
#### Generate the data.json file
````
# php artisan NextExchange:updatecryptocap
````

#### Use NPM Decimal for all javascript number calculations
````
# Refer link https://www.npmjs.com/package/decimal for tutorials.
````

#### Generate the market data cache
````
# php artisan NextExchange:cacheMarketData
````

### Maintainance
#### Update the firewall Regions once a month
````
# php artisan firewall:updategeoip
````

### Supervisord
#### Make the supervisord config
````
[program:next-echo-server]
process_name=%(program_name)s
command=laravel-echo-server start --dir=/srv/www/next.exchange
autostart=true
autorestart=true
user=ubuntu
redirect_stderr=true
stdout_logfile=/var/log/next-echo-server.log
````
````
# supervisorctl reread
# supervisorctl update
# supervisorctl start all
````

### Features
#### A [Laravel](http://laravel.com/) 5.8.x with minimal [Bootstrap](http://getbootstrap.com) 4.0.x project.


