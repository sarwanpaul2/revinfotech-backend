@extends('adminlte::page')

@section('title', 'Terms And Conditions')

@section('content_header')
    <h1>Terms And Conditions Records <a href="{{url('/termsconditionadd')}}"><button class="btn btn-primary">Add Terms And Condition</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered">
		<thead>
			<th>S.No.</th>
			<th>Title</th>
			<th>Meta Title</th>
			<th>Meta Description</th>
			<th>Keyword</th>
			<th>Description</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->title}}</td>
					<td>{{$value->meta_title}}</td>
					<td>{{$value->meta_description}}</td>
					<td>{{$value->keywords}}</td>
					<td>{!! $value->description !!}</td>
					<td>
						<a href="{{url('termscondition/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deleteTermsAndCondition(this,{{$value->id}})">Delete</button>
					</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/termsconditiondelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<script type="text/javascript">
		function deleteTermsAndCondition(delthis, id){
			$('#exampleModal').modal('show');
			$('#delbtn').val(id);

		}
	</script>
@endsection

