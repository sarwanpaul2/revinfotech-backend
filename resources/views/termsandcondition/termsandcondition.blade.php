@extends('adminlte::page')

@section('title', 'Terms And Conditions')

@section('content_header')
    <h1>Terms And Conditions</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/termscondition')}}">
		@csrf
	<div class="box-body">

			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Title">
			</div>
			
			<div class="form-group">
				<label for="meta_title">Meta Title</label>
					<textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter meta title"></textarea>
			</div>
			
			<div class="form-group">
				<label for="meta_description">Meta Description</label>
					<textarea class="form-control" id="meta_description" name="meta_description" rows="3" placeholder="Enter Meta Description"></textarea>
			</div>

			<div class="form-group">
				<label for="keywords">Keywords</label>
				<textarea class="form-control" id="keywords" name="keywords" rows="3" placeholder="Enter Keywords"></textarea>
			</div>
			
			<div class="form-group">
				<label for="desc1">Description</label>
				<textarea id="desc1" name="description_1" rows="10" cols="80" required="required"></textarea>
			</div>
			
		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush