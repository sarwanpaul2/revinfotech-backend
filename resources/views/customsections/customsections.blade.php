@extends('adminlte::page')

@section('title', 'Frontend Pages')

@section('content_header')
    <h1>Add Section</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('danger'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/sections/create')}}" enctype="multipart/form-data">
		@csrf
	    <div class="box-body">

			<div class="form-group">
				<label for="section_name">Section Name</label>
				<input type="text" class="form-control" id="section_name" name="section_name" placeholder="Enter section name" required="true">
			</div>
			<div class="form-group">
				<label for="section_description">Section Description</label>
				<input type="text" class="form-control" id="section_description" name="section_description" placeholder="Enter section description" >
			</div>
			<div class="form-group">
				<label for="field_name ">Field Name</label>
				<input type="text" class="form-control" id="slug" name="field_name" rows="3" placeholder="Enter field name" required="true">
			</div>
			<div class="form-group">
				<label for="field_type ">Field Type</label>
				{{-- <input type="text" class="form-control" id="field_type" name="field_type" rows="3" placeholder="Enter field type" required="true"> --}}
				<select class="form-control" name="field_type" id="field_type" required>
					<option value="text">Text</option>
					<option value="textarea">Textarea</option>	
					<option value="image">Image</option>
				</select>
			</div>
			<div class="form-group">
				<label for="field_description ">Field Description</label>
				<input type="text" class="form-control" id="field_description" name="field_description" rows="3" placeholder="Enter field description">
			</div>
			<div class="form-group">
				<label for="page_id ">Page</label>
				<select class="form-control" name="page_id" placeholder="Please select an option" required="true">
						{{-- <select class="mdb-select colorful-select dropdown-primary md-form" multiple searchable="Search here.."> --}}
						@foreach($pages_data as $value){
							<option value="{{$value->id}}">{!! $value->name !!}</option>
						@endforeach	
				</select>
			</div>
			<div class="form-group">
				<label for="status">Status</label>
				<select class="form-control" name="status" id="status">
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
			</div>
			
		</div>
		<!-- /.box-body -->
		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush