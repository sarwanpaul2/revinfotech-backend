@extends('adminlte::page')

@section('title', 'Blog-List')

@section('content_header')
    <h1>Edit Section</h1>
@stop

@section('content')
	
<form method="post" action="{{url('/sectionsupdate/'.$data->id)}}" enctype="multipart/form-data">
        @csrf
    <div class="form-group">
        <label for="section_name">Section Name</label>
        <input type="text" class="form-control" id="section_name" name="section_name" placeholder="Section Name" value="{{$data->section_name}}">
    </div>
    <div class="form-group">
        <label for="name">Section Description</label>
        <input type="text" class="form-control" id="section_description" name="section_description" placeholder="Section Description" value="{{$data->section_description}}">
    </div>
    <div class="form-group">
        <label for="field_name">Field Name</label>
        <input type="text" class="form-control" id="field_name" name="field_name" placeholder="Field Name" value="{{$data->field_name}}">
    </div>
    <div class="form-group">
        <label for="field_type">Field Type</label>
        {{-- <input type="text" class="form-control" id="field_type" name="field_type" placeholder="Field Type" value="{{$data->field_type}}"> --}}
        <select class="form-control" name="field_type" id="field_type" required >
            <option value="image" @if($data->field_type == 'image') selected @endif>Image</option>
            <option value="text"  @if($data->field_type == 'text') selected @endif>Text</option>
            <option value="textarea" @if($data->field_type == 'textarea') selected @endif>Textarea</option>
        </select>
    </div>
    <div class="form-group">
        <label for="field_description">Field Description</label>
        <input type="text" class="form-control" id="field_description" name="field_description" placeholder="Field Description" value="{{$data->field_description}}">
    </div>
    <div class="form-group">
        <label for="page_id">Page</label>
        {{-- <input type="text" class="form-control" id="page_id" name="page_id" placeholder="Page Id" value="{{$data->page_id}}"> --}}
        <select class="form-control" name="page_id" placeholder="Please select an option" required="true">
        @foreach($pages_data as $value){
            <option value="{{$value->id}}"  @if($value->id == $data->page_id) selected @endif>{!! $value->name !!}</option>
        @endforeach	
        </select>
    </div>
    <div class="form-group">
        <label for="status">Status</label>
        <select class="form-control" name="status" id="status">
            <option value="0"  @if($data->status == 0) selected @endif>Inactive</option>
            <option value="1" @if($data->status == 1) selected @endif>Active</option>
        </select>
    </div>

   
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush