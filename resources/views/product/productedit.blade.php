@extends('adminlte::page')

@section('title', 'Product-List')

@section('content_header')
    <h1>Edit Product</h1>
@stop

@section('content')
	
	<form method="post" action="{{url('/productupdate/'.$data->id)}}" enctype="multipart/form-data">
		@csrf
	<div class="box-body">

			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$data->title}}">
			</div>
			
			<div class="form-group">
				<label for="meta_title">Meta Title</label>
					<textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter meta title">{{$data->meta_title}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="keyword">Meta Description</label>
					<textarea class="form-control" id="keyword" name="meta_description" rows="3" placeholder="Enter Meta Description">{{$data->meta_description}}</textarea>
			</div>

			<div class="form-group">
				<label for="keyword">Meta KeyWord</label>
				<textarea class="form-control" id="keyword" name="keyword" rows="3" placeholder="Enter keyword">{{$data->keyword}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="desc1">Description 1</label>
				<textarea id="desc1" name="description_1" rows="10" cols="80" required="required">{{$data->description_1}}</textarea>
			</div>
			<div class="form-group">
				<label for="desc2">Description 2</label>
				<textarea id="desc2" name="description_2" rows="10" cols="80" required="required">{{$data->descritpion_2}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="desc3">Description 3</label>
				<textarea id="desc3" name="descritpion_3" rows="10" cols="80" required="required">{{$data->description_3}}</textarea>
			</div>

			<div class="form-group">
				<label for="meta_title">Image1</label>
				<input type="file" id="image_1" name="image_1" placeholder="image_1">
				@if($data->image_1)
				<img src="{{Storage::disk('s3')->url('images/'.$data->image_1)}}" width="100" height="100"/>
						@else
						<p>No Image</p>
				@endif
			</div>
			<div class="form-group">
				<label for="meta_title">Image2</label>
				<input type="file" id="image_2" name="image_2" placeholder="image_2">
				@if($data->image_2)
				<img src="{{Storage::disk('s3')->url('images/'.$data->image_2)}}" width="100" height="100"/>
				@else
				<p>No Image</p>
				@endif
			</div>
			<div class="form-group">
				<label for="meta_title">Image3</label>
				<input type="file" id="image_3" name="image_3" placeholder="image_3">
				@if($data->image_3)
				<img src="{{Storage::disk('s3')->url('images/'.$data->image_3)}}" width="100" height="100"/>
				@else
				<p>No Image</p>
				@endif
			</div>

			<div class="form-group">
				<label for="meta_title">Url</label>
				<input type="text" class="form-control" id="url" name="url" placeholder="url">
			</div>

		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
		CKEDITOR.replace('description_2');
		CKEDITOR.replace('descritpion_3');
	});
</script>
@endpush