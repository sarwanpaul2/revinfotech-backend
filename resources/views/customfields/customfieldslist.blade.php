@extends('adminlte::page')

@section('title', 'Frontend-Page-List')

@section('content_header')
    <h1>Fields List <a href="{{url('/fields/add')}}"><button class="btn btn-primary">Add/Edit Data</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="blogTable">
		<thead>
			<th>S.No.</th>
			<th>Section Name</th>
			<th>Page Name</th>
			<th>Field Data</th>
			<th>Status</th>
			{{-- <th>Action</th> --}}
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$section_result[$value->section_id]}}</td>
					<td>{{$page_name[$value->page_id]}}</td>
					<td width="700px">{{$value->field_data}}</td>
					{{-- <td>
						@if($value->image)
						<img src="{{Storage::disk('s3')->url('images/'.$value->image)}}" width="100" height="100"/>
						@else
						<p>No Image</p>
						@endif
					</td> --}}
					
					@if($value->status == 0)
					<td>Inactive</td>
					@else
					<td>Active</td>
					@endif
					{{-- <td>
						<a href="{{url('fields/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deleteBlog(this,{{$value->id}})">Delete</button>
					</td> --}}
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/frontendpagesdelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection
@push('js')


<script type="text/javascript">
	function deleteBlog(delthis, id){
		$('#exampleModal').modal('show');
		$('#delbtn').val(id);
	}
	$(function () {
		$('#blogTable').DataTable();
	});
</script>
@endpush

