@extends('adminlte::page')

@section('title', 'Frontend Pages')

@section('content_header')
    <h1>Frontend Fields</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/fields/create')}}" enctype="multipart/form-data">
		@csrf
		{{-- @php dd($pages);
		@endphp --}}
	    <div class="box-body">
			<div class="form-group">
				<label for="page_id">Page Name</label>
				<select class="form-control" name="page_id" id="page_id">
					{{-- <option value="0">Default</option> --}}
					@foreach($custom_sections as $value)
						<option value="{!! $value->page_id !!}">{{ $result[$value->page_id] }}</option>
					@endforeach
				</select>
			</div>
			<div id="allappend">
				{{-- <div class="form-group">
					<label for="section_id">Section Name</label>
					<select id = "section_id" name="section_id" class="form-control"></select>
				</div>
				<div class="form-group">
					<label for="field_data">Field Data</label>
					<textarea class="form-control" id="field_data" name="field_data" placeholder="Enter field data" required="true"></textarea>
				</div> --}}
			</div>
			
			<div class="form-group">
				<label for="status">Status</label>
				<select class="form-control" name="status" id="status">
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
			</div>
			
		</div>
		<!-- /.box-body -->
		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript">

    $(document).ready(function() {

        $('select[name="page_id"]').on('change', function() {

            var stateID = $(this).val();

            if(stateID) {

                $.ajax({

                    url: '/myform/'+stateID,

                    type: "GET",

                    dataType: "json",
					
                    success:function(data) {
// alert(data);
                        $('#allappend').empty();
						$('#allappend').append(data);

                    }

                });

            }else{

                $('#allappend').empty();

            }

        }).change();

    });

</script>
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush