@extends('adminlte::page')

@section('title', 'Index')

@section('content_header')
    <h1>Frontend Pages List <a href="{{url('/frontendpagesadd')}}"><button class="btn btn-primary">Add Pages</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="blogTable">
		<thead>
			<th>S.No.</th>
			<th>Name</th>
			<th>Description</th>
			<th>Image </th>
			<th>Slug</th>
			<th>Template Number</th>
			<th>Status</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->name}}</td>
					<td>{{$value->description}}</td>
					<td>
							@if($value->image)
							<img alt="Image not found" src="{{Storage::disk('s3')->url('images/'.$value->image)}}" width="100" height="100"/>
							@else
							<p>No Image</p>
							@endif
						</td>
					<td>{{$value->slug}}</td>
					<td>{{$value->template_id}}</td>
					@if($value->status == 0)
					<td>Inactive</td>
					@else
					<td>Active</td>
					@endif
					<td>
						<a href="{{url('frontendpages/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deleteBlog(this,{{$value->id}})">Delete</button>
					</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/frontendpagesdelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection
@push('js')


<script type="text/javascript">
	function deleteBlog(delthis, id){
		$('#exampleModal').modal('show');
		$('#delbtn').val(id);
	}
	$(function () {
		$('#blogTable').DataTable();
	});
</script>
@endpush

