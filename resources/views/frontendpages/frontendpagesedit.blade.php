@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Edit Blog</h1>
@stop

@section('content')
	
<form method="post" action="{{url('/frontendpagesupdate/'.$data->id)}}" enctype="multipart/form-data">
        @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Title" value="{{$data->name}}">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3" placeholder="Enter meta description">{{$data->description}}</textarea>
    </div>    
	<div class="box-body">
        <div class="form-group">
            <label for="meta_title">Image</label>
            <input type="file" id="image" name="image" placeholder="image">
            @if($data->image)
            <img src="{{Storage::disk('s3')->url('images/'.$data->image)}}" width="100" height="100"/>
            @else
            <p>No Image</p>
            @endif
        </div>
        <div class="form-group">
            <label for="slug">slug</label>
            <input type="text" class="form-control" id="slug" name="slug"  placeholder="Enter slug" value="{{$data->slug}}">
        </div>
        <div class="form-group">
		<label for="template_id">Template</label>
		<select class="form-control" name="template_id" placeholder="Please select an option" required="true">
        @foreach($template as $value){
            <option value="{{$value->id}}"  @if($value->id == $data->template_id) selected @endif>{!! $value->id !!}</option>
         @endforeach	
		</select>
	</div>
        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control" name="status" id="status">
                <option value="0"  @if($data->status == 0) selected @endif>Inactive</option>
                <option value="1" @if($data->status == 1) selected @endif>Active</option>
            </select>
        </div>

    </div>
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush