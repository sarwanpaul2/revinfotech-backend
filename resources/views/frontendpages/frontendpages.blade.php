@extends('adminlte::page')

@section('title', 'Add')

@section('content_header')
    <h1>Frontend Pages</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<label for="title">Post Title</label>
	<input id="title" type="text" class="@error('title') is-invalid @enderror">
	@error('title')
		<div class="alert alert-danger">{{ $message }}</div>
	@enderror

	<form method="post" action="{{url('/frontendpagescreate')}}" enctype="multipart/form-data">
		@csrf
	    <div class="box-body">

			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" required="true">
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<textarea class="form-control" id="description" name="description" placeholder="Enter description" required="true"></textarea>
			</div>
			<div class="form-group">
					<label for="image">Image</label>
					<input type="file" id="image" name="image" placeholder="image" required="true">
			</div>
			<div class="form-group">
				<label for="slug ">Slug</label>
				<input type="text" class="form-control" id="slug" name="slug" rows="3" placeholder="Enter slug" required="true">
			</div>
			<div class="form-group">
				<label for="template_id ">Template Id</label>
				<select class="form-control" name="template_id" placeholder="Please select an option" required="true">
						@foreach($template as $value){
							<option value="{{$value->id}}">{!! $value->id !!}</option>
						@endforeach	
				</select>
			</div>
			<div class="form-group">
				<label for="status">Status</label>
				<select class="form-control" name="status" id="status">
					<option value="0">Inactive</option>
					<option value="1">Active</option>
				</select>
			</div>
			
		</div>
		<!-- /.box-body -->
		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush