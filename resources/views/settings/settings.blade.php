@extends('adminlte::page')

@section('title', 'Settings')

@section('content_header')
    <h1>Settings</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/setting')}}">
		@csrf
		<div class="box-body">

			<div class="form-group">
				<label for="title">Slug</label>
				<input type="text" class="form-control" id="title" name="slug" placeholder="Slug" required>
			</div>
			
			<div class="form-group">
				<label for="label">Label</label>
				<input type="text" class="form-control" id="label" name="label" placeholder="Label" required>
			</div>

            <div class="form-group">
				<label for="value">Value</label>
				<input type="text" class="form-control" id="value" name="value" placeholder="Value" required>
			</div>

            <div class="form-group">
				<label for="helpertext">Helpertext</label>
				<input type="text" class="form-control" id="helpertext" name="helpertext" placeholder="Helper Text" required>
			</div>

            <div class="form-group">
				<label for="encrypted">Encrypted</label>
				<div class="radio">
                <label><input type="radio" name="encrypt" value="yes" checked>Yes</label>
                </div>
                <div class="radio">
                <label><input type="radio" name="encrypt" value="no">No</label>
                </div>
                
			</div>
		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

