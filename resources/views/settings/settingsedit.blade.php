@extends('adminlte::page')

@section('title', 'Portfolio-List')

@section('content_header')
    <h1>Edit Portfolio</h1>
@stop

@section('content')
	
	<form method="post" action="{{url('/settingupdate/'.$data->id)}}">
		@csrf
        <div class="box-body">

			<div class="form-group">
				<label for="title">Slug</label>
				<input type="text" class="form-control" id="title" name="slug" placeholder="Slug" value="{{$data->slug}}" required>
			</div>
			
			<div class="form-group">
				<label for="label">Label</label>
				<input type="text" class="form-control" id="label" name="label" placeholder="Label" value="{{$data->label}}" required>
			</div>

            <div class="form-group">
				<label for="value">Value</label>
				<input type="text" class="form-control" id="value" name="value" placeholder="Value" value="{{$data->value}}" required>
			</div>

            <div class="form-group">
				<label for="helpertext">Helpertext</label>
				<input type="text" class="form-control" id="helpertext" name="helpertext" placeholder="Helper Text" value="{{$data->helpertext}}" required>
			</div>

            <div class="form-group">
				<label for="encrypted">Encrypted</label>
				<div class="radio">
                <label><input type="radio" name="encrypt" value="yes" >Yes</label>
                </div>
                <div class="radio">
                <label><input type="radio" name="encrypt" value="no" >No</label>
                </div>
                
			</div>

		</div>
		<!-- /.box-body -->


		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

