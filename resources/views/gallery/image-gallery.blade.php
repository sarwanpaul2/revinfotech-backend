@extends('adminlte::page')
@section('title', 'Gallery')
@push('css')
    <!-- Latest compiled and minified CSS -->
    
    <!-- References: https://github.com/fancyapps/fancyBox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
   
   
    <style type="text/css">
    
    .gallery
    {
        display: inline-block;
        margin-top: 20px;
    }
    .close-icon{
    	border-radius: 50%;
        position: absolute;
        right: -10px;
        top: -10px;
        padding: 5px 8px;
    }
    .form-image-upload{
        background: #e8e8e8 none repeat scroll 0 0;
        padding: 15px;
        margin: 15px;
    }
    .img-style{
        width: 300px;
    }
    .word-break{
        word-break: break-all;
    }

    .thumbanailParent{
    display:flex;
    flex-wrap:wrap;
    }
    .thumbanailParent .thumbanailItem{
    width:100%;
    margin:auto;
    } 
    .thumbanailItem{
        position: relative;
    }

    @media(min-width:768px){
    .thumbanailParent .thumbanailItem{
    width:47%;    
    }
    }
    @media(min-width:992px){
    .thumbanailParent .thumbanailItem{
    width:23%;    
    }
    }
    </style>
@endpush
@section('content_header')
    <h3>Revinfotech - Image Gallery </h3>
@stop

@section('content')
    <!-- <title>Image Gallery Example</title> -->
<div class="">
   <div class="row">
   <form action="{{ url('image-gallery') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @elseif($message = Session::get('failure'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="row">
            <!-- <div class="col-md-5">
                <strong>Title:</strong>
                <input type="text" name="title" class="form-control" placeholder="Title">
            </div> -->
            <div class="col-md-5">
                <strong>Image:</strong>
                <input type="file" name="image[]" class="form-control" multiple>
            </div>
            <div class="col-md-2">
                <br/>
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
        </div>
    </form> 
</div>
    <div class="thumbanailParent">
    <!-- <div class='list-group gallery'> -->
            @if($images->count())
                @foreach($images as $value)
                <div class='thumbanailItem position-relative'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="{{Storage::disk('s3')->url('images/'.$value->image)}}">
                        <img class="img-fluid img-style" alt="Image N/A"  src="{{Storage::disk('s3')->url('images/'.$value->image)}}" />
                    </a>                   
                    <div class='thumbnail'>
                        <small>
                            @php
                                $image = Storage::disk('s3')->url('images/'.$value->image);
                                $data = getimagesize("$image");
                                echo "
                                    <p><strong>Title</strong> : ". $value->title ."</p>" .
                                " <p><strong>Dimensions</strong> : ". $data[3] ."</p>".
                                "<p><strong>Extension</strong> : " . $data['mime'] ."</p>".
                                "<p class='word-break'><strong>Link</strong> <span>: ".$image."</span></p>";
                                $myVar1 = htmlspecialchars("<img src=", ENT_QUOTES); 
                                $myVar2 = htmlspecialchars(">", ENT_QUOTES);
                                echo  "<p class='word-break'><strong>Image Tag : </strong>". $myVar1.'"'.$image.'"'.$myVar2."</p>";  
                            @endphp
                        </small>        
                    </div>
                        <!-- text-center / end -->
                       
                    
                    <form action="{{ url('image-gallery',$value->id) }}" method="POST">
                    <input type="hidden" name="_method" value="delete">
                    {!! csrf_field() !!}
                    <button type="submit" class="close-icon btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                    </form>
                </div> <!-- col-6 / end -->
                @endforeach
            @endif
        <!-- </div> list-group / end -->
       
    </div> <!-- row / end -->
    <div class="float-md-right">{{ $images->links() }}</div>
</div> <!-- container / end -->
@endsection
@push('js')

<script type="text/javascript">
    $(document).ready(function(){
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
@endpush