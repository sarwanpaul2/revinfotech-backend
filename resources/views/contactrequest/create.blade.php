@extends('adminlte::page')

@section('title', 'Add')

@section('content_header')
<h1>Add Identifier</h1>
@stop

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
</div>
@endif
<form method="post" action="{{ route('identifiers.store') }}" enctype="multipart/form-data">
@include('identifiers/_form')
	
</form>
@endsection

