@extends('adminlte::page')

@section('title', 'Index')

@section('content_header')
    <h1>Contact Request List 
		<!-- <a href="{{ route('identifiers.create') }}"><button class="btn btn-primary">Add Identifiers</button></a> -->
	</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('danger'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="blogTable">
		<thead>
			<th>S.No.</th>
			<th>Id</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Website</th>
			<th>Company</th>
			<th>Phone</th>
			<th>Page Name</th>
			<th>System Ip</th>
		</thead>
		<tbody>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->id}}</td>
					<td>{{$value->fist_name}}</td>
					<td>{{$value->last_name}}</td>
					<td>{{$value->email}}</td>
					<td>{{$value->website}}</td>
					<td>{{$value->company}}</td>
					<td>{{$value->phone}}</td>
					<td>{{$value->page_name}}</td>
					<td>{{$value->system_ip}}</td>
					<!-- <td>
						
						<a href="{{ route('identifiers.edit', $value->id) }}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deletePopupModal(this,{{$value->id}})">Delete</button> 
					</td> -->
					
				</tr>
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/identifiers/{identifier}/delete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection
@push('js')


<script type="text/javascript">
	function deletePopupModal(delthis, id){
		$('#exampleModal').modal('show');
		$('#delbtn').val(id);
	}
	$(function () {
		$('#blogTable').DataTable();
	});
</script>
@endpush

