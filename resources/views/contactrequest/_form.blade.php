<!-- single data form -->
<!-- <div class="box-body">
<div class="form-group">
	<label for="name">Name : </label>
	<input type="text" class="form-control" name="name" >
</div>
<div class="form-group">
	<label for="type ">Type</label>
	<select class="form-control" name="type" id="type" required>
		<option value="text">Text</option>
		<option value="file">File</option>	
		<option value="textarea">Textaraea</option>	
	</select>
</div>
<div class="form-group">
	<label for="template_id ">Template Id</label>
	<select class="form-control" name="template_id" placeholder="Please select an option" required="true">
			@foreach($templates as $value){
				<option value="{{$value->id}}">{{ $value->id}}</option>
			@endforeach	
	</select>
</div>
<div class="form-group">
	<label for="value">Value</label>
	<input type="text" class="form-control" name="value" required ="true" />
</div>
	
</div> -->
<!-- /.box-body -->
<!-- <button type="submit" class="btn btn-primary">Submit</button> -->
<!-- single data form -->

@push('css')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endpush
@csrf
    <div class="container">    
        <br/>
        <h3 align="center">Dynamically Add / Remove Identifier</h3>
        <br />
        <div class="table-responsive">
            <form method="post" id="dynamic_form" action="{{ route('identifiers.store') }}" enctype="multipart/form-data">
                <span id="result"></span>
                <table class="table table-bordered table-striped" id="user_table">
                    <thead>
                        <tr>
                            <th width="15%">Name</th>
                            <th width="15%">Type</th>
                            <th width="15%">Template Id</th>
                            <th width="15%">Value</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" align="right">&nbsp;</td>
                            <td>
                                @csrf
                                <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>

<script>
    $(document).ready(function(){

    var count = 1;

    dynamic_field(count);

    function dynamic_field(number)
    {
    html = '<tr>';
            html += '<td><input type="text" name="name[]" class="form-control" required placeholder="Please insert a identifier"/></td>';
            html += '<td><select class="form-control" name="type[]" id="type" required><option value="text">Text</option><option value="file">File</option><option value="textarea">Textaraea</option></select></td>';
            html += '<td><select class="form-control" name="template_id[]" id="template_id" required> @foreach($templates as $value){<option value="{{$value->id}}">{{ $value->id}}</option>@endforeach </select></td>';
            
            html += '<td><input type="text" name="value[]" class="form-control"  placeholder="Please insert a value" required/></td>';
            
            if(number > 1)
            {
                html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
                $('tbody').append(html);
            }
            else
            {   
                html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                $('tbody').html(html);
            }
    }

    $(document).on('click', '#add', function(){
    count++;
    dynamic_field(count);
    });

    $(document).on('click', '.remove', function(){
    count--;
    $(this).closest("tr").remove();
    });

    $('#dynamic_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url:'{{ route("dynamic-field.insert") }}',
                method:'post',
                data:$(this).serialize(),
                dataType:'json',
                beforeSend:function(){
                    $('#save').attr('disabled','disabled');
                },
                success:function(data)
                {
                    if(data.error)
                    {
                        var error_html = '';
                        for(var count = 0; count < data.error.length; count++)
                        {
                            error_html += '<p>'+data.error[count]+'</p>';
                        }
                        $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                    }
                    else
                    {
                        dynamic_field(1);
                        $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                    }
                    $('#save').attr('disabled', false);
                }
            })
    });

    });
</script>