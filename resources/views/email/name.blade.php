

@component('mail::message')

@php
$user_template = str_replace('{first_name}',$first_name,$user_template);
$user_template = str_replace('{last_name}',$last_name,$user_template);
$user_template = str_replace('{contact_num}',$contact_num,$user_template);
$user_template = str_replace('{email}',$email,$user_template);
$user_template = str_replace('{qualification}',$qualification,$user_template);
$user_template = str_replace('{experience}',$experience,$user_template);
$user_template = str_replace('{position}',$position,$user_template);
@endphp

@if (! empty($user_template))
{!!$user_template!!} <br>
@endif
@endcomponent