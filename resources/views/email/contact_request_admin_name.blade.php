
@component('mail::message')

@php
$user_template = str_replace('{first_name}',$first_name,$user_template);
$user_template = str_replace('{last_name}',$last_name,$user_template);
$user_template = str_replace('{email}',$email,$user_template);
$user_template = str_replace('{website}',$website,$user_template);
$user_template = str_replace('{company}',$company,$user_template);
$user_template = str_replace('{phone}',$phone,$user_template);
$user_template = str_replace('{details}',$details,$user_template);
@endphp

@if (! empty($user_template))
{!!$user_template!!} <br>
@endif
@endcomponent