@extends('adminlte::page')

@section('title', 'Case Study List')

@section('content_header')
    <h1>Case Study Records <a href="{{url('/casestudyadd')}}"><button class="btn btn-primary">Add Case Study</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered">
		<thead>
			<th>S.No.</th>
			<th>Title</th>
			<th>Meta Title</th>
			<th>Meta Description</th>
			<th>Keyword</th>
			<th>Description</th>
			<th>Image </th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->title}}</td>
					<td>{{$value->meta_title}}</td>
					<td>{{$value->meta_description}}</td>
					<td>{{$value->keyword}}</td>
					<td>{!! $value->description !!}</td>
					<td>
						@if($value->image)
						<img src="{{Storage::disk('s3')->url('images/'.$value->image)}}" width="100" height="100"/>
						@else
						<p>No Image</p>
						@endif
					</td>
					<td>
						<a href="{{url('casestudy/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deleteCaseStudy(this,{{$value->id}})">Delete</button>
					</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/casestudydelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<script type="text/javascript">
		function deleteCaseStudy(delthis, id){
			$('#exampleModal').modal('show');
			$('#delbtn').val(id);

		}
	</script>
@endsection

