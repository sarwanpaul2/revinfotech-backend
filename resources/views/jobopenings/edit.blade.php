@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Edit Section</h1>
@stop

@section('content')
	
<form method="post" action="{{ url('job-openings/'.$id.'/update') }}" enctype="multipart/form-data">
        @csrf
    
	<div class="form-group">
		<label for="position">Position</label>
		<input type="text" class="form-control" id="position" name="position" placeholder="Enter position" value="{{$data->position}}">
	</div>
	<div class="form-group">
		<label for="experience">Experience</label>
		<input type="number" class="form-control" id="experience" name="experience" placeholder="Enter Experience" value="{{$data->experience}}">
	</div>
	<div class="form-group">
		<label for="location">Location</label>
		<input type="text" class="form-control" id="location" name="location" placeholder="Enter Location" value="{{$data->location}}">
	</div>
	<div class="form-group">
		<label for="sharp_skills">Sharp Skills</label>
		<input type="text" class="form-control" id="sharp_skills" name="sharp_skills" placeholder="Enter sharp Skills" value="{{$data->sharp_skills}}">
	</div>
	<div class="form-group">
		<label for="skills">Skills</label>
		<input type="text" class="form-control" id="skills" name="skills" placeholder="Enter skills" value="{{$data->skills}}">
	</div>
	<div class="form-group">
		<label for="remark">Short Summary</label>
		<input type="text" class="form-control" id="remark" name="remark" placeholder="Enter Remark" value="{{$data->remark}}">
	</div>
	<div class="form-group">
		<label for="job_desc">Job Desc</label>
		<textarea class="form-control" id="job_desc" name="job_desc" rows="5" placeholder="Enter job_desc">{{$data->job_desc}}</textarea>
	</div>
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('job_desc',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)						[*]{*};button(*)[*]{*};main(*)[*]{*};section(*)[*]{*};h5(*)[*]{*};img(*)[*]{*};script(*)[*]{*}',
       });
	});
</script>
@endpush
