@csrf
<div class="box-body">
	<div class="form-group">
		<label for="Position">Position</label>
		<input type="text" class="form-control" id="position" name="position" placeholder="Enter position" required >
	</div>
	<div class="form-group">
		<label for="experience">Experience</label>
		<input type="number" class="form-control" id="experience" name="experience" placeholder="Enter Experience" required>
	</div>
	<div class="form-group">
		<label for="external_css">Location</label>
		<input type="text" class="form-control" id="location" name="location" placeholder="Enter Location" required>
	</div>
	<div class="form-group">
		<label for="sharp_skills">Sharp Skills</label>
		<input type="text" class="form-control" id="sharp_skills" name="sharp_skills" placeholder="Enter Skills" required>
	</div>
	<div class="form-group">
		<label for="skills">Skills</label>
		<input type="text" class="form-control" id="skills" name="skills" placeholder="Enter Skills" required>
	</div>
	<div class="form-group">
		<label for="remarks">Short Summary</label>
		<input type="text" class="form-control" id="remarks" name="remarks" placeholder="Enter Remarks" >
	</div>
	
	<div class="form-group">
		<label for="job_desc">Job Description</label>
		<textarea class="form-control" id="job_desc" name="job_desc" rows="10" placeholder="Enter job desciption" required></textarea>
	</div>
	
</div>
<!-- /.box-body -->
<button type="submit" class="btn btn-primary">Submit</button>

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('job_desc',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)						[*]{*};button(*)[*]{*};main(*)[*]{*};section(*)[*]{*};h(*)[*]{*}',
       });
	});
</script>
@endpush