@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Edit Menu</h1>
@stop

@section('content')
	
	<form method="post" action="{{url('/menuupdate/'.$data->id)}}" enctype="multipart/form-data">
		@csrf
		<div class="box-body">

			<div class="form-group">
				<label for="menu_name">Name</label>
				<input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="Manu Name" value="{{$data->menu_name}}">
			</div>
			<div class="form-group">
				<label for="menu_alias">Alias/Slug</label>
				<select class="form-control" name="menu_alias" placeholder="Please select an option">
						{{-- <select class="mdb-select colorful-select dropdown-primary md-form" multiple searchable="Search here.."> --}}
						@foreach($pages_data as $value){
							<option value="{{$value->id}}"@if($value->id == $data->menu_alias) selected @endif>{!! $value->slug !!}</option>
						@endforeach	
				</select>
			</div>
			
			<div class="form-group">
				<label for="parent_id">Parent Menu</label>
				<select class="form-control" name="parent_id" id="parent_id">
					<option value="0">Default</option>
					@foreach($menu_list as $value)
						<option value="{!! $value->id !!}" @if($value->id == $data->parent_id) selected @endif>{!! $value->menu_name !!}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="status">Status</label>
				<select class="form-control" name="status" id="status">
					<option value="0"  @if($data->status == 0) selected @endif>Inactive</option>
					<option value="1" @if($data->status == 1) selected @endif>Active</option>
				</select>
			</div>

		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection