@extends('adminlte::page')

@section('title', 'Add')

@section('content_header')
    <h1>Menu</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/menu')}}" enctype="multipart/form-data">
		@csrf
		<div class="box-body">

			<div class="form-group">
				<label for="menu_name">Name</label>
				<input type="text" class="form-control" id="menu_name" name="menu_name" placeholder="Menu Name">
			</div>
			<div class="form-group">
				<label for="menu_alias">Alias/Slug</label>
				<select class="form-control" name="menu_alias" placeholder="Please select an option" required="true">
						{{-- <select class="mdb-select colorful-select dropdown-primary md-form" multiple searchable="Search here.."> --}}
						@foreach($pages_data as $value){
							<option value="{{$value->id}}">{!! $value->slug !!}</option>
						@endforeach	
				</select>
			</div>
			
			<div class="form-group">
				<label for="parent_id">Parent Menu</label>
				<select class="form-control" name="parent_id" id="parent_id">
					<option value="0">Default</option>
					@foreach($menu_list as $value)
						<option value="{!! $value->id !!}">{!! $value->menu_name !!}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="status">Status</label>
				<select class="form-control" name="status" id="status">
					<option value="0">Inactive</option>
					<option value="1">Active</option>
				</select>
			</div>

		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection