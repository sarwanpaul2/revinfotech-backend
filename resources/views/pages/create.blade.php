@extends('adminlte::page')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Pages</h3>
	</div>

	

	@if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
	<!-- /.box-header -->
	<!-- form start -->
	<form method="post" action="{{ route('pages.store') }}" enctype="multipart/form-data">
	@csrf
		<div class="box-body">
			
			<div class="form-group">
				<label for="slug1">Title</label>
				<input type="text" class="form-control" name="title" placeholder="Title" >
			</div>
			<div class="form-group">
				<label for="meta_title">Meta Title</label>
					<textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter meta title"></textarea>
			</div>
			<div class="form-group">
				<label for="meta_description">Meta Description</label>
					<textarea class="form-control" id="meta_description" name="meta_description" rows="3" placeholder="Enter meta description"></textarea>
			</div>
			<div class="form-group">
				<label for="keyword">KeyWord</label>
					<textarea class="form-control" id="keyword" name="keyword" rows="3" placeholder="Enter keyword"></textarea>
			</div>
			<div class="form-group">
				<label for="desc1">Description 1</label>
				<textarea id="desc1" name="desc1" rows="10" cols="80"></textarea>
			</div>
			<div class="form-group">
				<label for="desc2">Description 2</label>
				<textarea id="desc2" name="desc2" rows="10" cols="80"></textarea>
			</div>
			
			<div class="form-group">
				<label for="desc3">Description 3</label>
				<textarea id="desc3" name="desc3" rows="10" cols="80"></textarea>
			</div>

			<div class="form-group">
				<label for="meta_title">Image1</label>
				<input type="file" id="image1" name="image1" placeholder="image1">
			</div>
			<div class="form-group">
				<label for="meta_title">Image2</label>
				<input type="file" id="image2" name="image2" placeholder="image2">
			</div>
			<div class="form-group">
				<label for="meta_title">Image3</label>
				<input type="file" id="image3" name="image3" placeholder="image3">
			</div>
			<!-- <div class="form-group">
				<label for="state">State</label>
				<input type="text" class="form-control" name="state" placeholder="state">
			<div class="form-group">
				<label for="state">City</label>
				<input type="text" class="form-control" name="city" placeholder="city" >
			</div>
			<div class="form-group">
				<label for="slug1">Slug1</label>
				<input type="text" class="form-control" name="slug1" placeholder="slug1" >
			</div>
			<div class="form-group">
				<label for="slug2">Slug2</label>
				<input type="text" class="form-control" name="slug2" placeholder="slug2" >
			</div> -->
		
		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	{{ Form::close() }}
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('desc1');
		CKEDITOR.replace('desc2');
		CKEDITOR.replace('desc3');
	});
</script>
@endpush