@extends('adminlte::page')

@section('title', 'Pages-List')

@section('content_header')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 

    <h1>Pages Records <a style="margin: 19px;" href="{{ route('pages.create')}}" class="btn btn-primary">Create Page</a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="myTable">
		<thead>
			<th>S.No.</th>
			<th>Title</th>
			<th>Url</th>
			<th>Meta Description</th>
			<th>Keyword</th>
      <th>State</th>
      <th>City</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->title}}</td>
					<td>{{$value->url}}</td>
					<td>{{$value->meta_description}}</td>
					<td>{{$value->keyword}}</td>
          <td>{{$value->state}}</td>
          <td>{{$value->city}}</td>
					
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/portfoliodelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection

@push('js')
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#myTable').DataTable();
	});
</script>
@endpush

