@extends('adminlte::page') 
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h2 class="box-title">Update Page</h2>
    </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br> 
        @endif
        <form method="post" action="{{ route('pages.update', $pages->id) }}" enctype="multipart/form-data">
            
            @csrf
            <div class="box-body">    
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" value={{ $pages->title }} />
                </div>

                <div class="form-group">
                    <label for="last_name">Meta Title:</label>
                    <textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter keyword">{{ $pages->meta_title }}</textarea>
                </div>

                <div class="form-group">
                    <label for="email">Keyword:</label>
                    <!-- <input type="text" class="form-control" name="keyword" value={{ $pages->keyword }} /> -->
                    <textarea class="form-control" id="keyword" name="keyword" rows="3" placeholder="Enter keyword">{{ $pages->keyword }}</textarea>
                </div>
                <div class="form-group">
                    <label for="desc1">Description 1</label>
                    <!-- <input type="text" class="form-control" name="desc1" value={{ $pages->desc1 }} /> -->
                    <textarea id="desc1" name="desc1" rows="10" cols="80" required="required">{{ $pages->desc1 }}</textarea>
                </div>
                
                
                <div class="form-group">
                    <label for="city">Description2:</label>
                    <!-- <input type="text" class="form-control" name="desc2" value={{ $pages->desc2 }} /> -->
                    <textarea id="desc2" name="desc2" rows="10" cols="80" required="required">{{ $pages->desc2 }}</textarea>
                </div>
                <div class="form-group">
                    <label for="city">Description3:</label>
                    <!-- <input type="text" class="form-control" name="desc3" value={{ $pages->desc3 }} /> -->
                    <textarea id="desc3" name="desc3" rows="10" cols="80" required="required">{{ $pages->desc3 }}</textarea>
                </div>
                <div class="form-group">
                    <label for="country">Images1:</label>
                    <input type="file"  name="image1" value={{ $pages->image1 }} />
                    <img title = "{{$pages->image1}}" class="profile-user-img img-responsive img-thumbnails "  src='{{Storage::disk('s3')->url('images/'.$pages->image1)}}'>
                    <p><strong>Image Name</strong> - {{ $pages->image1 }}</p>
                </div>
                <div class="form-group">
                    <label for="country">Images2:</label>
                    <input type="file"  name="image2" value={{ $pages->image2 }} />
                    <img title = "{{$pages->image2}}" class="profile-user-img img-responsive img-thumbnails "  src='{{Storage::disk('s3')->url('images/'.$pages->image2)}}'>
                    <p><strong>Image Name</strong> - {{ $pages->image2 }}</p>
                </div>
                <div class="form-group">
                    <label for="country">Images3:</label>
                    <input title = "{{$pages->image2}}" type="file"  name="image3" value={{ $pages->image3 }} />
                    <img title = "{{$pages->image3}}" class="profile-user-img img-responsive img-thumbnails "  src='{{Storage::disk('s3')->url('images/'.$pages->image3)}}'>
                    <p><strong>Image Name</strong> - {{ $pages->image3 }}</p>
                </div>
                <div class="form-group">
                    <label for="job_title">City:</label>
                    <input title = "{{$pages->image3}}" type="text" class="form-control" name="city" value={{ $pages->city }} />
                </div>
                <div class="form-group">
                    <label for="job_title">State:</label>
                    <input type="text" class="form-control" name="state" value={{ $pages->state }} />
                </div>
                <div class="form-group">
                    <label for="job_title">Slug1:</label>
                    <input type="text" class="form-control" name="slug1" value={{ $pages->slug1 }} />
                </div>
            
                
                <div class="form-group">
                    <label for="job_title">Slug2:</label>
                    <input type="text" class="form-control" name="slug2" value={{ $pages->slug2 }} />
                </div>
                
               
           
            
                <div class="form-group"> 
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('desc1');
		CKEDITOR.replace('desc2');
		CKEDITOR.replace('desc3');
	});
</script>
@endpush