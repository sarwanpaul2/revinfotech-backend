@extends('adminlte::page')

@section('title', 'Cookie Policy')

@section('content_header')
    <h1>Edit Cookie Policy</h1>
@stop

@section('content')
	
	<form method="post" action="{{url('/cookiepolicyupdate/'.$data->id)}}" enctype="multipart/form-data">
		@csrf
	<div class="box-body">

			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$data->title}}">
			</div>
			
			<div class="form-group">
				<label for="meta_title">Meta Title</label>
				<textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter meta title">{{$data->meta_title}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="keyword">Meta Description</label>
				<textarea class="form-control" id="keyword" name="meta_description" rows="3" placeholder="Enter Meta Description">{{$data->meta_description}}</textarea>
			</div>

			<div class="form-group">
				<label for="keyword">Meta KeyWord</label>
				<textarea class="form-control" id="keyword" name="keyword" rows="3" placeholder="Enter keyword">{{$data->keyword}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="desc1">Description</label>
				<textarea id="desc1" name="description_1" rows="10" cols="80" required="required">{{$data->description}}</textarea>
			</div>
			
			<div class="form-group">
				<label for="meta_title">Image</label>
				<input type="file" id="image" name="image" placeholder="image">
				@if($data->image)
				<img src="{{Storage::disk('s3')->url('images/'.$data->image)}}" width="100" height="100"/>
				@else
				<p>No Image</p>
				@endif
			</div>
		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush