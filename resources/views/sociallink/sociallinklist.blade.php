@extends('adminlte::page')

@section('title', 'Social Link List')

@section('content_header')
    <h1>Social Link Records <a href="{{url('/sociallinkadd')}}"><button class="btn btn-primary">Add Social Link</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered">
		<thead>
			<th>S.No.</th>
			<th>Facebook url</th>
			<th>Twitter url</th>
			<th>Youtube url</th>
			<th>Instagram url</th>
			<th>Google url</th>
			<th>Linkedin url</th>
			<th>Pinterest url</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->facebook_url}}</td>
					<td>{{$value->twitter_url}}</td>
					<td>{{$value->youtube_url}}</td>
					<td>{{$value->instagram_url}}</td>
					<td>{{$value->google_url}}</td>
					<td>{{$value->linkedin_url}}</td>
					<td>{{$value->pinterest_url}}</td>
					<td>
						<button class="btn btn-danger" onClick="deleteSocialLink(this,{{$value->id}})">Delete</button>
					</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/sociallinkdelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<script type="text/javascript">
		function deleteSocialLink(delthis, id){
			$('#exampleModal').modal('show');
			$('#delbtn').val(id);
		}
	</script>
@endsection

