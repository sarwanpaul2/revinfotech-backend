@extends('adminlte::page')

@section('title', 'Social Link')

@section('content_header')
    <h1>Social Link</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/sociallink')}}" enctype="multipart/form-data">
		@csrf
		<div class="box-body">
			<div class="form-group">
				<label for="url">Facebook url</label>
				<input type="text" class="form-control" id="facebookurl" name="facebookurl" placeholder="Facebook url">
			</div>
			<div class="form-group">
				<label for="url">Twitter url</label>
				<input type="text" class="form-control" id="twitterurl" name="twitterurl" placeholder="Twitter url">
			</div>
			<div class="form-group">
				<label for="url">Youtube url</label>
				<input type="text" class="form-control" id="youtubeurl" name="youtubeurl" placeholder="Youtube url">
			</div>
			<div class="form-group">
				<label for="url">Instagram url</label>
				<input type="text" class="form-control" id="instagramurl" name="instagramurl" placeholder="Instagram url">
			</div>
			<div class="form-group">
				<label for="url">Google+ url</label>
				<input type="text" class="form-control" id="googleurl" name="googleurl" placeholder="Google url">
			</div>
			<div class="form-group">
				<label for="url">Linkedin url</label>
				<input type="text" class="form-control" id="linkedin" name="linkedinurl" placeholder="Linkedin url">
			</div>
			<div class="form-group">
				<label for="url">Pinterest url</label>
				<input type="text" class="form-control" id="pinterest" name="pinteresturl" placeholder="Pinterest url">
			</div>


		</div>
		<!-- /.box-body -->
		<button type="submit" class="btn btn-primary">Submit</button>	
	</form>
@endsection
