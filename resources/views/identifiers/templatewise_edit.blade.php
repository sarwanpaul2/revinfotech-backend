@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Edit Section</h1>
@stop

@section('content')
	
<form method="post" action="{{ url('identifiers/'.$id.'/update') }}" enctype="multipart/form-data">
        @csrf
<div class="form-group">
	<label for="name">Name : </label>
	<input type="text" class="form-control" name="name" value="{{$data->name}}">
</div>
<div class="form-group">
	<label for="type ">Type</label>
	<select class="form-control" name="type" id="type" required >
        <option value="text" @if($data->type == 'text') selected @endif>Text</option>
        <option value="file"  @if($data->type == 'file') selected @endif>File</option>
        <option value="textarea"  @if($data->type == 'textarea') selected @endif>Textarea</option>		
	</select>
</div>
<div class="form-group">
	<label for="template_id ">template_id</label>
	<select class="form-control" name="template_id" placeholder="Please select an option" required="true">
		@foreach($templates as $value){
			<option value="{{$value->id}}"  @if($value->id == $data->template_id) selected @endif>{!! $value->id !!}</option>
		@endforeach	
	</select>
</div>
<div class="form-group">
	<label for="value">Value</label>
	<input type="text" class="form-control" name="value" required ="true" value="{{$data->value}}"/>
</div>
   
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush