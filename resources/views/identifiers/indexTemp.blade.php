@extends('adminlte::page')

@section('title', 'Index')

@section('content_header')
    <h1>Templatewise Identifiers List <a href="{{ route('identifiers.create') }}"><button class="btn btn-primary">Add Identifiers</button></a></h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('danger'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="blogTable">
		<thead>
			<th>S.No.</th>
			<th>Id</th>
			<th>Name</th>
			<th>Type</th>
			<th>value</th>
			<th>template</th>
			<th>Action</th>
		</thead>
		<tbody>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->id}}</td>
					<td>{{$value->name}}</td>
					<td>{{$value->type}}</td>
					<td>{{$value->value}}</td>
					<td>{{$value->template_id}}</td>
					<td style="width:205px;">
						
						
						<button class="btn btn-primary" onClick="quick_updateModal(this,{{$value->id,$value->name}})" data-identityname="{{$value->name}}"  data-identitytype="{{$value->type}}" data-identityvalue="{{$value->value}}" >Quick Edit</button> 
						<a href="{{ route('identifiers.templatewise_edit', $value->id) }}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deletePopupModal(this,{{$value->id}})">Delete</button> 
					</td>
					
				</tr>
			@endforeach
		</tbody>
	</table>
	<!-- Quick Update Modal -->
	<div class="modal fade" id="quick_updateModal" tabindex="-1" role="dialog" aria-labelledby="quick_updateModal" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Quick Update Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
		  <div class="modal-body">
	        Are you sure you want to Update this record?
	      </div>
		  <div class="modal-body">
		  <form method="POST" id="quick_update">
	      		@csrf
				  <input type="hidden" class="form-control" name="id" id="quick_update_id"> 
				<div class="form-group">
					<label for="name">Name: </label>
					<input type="text" class="form-control" name="name" id="name"> 
				</div>
				<!-- <div class="form-group">
					<label for="type">Type: </label>
					<input type="text" class="form-control" name="type" id="type"> 
				</div> -->
				<div class="form-group">
					<label for="value">Value: </label>
					<input type="text" class="form-control" name="value" id="value"> 
				</div>
	        	<button type="submit" id="update" name="submit" value="quick_update" class="btn btn-success">Update</button>
	        	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	      	</form>
			</div>
	      
	      <div class="modal-footer">
	      	
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/identifiers/{identifier}/delete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	
@endsection
@push('js')


<script type="text/javascript">
	function deletePopupModal(delthis, id){
		$('#exampleModal').modal('show');
		$('#delbtn').val(id);
	}
	function quick_updateModal(updatethis, id){
		$('#quick_updateModal').modal('show');
		$('#quick_update_id').val(id);
		$('#quickupbtn').val(id);
	//	var name = ;
		$('#name').val($(updatethis).data('identityname'));
		$('#type').val($(updatethis).data('identitytype'));
		$('#value').val($(updatethis).data('identityvalue'));

		var url = '/identifiers';
		var action = url+'/'+id+'/update'

		$('#quick_update').attr('action', action);

		console.log($(updatethis).data('identityname'));
	}
	$(function () {
		$('#blogTable').DataTable();
	});
</script>
@endpush

