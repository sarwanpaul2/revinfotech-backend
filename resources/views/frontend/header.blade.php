     
     <!-- Header Start -->
     <div class="cross-btn d-flex justify-content-end">
      <div class="hamburger" id="hamburger" onclick="sideNavBar()">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </div>
    </div>

    <section id="right-top-cross-menu">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-9 position-relative">
            <div class="row navigation">
              <div
                class="d-none d-xl-block col-xl-4 navigation-1-col text-center text-white"
              >
                <div
                  class="navigationInner d-flex flex-column justify-content-between py-4"
                >
                  <div class="navigationMenu py-3 px-2">
                    <h2 class="font-weight-bold ">Digital Core Capabilities</h2>
                    <p>Build vital capabilities to deliver digital outcomes.</p>
                  </div>
                  <div class="navigationImg">
                    <img src="/images/images_demo.png" class="img-responsive" />
                  </div>
                </div>
              </div>
              <div
                class="d-none d-xl-block col-xl-4 navigation-2-col text-center text-white"
              >
                <div
                  class="navigationInner d-flex flex-column justify-content-between py-4"
                >
                  <div class="navigationMenu py-3 px-2">
                    <h2 class="font-weight-bold">Digital Operating Models</h2>
                    <p>Adopt accelerators to evolve your way of working.</p>
                  </div>
                  <div class="navigationImg">
                    <img src="/images/images_demo.png" class="img-responsive" />
                  </div>
                </div>
              </div>
              <div
                class="d-none d-xl-block col-xl-4 navigation-3-col text-center text-white"
              >
                <div
                  class="navigationInner d-flex flex-column justify-content-between py-4"
                >
                  <div class="navigationMenu py-3 px-2">
                    <h2 class="font-weight-bold">
                      Empowering Talent Transformations
                    </h2>
                    <p>
                      Embrace the talent revolution to remain relevant in the
                      future.
                    </p>
                  </div>
                  <div class="navigationImg">
                    <img src="/images/images_demo.png" class="img-responsive" />
                  </div>
                </div>
              </div>
            </div>
            
            <div
              class="row industries w-100 position-absolute menuItem d-none"
              id="technology-menu"
            >
              <div class="arrow-back m-3 d-xl-none">
                <i
                  class="fas fa-lg fa-arrow-circle-left"
                  onclick="removeAll()"
                ></i>
              </div>

              <div class="col-lg-12">
                <div class="row add-sp-h">
                  <div class="col-lg-12 heading-ind">
                    <h1 class="indus-h1">Technology</h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <ul>
                      <!-- @foreach($menudata as $value) 
                        <li><a href="{{$menu_slugs[$value->menu_alias]}}">{{$value->menu_name}}</a></li>
                      @endforeach -->
                      <li>.NET Development</li>
                      <li>.NET Core Development</li>
                      <li>Devops</li>
                      <li>Python Panda</li>
                      <li>Plotly</li>
                      <li>IOT</li>
                    </ul>
                  </div>
                  <div class="col-md-4">
                    <ul>
                      <li>Virtual Reality</li>
                      <li>Cloud Computing</li>
                      <li>Hyperledger CaseStudy</li>
                      <li>Hyperledger Development</li>
                      <li>Business Inteligence</li>
                    </ul>
                  </div>
                  <!-- <div class="col-md-4">
                    <ul>
                      <li>Aerospace & Defense</li>
                      <li>Agriculture</li>
                      <li>Automotive</li>
                      <li>Communication Services</li>
                      <li>Consumer Packaged Goods</li>
                      <li>Education</li>  
                      <li>Financial Services</li>
                    </ul>
                  </div>
                  <div class="col-md-4">
                    <ul>
                      <li>Aerospace & Defense</li>
                      <li>Agriculture</li>
                      <li>Automotive</li>
                      <li>Communication Services</li>
                      <li>Consumer Packaged Goods</li>
                      <li>Education</li>
                      <li>Financial Services</li>
                    </ul>
                  </div> -->
                </div>
              </div>
            </div>

            <!-- 2nd window starts here -->
            <div
              class="row industries w-100 position-absolute menuItem d-none"
              id="industries-menu"
            >
              <div class="arrow-back m-3 d-xl-none">
                <i
                  class="fas fa-lg fa-arrow-circle-left"
                  onclick="removeAll()"
                ></i>
              </div>

              <div class="col-lg-12">
                <div class="row add-sp-h">
                  <div class="col-lg-12 heading-ind">
                    <h1 class="indus-h1">Industries</h1>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <ul>
                    
                     @php
                     
                     @endphp
                      @foreach($menudata as $value) 
                        <li><a href="{{$menu_slugs[$value->menu_alias]}}">{{$value->menu_name}}</a></li>
                      @endforeach
                      
                    
                      <li><a href="/front-industries/aerospace">Aerospace & Defensedsfdsf</a></li>
                      <li>Education</li>
                      <li>Banking</li>
                      <li>Transport</li>
                      <li>Food Restaurant</li>
                      <li>Ecommerce</li>
                      <li>Medical & Healthcare</li>
                    </ul>
                  </div>
                  <!-- <div class="col-md-4">
                    <ul>
                      <li>Aerospace & Defense</li>
                      <li>Agriculture</li>
                      <li>Automotive</li>
                      <li>Communication Services</li>
                      <li>Consumer Packaged Goods</li>
                      <li>Education</li>  
                      <li>Financial Services</li>
                    </ul>
                  </div>
                  <div class="col-md-4">
                    <ul>
                      <li>Aerospace & Defense</li>
                      <li>Agriculture</li>
                      <li>Automotive</li>
                      <li>Communication Services</li>
                      <li>Consumer Packaged Goods</li>
                      <li>Education</li>
                      <li>Financial Services</li>
                    </ul>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- 2nd window ends here -->

            <!-- 3rd window starts here -->
            <div
              class="row services w-100 position-absolute menuItem d-none"
              id="services-menu"
            >
              <div class="arrow-back m-3 d-xl-none">
                <i
                  class="fas fa-lg fa-arrow-circle-left"
                  onclick="removeAll()"
                ></i>
              </div>
              <div class="col-md-12">
                <div class="row add-sp-h">
                  <div class="col-md-12 heading-services">
                    <h1 class="services-h1">Services</h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Custom Web Development</li>
                      <li class="border-0"><a href="/front-services/services">Digital Marketing</a></li>
                      <li class="border-0">E-Commerce Development</li>
                      <li class="border-0">BPM</li>
                      <li class="border-0">CRM</li>
                      <li class="border-0">ICO</li>
                      <li class="border-0">UI/UX & Web Designing</li>
                      <li class="border-0">Blockchain Services</li>
                      <li class="border-0">ICO Marketing Services</li>
                    </ul>
                  </div>

                  <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Mobility</li>
                      <!-- <li class="border-0">BPM</li>
                      <li class="border-0">UI/UX & Web Designing</li>
                      <li class="border-0">UI/UX & Web Designing</li> -->
                    </ul>
                  </div>

                  <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Innovate</li>
                      <!-- <li class="border-0">Blockchain</li>
                      <li class="border-0">Engineering Services</li>
                      <li class="border-0">Internet of Things (IoT)</li> -->
                    </ul>
                  </div>

                  <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Accelerate</li>
                      <li class="border-0">Blockchain</li>
                      <li class="border-0">Engineering Services</li>
                      <li class="border-0">Internet of Things (IoT)</li>
                    </ul>
                  </div>

                  <!-- <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Assure</li>
                      <li class="border-0">Cyber Security</li>
                      <li class="border-0">Testing</li>
                    </ul>
                  </div>

                  <div class="col-6 col-md-4 col-lg-3">
                    <ul>
                      <li class="font-weight-bold">Assure</li>
                      <li class="border-0">Cyber Security</li>
                      <li class="border-0">Testing</li>
                    </ul>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- 3rd window Ends here -->

            <div
              class="row platforms w-100 position-absolute menuItem d-none"
              id="platform-menu"
            >
              <div class="arrow-back m-3 d-xl-none">
                <i
                  class="fas fa-lg fa-arrow-circle-left"
                  onclick="removeAll()"
                ></i>
              </div>
              <div class="col-md-12">
                <div class="row add-sp-h">
                  <div class="col-md-12 heading-platform">
                    <h1 class="platform-h1">Product</h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                      <li><a href="/front-tokenlauncher/tokenlauncher">Token Launcher</a></li>
                      <li>Crypto Exchange </li>
                      <li>Revinfotech Nia</li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <ul>
                      {{-- <li>Panaya</li> --}}
                      
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <!-- 4th window starts here -->
            <div
              class="row aboutus w-100 position-absolute d-none menuItem"
              id="aboutUs-menu"
            >
              <div class="arrow-back m-3 d-xl-none">
                <i
                  class="fas fa-lg fa-arrow-circle-left"
                  onclick="removeAll()"
                ></i>
              </div>
              <div class="col-md-12">
                <div class="row add-sp-h">
                  <div class="col-md-12 heading-aboutus">
                    <h1 class="aboutus-h1">About Us</h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <ul>
                      <li><a href="/front-aboutus/aboutus">About us</a></li>
                      <li><a href="/front-aboutus/leadership">leadership</a></li>
                      <li><a href="/front-aboutus/leadership">Partners</a></li>
                    </ul>
                  </div>
                  <div class="col-md-4">
                    <ul>
                      <li>Why Us</li>
                      <li><a href="/front-aboutus/leadership">Project Communication</a></li>
                      <li><a href="/front-aboutus/leadership">Our Engagement Models</a></li>
                      <li><a href="/"></a></li>
                    </ul>
                  </div>

                  <div class="col-md-4">
                    <ul>
                      {{-- <li>EdgeVerve</li> --}}
                      
                    </ul>
                  </div>

                  <div class="col-md-4">
                    <ul>
                      {{-- <li>EdgeVerve</li> --}}
                      
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- 4th window ends here -->
          </div>
          <!-- Main Navbar(Fixed) starts here -->
          <div class="col-xl-3 add-padding">
            <a href="/">
              <img
                class="img-fluid logo-revinfotech"
                src="/images/logo.png"
                alt=""
                id="companyLogo"
              />
          </a>
            <div class="right-top-cross-menu-navs">
              <!-- <a href="#"><h4 class="my-3">Navigate your next</h4></a> -->
              <!-- <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block mt-3 mb-1 navitem navitemLink"
                  onmouseenter="removeAll()"
                  >Navigate Next</a
                >
              </span> -->
              <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block  my-1 navitem navitemLink"
                  onmouseenter="showMenu('technology-menu')"
                  >Technology</a
                >
              </span>
              <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block  my-1 navitem navitemLink"
                  onmouseenter="showMenu('industries-menu')"
                  >Industries</a
                >
              </span>

              <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block  my-1 navitem navitemLink"
                  onmouseenter="showMenu('platform-menu')"
                  >Product</a
                >
              </span>

              <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block  my-1 navitem navitemLink"
                  onmouseenter="showMenu('services-menu')"
                  >Services</a
                >
              </span>

              <span class="d-block">
                <a
                  href="#"
                  class="d-inline-block  my-1 navitem navitemLink"
                  onmouseenter="showMenu('aboutUs-menu')"
                  >About Us</a
                >
              </span>
            </div>
            <div class="pees my-4">
              <span class="d-block navitemLink">
                <a href="#" class="d-inline-block" onmouseenter="removeAll()"
                  >Our Customers</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-gallery/gallery" class="d-inline-block" onmouseenter="removeAll()"
                  >Gallery</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="#" class="d-inline-block" onmouseenter="removeAll()"
                  >Investors</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-career/career" class="d-inline-block" onmouseenter="removeAll()"
                  >Careers</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-blogs/blog" class="d-inline-block" onmouseenter="removeAll()"
                  >Blogs</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-news/news" class="d-inline-block" onmouseenter="removeAll()"
                  >News</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-casestudy/casestudy" class="d-inline-block" onmouseenter="removeAll()"
                  >case studies</a
                >
              </span>
              <span class="d-block navitemLink">
                <a href="/front-contactus/contactus" class="d-inline-block" onmouseenter="removeAll()"
                  >Contact Us</a
                >
              </span>
            </div>
            <div class="fabicons" id="fabIcon">
              <a href="#"><i class="fab fa-linkedin-in"></i></a>
              <a href="#"><i class="fab fa-twitter mx-1"></i></a>
              <a href="#"><i class="fab fa-facebook-f mx-1"></i></a>
              <a href="#"><i class="fab fa-youtube mx-1"></i></a>
            </div>
          </div>
          <!-- Main Navbar(Fixed) Ends here -->
        </div>
      </div>
    </section>
    <!-- Header End-->