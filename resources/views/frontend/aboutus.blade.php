{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'About-us')

@push('css')
<!-- External CSS abouts-->

<link rel="stylesheet" href="/frontend/css/aboutus.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"     crossorigin="anonymous"/>
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
@endpush


{{-- This defines content/body section here --}}
@section('content')
@php

if($data != null && $page->status == 0 ){
    echo $data->layout;   
}else{
    
@endphp 

<div class="container-fluid aboutBanner py-3 d-flex align-items-center">
<div class="row">
<div class="contentContainer px-4 py-3">
<h1 class="mb-0">About Us</h1>

<hr class="afterHead my-2 mx-0" />
<p>Innovation needs to be part of your business culture. Consumers are transforming faster than we are, and if we don&rsquo;t catch up, we&rsquo;re in trouble.</p>
</div>
</div>
</div>

<div class="container storyContainer mt-5">
<div class="mx-2 text-center">
<h2>Our Vision</h2>

<hr class="afterHead mt-2  mb-3 mx-auto" />
<p class="text-center">Our vision is to work on the latest technologies for the betterment of multiple industries. We are empowering the team to create a new era of RevInfotech which will provide the best solutions to the businesses across the globe.</p>
</div>
</div>

<div class="container-fluid aboutBackground my-5 text-white d-flex align-items-center py-3">
<div>
<div class="row">
<div class="aboutBackgroundContent text-center">
<h2 class="text-center mb-4">Innovations Through Imagination</h2>

<p class="text-center">We are moving in the direction to become the most trusted and preferred IT offshore partner for Startups and Small to Large business Enterprises through our Innovation and 10 years of Industry experience.</p>
</div>
</div>

<div class="row py-3 mt-4">
<div class="col-6 col-lg-3">
<div class="aboutNumber text-center mb-3">
<h1>100 <span>+</span></h1>

<p>Lorem Ipsum</p>
</div>
</div>

<div class="col-6 col-lg-3">
<div class="aboutNumber text-center mb-3">
<h1>100 <span>+</span></h1>

<p>Lorem Ipsum</p>
</div>
</div>

<div class="col-6 col-lg-3">
<div class="aboutNumber text-center mb-3">
<h1>100 <span>+</span></h1>

<p>Lorem Ipsum</p>
</div>
</div>

<div class="col-6 col-lg-3">
<div class="aboutNumber text-center mb-3">
<h1>100 <span>+</span></h1>

<p>Lorem Ipsum</p>
</div>
</div>
</div>
</div>
</div>

<div class="container ourMission">
<div class="xmx-2">
<h2 class="text-center">Our Mission</h2>

<hr class="afterHead mt-2  mb-3 mx-auto" />
<p class="text-center">We are into this Industry from a decade. We are combining our solid business domain experience, technical expertise, profound knowledge of latest Industry trends and quality-driven delivery model, we deliver solutions that go beyond meeting customer expectations.</p>
</div>
</div>

<div class="container-fluid ourValue py-3 px-2 px-xl-4 my-5 text-white py-5">
<h2 class="text-center">Our Values</h2>

<hr class="afterHead mt-2  mb-3 mx-auto" />
<div class="container">
<div class="row mt-5">
<div class="col-md-4">
<div class="missionSection text-center"><img class="img-fluid" src="/images/belief-icon.png" />
<p>Our Belief</p>

<p class="text-center">Greatness demands reinvention</p>
</div>
</div>

<div class="col-md-4">
<div class="missionSection text-center"><img class="img-fluid" src="/images/talent-icon.png" />
<p>Our Talent</p>

<p class="text-center">Collaborative and driven</p>
</div>
</div>

<div class="col-md-4">
<div class="missionSection text-center"><img class="img-fluid" src="/images/clients-icon.png" />
<p>Our Clients</p>

<p class="text-center">Progressive and ambitious</p>
</div>
</div>
</div>
</div>
</div>
<!-- Our Client  -->

<div class="container my-5">
<h2 class="text-center">Our Partners</h2>

<hr class="afterHead mt-2  mb-4 mx-auto" />
<section class="about-logos slider">
<div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg" /></div>
</section>
</div>
<!-- Our Client End--><!-- Testimonial Slider -->

<div class="container-fluid mt-5 py-5 px-3" id="testimonialSlider">
<div class="container text-white">
<h2 class="text-center mb-4">Testimonial</h2>

<section class="clientTestimonial slider">
<div class="slide text-center">
<div class="testimonialItemInner">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>

<h5>Brain Lara</h5>
</div>
</div>

<div class="slide text-center">
<div class="testimonialItemInner">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>

<h5>Brain Lara</h5>
</div>
</div>

<div class="slide text-center">
<div class="testimonialItemInner">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>

<h5>Brain Lara</h5>
</div>
</div>

<div class="slide text-center">
<div class="testimonialItemInner">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>

<h5>Brain Lara</h5>
</div>
</div>
</section>
</div>
</div>
<!-- Testimonial Slider End --><!-- Certificate Section -->

<div class="container my-5">
<h2 class="text-center">Our Certificate</h2>

<hr class="afterHead mt-2 mb-4 mx-auto" />
<div class="row">
<div class="col-lg-6">
<div class="certificateSection"><img class="img-fluid img-thumbnail" src="/images/nscom-cert.jpg" /></div>
</div>

<div class="col-lg-6">
<div class="certificateSection"><img class="img-fluid img-thumbnail" src="/images/iso-cert.jpg" /></div>
</div>
</div>
</div>
<!-- Certificate Section End -->

@php
}

@endphp

@endsection  

@push('js')
 <!-- Client Side Script -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
<!-- Client Side Script End -->
@endpush