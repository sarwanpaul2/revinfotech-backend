{{-- This extends layout page proprty to home page  --}}
    @extends('/frontend/layouts/layout')
    @section('title', 'Token Launcher')

<!-- External CSS abouts-->
    @push('css')
        <link rel="stylesheet" href="/frontend/css/tokenlauncher.css"/>
    @endpush

{{-- This defines content/body section here --}}
@section('content') 
<!-- This is token launcher template -->
    <div class="toknLauncherBanner d-flex align-items-center justify-content-center">
    <div class="toknLauncherContent text-center text-white">
    <h1 class="font-weight-bold">Token Launcher Company</h1>
    <h2 class="font-weight-bolder my-3">Smart, Compliant And Audited</h2>
    <h4 class="mb-3">Token Sale Launch Platform Tailored for your Success</h4>
    <button type="button" class="px-4 py-2 font-weight-bolder mt-3 btn-orange">Get In Touch</button>
    </div>
    </div>

    <!-- feature -->
    <div class="container tokenFeature my-5">
    <div class="tokenFeatureHead">
    <h2 class="text-center font-weight-bold">Features Of Token Launcher Platform</h2>
        <hr class="headingBorder my-2"/>
    <p class="text-center">100% compliant tokens & a user-friendly platform geared for contributor engagement.</p>
    </div>
    <div class="row mt-4">
    <div class="col-md-6 col-lg-4 mb-4">
    <div class="featureItem position-relative">
    <img src="/images/smart.png" class="img-fluid" />
    <div class="featureContent position-absolute">
    <h5 class="font-weight-bolder">Smart</h5>
    <p>The client dashboard maintains a track of your Token Sale activity, and thus enables you to measure the results of your marketing campaigns and refine them.<p>
    </div>
    </div>
    </div>

    <div class="col-md-6 col-lg-4 mb-4">
        <div class="featureItem position-relative">
        <img src="/images/compliant.png" class="img-fluid" />
        <div class="featureContent position-absolute">
        <h5 class="font-weight-bolder">Compliant</h5>
        <p>We integrate easy-to-manage, customised KYC mechanisms in the Token Sale Launch platform to enable you to meet the requisite KYC compliance requirements.<p>
        </div>
        </div>
        </div>

        <div class="col-md-6 col-lg-4 mb-4">
            <div class="featureItem position-relative">
            <img src="/images/audited.png" class="img-fluid" />
            <div class="featureContent position-absolute">
            <h5 class="font-weight-bolder">Audited</h5>
            <p>We do extensive external penetration testing of the Token Sale website, and audit the Smart Contracts code, and recommend best practices for stronger Token Sale security.<p>
            </div>
            </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="featureItem position-relative">
                <img src="/images/crm.png" class="img-fluid" />
                <div class="featureContent position-absolute">
                <h5 class="font-weight-bolder">CRM Tools Integration</h5>
                <p>We create dashboards equipped with CRM tools for our clients which organize, track & manage all contributors’ information and gives the marketing & support team analytical reports of their engagement campaigns.<p>
                </div>
                </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="featureItem position-relative">
                    <img src="/images/user-friendly.png" class="img-fluid" />
                    <div class="featureContent position-absolute">
                    <h5 class="font-weight-bolder">User Friendly</h5>
                    <p>We keep our designs intuitive to ensure that visitors can easily navigate the Token Sale Launch Platform, register themselves and make contributions to the Token Sale.<p>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4">
                        <div class="featureItem position-relative">
                        <img src="/images/customised.png" class="img-fluid" />
                        <div class="featureContent position-absolute">
                        <h5 class="font-weight-bolder">Customised</h5>
                        <p>From contributor dashboard to Token Sale website to Smart Contract, every component of our Token Sale platform is customised to provide maximum convenience to you and contributors.<p>
                        </div>
                        </div>
                        </div>
    
    </div>
    </div>
    <!-- feature end -->
    
  
    <!--  How It Works -->
  
    <div class="processSection py-4" id="processSection">
        <div class="container">
        <h2 class="text-center font-weight-bold">How It Works</h2>
              <hr class="headingBorder my-2"/>
          <section class="roadMap-conatiner">
            <div class="roadmap-inner">
              <div class="page-header d-flex justify-content-center mb-3"></div>
              <ul class="timeline">
                <li>
                  <div
                    class="timeline-badge d-flex align-items-center justify-content-center"
                  >
                    <span>1</span>
                  </div>
                  <div class="timeline-panel" data-aos="fade-right">
                    <div class="timeline-heading">
                      <h5 class="timeline-title font-weight-bold mb-1">Get Whitelisted</h5>
                      <p class="text-justify">Register yourself for the Token Sale in advance to get whitelisted. Fill in the required personal details, and upload identity documents for the KYC process.</p>
                    </div>
                  </div>
                </li>
                <li class="timeline-inverted">
                  <div class="timeline-badge warning">
                   <span> 2 </span>
                  </div>
                  <div class="timeline-panel" data-aos="fade-left">
                    <div class="timeline-heading">
                        <h5 class="timeline-title font-weight-bold mb-1">Contribute</h5>
                        <p class="text-justify">Contribute in the Token Sale with fiat currencies such as USD, EUR, GBP, etc. or cryptocurrencies such as BTC, ETH, USDT, etc.</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div
                    class="timeline-badge d-flex align-items-center justify-content-center"
                  >
                    <span>3</span>
                  </div>
                  <div class="timeline-panel" data-aos="fade-right">
                    <div class="timeline-heading">
                        <h5 class="timeline-title font-weight-bold mb-1">Get Tokens</h5>
                        <p class="text-justify">Tokens are distributed either during the Token Sale or after the Token Sale finishes. Receive your tokens as soon as the token distribution process begins.</p>
                    </div>
                  </div>
                </li>
                <li class="timeline-inverted">
                  <div class="timeline-badge warning">
                      <span>4</span>
                  </div>
                  <div class="timeline-panel" data-aos="fade-left">
                    <div class="timeline-heading">
                        <h5 class="timeline-title font-weight-bold mb-1">Refer & Earn</h5>
                        <p class="text-justify">Get a personalised referral link to share with your friends and earn everytime they buy tokens using your referral link.</p>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </section>
        </div>
      </div>
    <!-- How It Works End -->
   
  
    <!-- Our Client -->
    <div class="ourClient my-5">
    <h2 class="text-center font-weight-bold">Our Clients</h2>
        <hr class="headingBorder my-2"/>
    <div class="logoSection d-flex justify-content-center mt-4">
    <div class="mx-1 mx-md-4">
    <img src="/images/bitbose.png" class="img-fluid">
    </div>
    <div class="mx-1 mx-md-4">
    <img src="/images/velorem.png" class="img-fluid">
    </div>
    </div>
    </div>
    <!-- Our Client End -->


  </body>
    <!-- Client Side Script -->
    <script src="js/navbar.js"></script>
    <script src="js/main.js"></script>
    <!-- Client Side Script End -->
    @endsection
