<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Synchrobit | Home</title>
   <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
   <!-- Font Awesome Icons -->
	<link
   rel="stylesheet"
   href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"
   integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ="
   crossorigin="anonymous"
 />
   <link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
   <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
   <div class="wrapper">
      <!-- header section -->
      <header class="header-wrap">
         <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light px-0">
               <a class="navbar-brand py-0" href="#">
                  <img src="./assets/images/logo-main.png" class="img-fluid" width="170" alt="Synchrobit Logo" />
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                     <li class="nav-item active">
                        <a class="nav-link" href="#">Markets <span
                              class="sr-only">(current)</span></a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#">Buy Crypto</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#">Trade</a>
                     </li>
   
                     <li class="nav-item">
                        <a class="nav-link" href="#" tabindex="-1"
                           aria-disabled="true">Academy</a>
                     </li>
                  </ul>
   
                  <div class="nav-links-right header-widget">
                     <div class="login-wrap">
                        <a href="#" class="login-link primary-text">
                           Login
                        </a>
                     </div>
                     <div class="register-wrap">
                        <a href="#" class="signup-link btn primary-outline-btn">
                           Register
                        </a>
                     </div>
                     <div class="dropdown select-dropdown d-inline-block">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           English
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">English</a>
                           <a class="dropdown-item" href="#">French</a>
                           <a class="dropdown-item" href="#">Italian</a>
                        </div>
                        </div>
                  </div>
               </div>
            </nav>
         </div>
      </header>
      <main>
         <!-- Landing Page Banner -->
         <section class="landing_Banner d-flex align-items-center">
            <div class="container banner_Content text-white pt-5 customContainer">
               <h1 class="text-center text-white main-title">Global Blockchain Asset Tranding Center</h1>
               <p class="text-center mt-3 text-white sub-title">
                  Provide blockchain asset services to global users, covering the core
                  areas of blockchain digital assets such as spot trading,
                  <span class="d-lg-block">technology research and development</span>
               </p>
         
               <div class="row synchroCategeory pt-5">
                  <div class="col-md-6 col-lg-3 synchroCategeorParent mb-3">
         
                     <img src="assets/images/card_4.svg" class="img-fluid d-block mx-auto" />
         
                  </div>
                  <div class="col-md-6 col-lg-3 synchroCategeorParent mb-3">
         
                     <img src="assets/images/card_1.svg" class="img-fluid d-block mx-auto" />
         
                  </div>
                  <div class="col-md-6 col-lg-3 synchroCategeorParent mb-3">
         
                     <img src="assets/images/card_2.svg" class="img-fluid d-block mx-auto" />
         
                  </div>
                  <div class="col-md-6 col-lg-3 synchroCategeorParent mb-3">
         
                     <img src="assets/images/card_3.svg" class="img-fluid d-block mx-auto" />
         
                  </div>
               </div>
            </div>
         </section>
         <!-- New Listing Token -->
         <section class="container newly_Listed customContainer">
            <h2 class="text-center mb-4 pb-2 font-weight-bold main-title">
               Newly Listed Token
            </h2>
            <div class="row">
               <div class="col-md-6 col-lg-3 newly_ListedContainer position-relative">
                  <div class="newly_ListedItem text-center pb-3">
                     <p class="ListedItem_Date py-3 px-2">2020-01-02</p>
                     <img src="assets/images/tron.svg" class="img-fluid mt-5" />
                     <h4 class="mt-3">TRON</h4>
                     <p class="fw-500">
                        Current Price: <span class="currentPrice d-block d-xl-inline">$0.0143537474</span>
                     </p>
                     <a href="#" class="tradeNow">Trade Now <i class="fas fa-angle-right"></i></a>
                  </div>
               </div>
               <div class="col-md-6 col-lg-3 newly_ListedContainer position-relative">
                  <div class="newly_ListedItem text-center pb-3">
                     <p class="ListedItem_Date py-3 px-2">2020-01-02</p>
                     <img src="assets/images/ripple.svg" class="img-fluid mt-5" />
                     <h4 class="mt-3">Ripple</h4>
                     <p class="fw-500">
                        Current Price: <span class="currentPrice d-block d-xl-inline">$0.0143537474</span>
                     </p>
                     <a href="#" class="tradeNow">Trade Now <i class="fas fa-angle-right"></i></a>
                  </div>
               </div>
               <div class="col-md-6 col-lg-3 newly_ListedContainer position-relative">
                  <div class="newly_ListedItem text-center pb-3">
                     <p class="ListedItem_Date py-3 px-2">2020-01-02</p>
                     <img src="assets/images/steller.svg" class="img-fluid mt-5" />
                     <h4 class="mt-3">Steller</h4>
                     <p class="fw-500">
                        Current Price: <span class="currentPrice d-block d-xl-inline">$0.0143537474</span>
                     </p>
                     <a href="#" class="tradeNow">Trade Now <i class="fas fa-angle-right"></i></a>
                  </div>
               </div>
               <div class="col-md-6 col-lg-3 newly_ListedContainer position-relative">
                  <div class="newly_ListedItem text-center pb-3">
                     <p class="ListedItem_Date py-3 px-2">2020-01-02</p>
                     <img src="assets/images/binance.svg" class="img-fluid mt-5" />
                     <h4 class="mt-3">Binance</h4>
                     <p class="fw-500">
                        Current Price: <span class="currentPrice d-block d-xl-inline">$0.0143537474</span>
                     </p>
                     <a href="#" class="tradeNow">Trade Now <i class="fas fa-angle-right"></i></a>
                  </div>
               </div>
            </div>
            <button type="button" class="d-block mx-auto mt-3 primary-outline-btn px-3 py-1">
               View More
            </button>
         </section>
         <!-- Coin Table -->
         <section class="container coinTableContainer customContainer">
            <div class="coinNav d-flex flex-column flex-md-row justify-content-md-between align-items-md-center">
               <div class="listContainer">
                  <ul class="pl-0 mb-0">
                     <li class="activeItem">24H Rising list</li>
                     <li>Favourite</li>
                     <li>USDT</li>
                     <li>BTC</li>
                     <li>TRX</li>
                     <li>ETH</li>
                  </ul>
               </div>
               <div class="searchBox my-2 my-lg-0">
                  <input type="text" placeholder="Search" class="searchIcon" />
               </div>
            </div>
         
            <div class="coinTable table-responsive">
               <table class="table table-borderless">
                  <thead>
                     <tr>
                        <th>Pair <i class="fas fa-exchange-alt"></i></th>
                        <th>Last Price</th>
                        <th>24h Change <i class="fas fa-exchange-alt"></i></th>
                        <th>24h High</th>
                        <th>24h Low</th>
                        <th class="text-right">24h Volume <i class="fas fa-exchange-alt"></i></th>
                        <th class="text-right">Chart</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="high_rate"><span class="d-inline-block">- 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="low_rate"><span class="d-inline-block">+ 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="high_rate"><span class="d-inline-block">- 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="low_rate"><span class="d-inline-block">+ 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="low_rate"><span class="d-inline-block">+ 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                     <tr>
                        <td>ETH/BTC</td>
                        <td>0.017300/<span class="text-light-grey"> 144.29USD</span></td>
                        <td class="low_rate"><span class="d-inline-block">+ 4.61 %</span></td>
                        <td>0.018157</td>
                        <td>0.011722</td>
                        <td class="text-right">9041.865155 BTC</td>
                        <td><img src="assets/images/graphIcon.png" class="d-block ml-auto" /></td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <button type="button" class="d-block mx-auto mt-3 primary-outline-btn px-3 py-1">
               Explore Market
               <i class="fas fa-long-arrow-alt-right pl-2"></i>
         </section>
         <!-- Get Started in few Minutes -->
         <section class="getStarted pt-5">
            <div class="container">
               <h2 class="text-white text-center pt-2 main-title">Get Started in a few minutes</h2>
               <div class="row mt-5">
                  <div class="col-xl-4">
                     <div class="getStartedItem">
                        <img src="assets/images/createAccount.png" class="d-block mx-auto">
                        <div class="getStartedContent mt-3">
                           <img src="assets/images/arrowDown.svg" class="img-fluid d-block mx-auto">
                           <h3 class="text-center mt-3">Create An Account</h3>
                        </div>
                     </div>
                  </div>
         
                  <div class="col-xl-4">
                     <div class="getStartedItem">
                        <img src="assets/images/doneKyc.png" class="d-block mx-auto">
                        <div class="getStartedContent mt-3">
                           <img src="assets/images/arrowDown.svg" class="img-fluid d-block mx-auto">
                           <h3 class="text-center mt-3">Done Your KYC</h3>
                        </div>
                     </div>
                  </div>
         
                  <div class="col-xl-4">
                     <div class="getStartedItem">
                        <img src="assets/images/startBuying.png" class="d-block mx-auto">
                        <div class="getStartedContent mt-3">
                           <img src="assets/images/arrowDown.svg" class="img-fluid d-none d-xl-block mx-auto">
                           <h3 class="text-center mt-3">Start Buying & Selling</h3>
                        </div>
                     </div>
                  </div>
               </div>
         
               <div class="row bitcoinTransactionParent pb-2 mb-5">
                  <div class="col-lg-6">
                     <div class="bitcoinTransaction mb-5 mb-lg-0">
                        <div class="bitcoinTransactionInner">
                           <img src="assets/images/buySell_Bitcoin.svg" class="img-fluid d-block mx-auto">
                           <h3 class="text-center">Buy/Sell Bitcoins</h3>
                           <p class="text-white text-center">SynchroBit™ is a place for everyone who wants to simply buy and sell
                              Bitcoins. Deposit funds using your Visa/MasterCard or bank transfer. Instant buy/sell of Bitcoins
                              at fair price is guaranteed. Nothing extra. Join over 3,000,000 customers from all over the world
                              satisfied with our services.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="bitcoinTransaction">
                        <div class="bitcoinTransactionInner">
                           <img src="assets/images/bitcoin_Trading.svg" class="img-fluid d-block mx-auto">
                           <h3 class="text-center">Bitcoin Trading</h3>
                           <p class="text-white text-center">Advanced order-matching algorithms are backed by unique trade
                              engine. Orderbook liquidity allows to execute high-volume orders and apply market making trading,
                              high frequency trading and scalping strategies. Compliant with security standards, we guarantee
                              safety of your assets and data.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- features section -->
         <section class="features-section">
            <div class="container">
               <div class="title-wrap text-center">
                  <h2 class="main-title mb-3">SYNCHROBIT™ Features</h2>
                  <h4 class="sub-title col-9 mx-auto">SynchroBit™ is not a me-too platform! Developed based on the leading-edge hybrid blockchain technology by SYNCHRONIUM®,</h4>
               </div>
               <div class="features-wrap d-flex flex-wrap ">
                  <div class="feature-content">
                     <div class="row">
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature1.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">SAFE AND SECURE</h4>
                                 <p class="mb-0">Benefiting from the leading-edge hybrid wallet technology, SynchroBit™ doesn’t hold any assets or private keys on escrow!</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature2.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">INSTANT TRADING</h4>
                                 <p class="mb-0">SynchroBit™ core matching technology enables +200K instant pairs per second!</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature3.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">TRADE LIKE A PRO</h4>
                                 <p class="mb-0">SynchroBit™ is not just another exchange! It provides the users with smart trading solutions!</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature4.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">EXTENSIVE MARKETS</h4>
                                 <p class="mb-0">Trade with extensive pairs, including US$, Euro, GBP, BTC, ETH, BCH, EOS, XRP, XLM, DASH, ZEC, LTC, SNB, USDⓈ markets!</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature5.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">DIVERSITY AND SIMPLICITY</h4>
                                 <p class="mb-0">Enjoy diversified trading solutions, including spot, margin, futures, swap, and many more all in one place!</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 mb-4">
                           <div class="feature-item media">
                              <img src="assets/images/feature6.png" class="mr-3" width="52" class="img-fluid" alt="feature image">
                              <div class="media-body">
                                 <h4 class="mb-2">BUY AND SELL CRYPTO</h4>
                                 <p class="mb-0">Buy and Sell Crypto with MasterCard and Visa Card, and enjoy instant SWAP between your crypto
                                    assets!</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="feature-image">
                     <div class="image-sec text-center text-xl-right">
                        <img src="assets/images/featureimg1.png" width="400" class="img-fluid d-inline-block" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- upcoming platforms -->
         <section class="upcoming-platform text-center">
            <div class="container">
               <div class="title-wrap mb-4">
                  <h2 class="main-title mb-3">SYNCHROSPHERE™ UPCOMING PLATFORMS</h2>
                  <h4 class="sub-title col-lg-9 px-0 mx-auto">SynchroSphere™ is the innovative hybrid blockchain-based ecosystem which benefits from leading-edge technologies including SynchroLedger™ and SynchroNet™.</h4>
               </div>
               <div class="platform-grid-section row">
                  <div class="col-md-6 col-lg-3 mb-4">
                     <div class="grid-item">
                        <div class="image-wrap">
                           <a href="#">
                              <img src="assets/images/platform1.png" class="img-fluid" width="148" alt="">
                           </a>
                        </div>
                        <div class="grid-content">
                           <h3 class="font-weight-bold grid-title"><a href="">Winkpay</a></h3>
                           <p class="mb-0">Blockchain Powered Payment wallet system to power cross border payments.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-3 mb-4">
                     <div class="grid-item">
                        <div class="image-wrap">
                           <a href="#">
                              <img src="assets/images/platform2.png" class="img-fluid" width="148" alt="">
                           </a>
                        </div>
                        <div class="grid-content">
                           <h3 class="font-weight-bold grid-title"><a href="">Pikchat</a></h3>
                           <p class="mb-0">Blockchain Powered Messaging system for facilitating communications around the world.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-3 mb-4">
                     <div class="grid-item">
                        <div class="image-wrap">
                           <a href="#">
                              <img src="assets/images/platform3.png" class="img-fluid" width="148" alt="">
                           </a>
                        </div>
                        <div class="grid-content">
                           <h3 class="font-weight-bold grid-title"><a href="">Agora</a></h3>
                           <p class="mb-0">Blockchain powered Augmented Reality platform and solutions.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-3 mb-4">
                     <div class="grid-item">
                        <div class="image-wrap">
                           <a href="#">
                              <img src="assets/images/platform4.png" class="img-fluid" width="148" alt="">
                           </a>
                        </div>
                        <div class="grid-content">
                           <h3 class="font-weight-bold grid-title"><a href="">SynchroLedger™</a></h3>
                           <p class="mb-0">SynchroSphere™ is a blockchain based ecosystem, innovated by SYNCHRONIUM®</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- partners section -->
         <section class="partners-section text-center">
            <div class="container">
               <div class="title-wrap mb-5">
                  <h2 class="main-title">OUR PARTNERS</h2>
               </div>
               <div class="make-partner-slider">
                  <div class="partner-item">
                     <div class="image-wrap">
                        <img src="assets/images/partner1.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner1-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
                  <div class="partner-item">
                     <div class="image-wrap">
                        <img src="assets/images/partner2.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner2-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
                  <div class="partner-item">
                     <div class="image-wrap">
                        <img src="assets/images/partner3.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner3-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
                  <div class="partner-item">                     
                     <div class="image-wrap">
                        <img src="assets/images/partner4.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner4-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
                  <div class="partner-item">                     
                     <div class="image-wrap">
                        <img src="assets/images/partner5.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner5-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
                  <div class="partner-item">                     
                     <div class="image-wrap">
                        <img src="assets/images/partner4.png" width="232" class="img-fluid d-inline-block" alt="Partner">
                        <img src="assets/images/partner4-yellow.png" width="232" class="img-fluid d-inline-block image-yellow" alt="Partner">
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <!-- footer -->
      <footer class="footer-wrap">
         <div class="container">
            <div class="join-comm">
               <div class="row align-items-center">
                  <div class="col-lg-8">
                     <div class="section-content">
                        <h2 class="mb-3 main-title">Join the Community</h2>
                        <div class="sub-title">SYNCHROBIT™ is global. Join the conversation in any of our worldwide communities.</div>
                        <ul class="social-list list-unstyled">
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/facebook.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/facebook-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/linkedin.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/linkedin-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/social1.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/social-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/github.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/git-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/insta.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/insta-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/bitcoin.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/bitcoin-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/send.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/send-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/twitter.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/twitter-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                           <li>
                              <a class="image-wrap" href="#">
                                 <img src="assets/images/youtube.svg" width="48" height="48" class="img-fluid">
                                 <img src="assets/images/youtube-yellow.svg" width="48" height="48" class="img-fluid image-white">
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="image-wrap text-lg-right">
                        <img src="assets/images/socialimage.png" width="329" class="img-fluid" alt="">
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-main">
               <div class="row">
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">About</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">About Us</a>
                        </li>
                        <li>
                           <a href="#">How it Works</a>
                        </li>
                        <li>
                           <a href="#">SYNCHONIUM®</a>
                        </li>
                        <li>
                           <a href="#">Join Us</a>
                        </li>
                        <li>
                           <a href="#">Contact Us</a>
                        </li>
                     </ul>
                  </div>
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">Features</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">Quick Trade</a>
                        </li>
                        <li>
                           <a href="#">Advanced Trade</a>
                        </li>
                        <li>
                           <a href="#">Buy Crypto with Fiat</a>
                        </li>
                        <li>
                           <a href="#">SWAP Trade</a>
                        </li>
                        <li>
                           <a href="#">Margin Trading</a>
                        </li>
                        <li>
                           <a href="#">Lending</a>
                        </li>
                        <li>
                           <a href="#">PAPM™</a>
                        </li>
                        <li>
                           <a href="#">Futures</a>
                        </li>
                        <li>
                           <a href="#">Social Trading</a>
                        </li>
                        <li>
                           <a href="#">APPS</a>
                        </li>
                     </ul>
                  </div>
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">Legal</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">Terms of services</a>
                        </li>
                        <li>
                           <a href="#">Privacy Policy</a>
                        </li>
                        <li>
                           <a href="#">GDPR</a>
                        </li>
                        <li>
                           <a href="#">Security</a>
                        </li>
                        <li>
                           <a href="#">KYC</a>
                        </li>
                        <li>
                           <a href="#">AML</a>
                        </li>
                     </ul>
                  </div>
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">Trading</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">Fee Schedule</a>
                        </li>
                        <li>
                           <a href="#">Markets</a>
                        </li>
                        <li>
                           <a href="#">Assets</a>
                        </li>
                        <li>
                           <a href="#">API Documentation</a>
                        </li>
                        <li>
                           <a href="#">Academy</a>
                        </li>
                        <li>
                           <a href="#">Account Manager</a>
                        </li>
                        <li>
                           <a href="#">Leanding Contacts</a>
                        </li>
                     </ul>
                  </div>
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">Support</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">News</a>
                        </li>
                        <li>
                           <a href="#">User Guide</a>
                        </li>
                        <li>
                           <a href="#">Help Center</a>
                        </li>
                        <li>
                           <a href="#">FAQ</a>
                        </li>
                        <li>
                           <a href="#">Newsletter</a>
                        </li>
                     </ul>
                  </div>
                  <div class="col-6 col-sm-4 col-xl-2">
                     <h4 class="footer-title">Social</h4>
                     <ul class="list-unstyled menu-list">
                        <li>
                           <a href="#">Twitter</a>
                        </li>
                        <li>
                           <a href="#">Telegram</a>
                        </li>
                        <li>
                           <a href="#">Facebook</a>
                        </li>
                        <li>
                           <a href="#">Instagram</a>
                        </li>
                        <li>
                           <a href="#">LinkedIn</a>
                        </li>
                        <li>
                           <a href="#">GitHub</a>
                        </li>
                        <li>
                           <a href="#">Bitcoin Talk</a>
                        </li>
                        <li>
                           <a href="#">Medium</a>
                        </li>
                        <li>
                           <a href="#">Youtube</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-bottom text-center">
            <div class="container">
               <div class="copyright-text">Copyright © 2018-2020 Synchrobit™. All rights reserved.</div>
            </div>
         </div>
      </footer>
   </div>
   
   <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script type="text/javascript" src="assets/js/slick.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>