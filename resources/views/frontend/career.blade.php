{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Career')

<!-- External CSS abouts-->
@push('css')
<link rel="stylesheet" href="/frontend/css/career.css">
@endpush

{{-- This defines content/body section here --}}
@section('content')     
<!-- This is Carrer Form -->
      <!-- Career's 1st Screen Starts here -->
      <div class="wrap-1" id="main-d">
        <div class="container-fluid">
         <div class="container height-fix position-relative">
         <h1 class="position-absolute head-align head text-white">Discover. Experience. <a href="/front-career/careerform" style="color:white;">Apply</a>.</h1>
         </div>
        </div>
      </div>
      <!-- Career's 1st Screen Ends here -->


      <!-- Career's 2nd Screen Starts here -->
      <div class="wrap-2 d-flex align-items-center" id="locations">
          <div class="container-fluid">
              <div class="container d-flex align-items-center justify-content-center flex-md-column alter-height-wrap-2 flex-column">
                <div class="row">
                    <div class="col-md-12 text-center">
                          <!-- heading -->
                          <div class="heading-wrap-2">
                              <h1 class="head my-5 d-none" id="hetch1">Our Locations</h1>
                          </div>
                          <!-- heading -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                            <!-- images collage starts here -->
                            <div class="mixed-collage-wrap-2 d-flex align-items-center justify-content-center flex-md-row flex-column">
                                
                                <div class="hover-eff d-none" id="col-1">
                                    <div class="bg-collage-image-1 d-flex align-items-center justify-content-center poi-cur">
                                        <h4 class="text-white alter-h4-collage">Americas</h4>
                                    </div>
                                </div>

                                <div class="hover-eff d-none" id="col-2">
                                    <div class="bg-collage-image-2 d-flex align-items-center justify-content-center poi-cur">
                                        <h4 class="text-white alter-h4-collage">Asia Pacific</h3>
                                    </div>
                                </div>

                                <div class="hover-eff d-none" id="col-3"> 
                                    <div class="bg-collage-image-3 d-flex align-items-center justify-content-center poi-cur">
                                        <h4 class="text-white text-center alter-h4-collage">Europe, Middle East and Africa</h4>
                                    </div>
                                </div>
                            </div>
                            <!-- images collage ends here -->
                    </div>
                </div>

              </div>
          </div>
      </div>
      <!-- Career's 2nd Screem Ends here -->

      <!-- Career's 3rd Screen Starts here -->
      <div class="wrap-3 d-flex align-items-center position-relative overflow-hidden">
          <div class="container-fluid">
              <div class="container">
                  <div class="row">
                      <div class="col-md-6">
                         <!-- Nothing -->
                      </div>

                      <div class="col-md-6 text-left invisible" id="float-text-1">

                          <div class="h1p-wrap-3">
                              <h1 class="head mb-3">Discover</h1>
                              <p class="alter-p-wrap-3 mb-5">The question isn’t what’s next. It’s what isn’t?</p>
                          </div>

                          <div class="p-wrap-3">
                          <p class="font-weight-bold alter-p">Find out how we help you navigate your next</p>
                          </div>

                          <div class="cards-wrap-3 d-flex ">
                              <div class="card mr-5 poi-cur effect-card">
                                  <img class="img-fluid" src="/images/card-1.jpg" class="card-img-top" alt="...">
                                  <div class="card-body alter-card-bg">
                                    <p class="card-text font-weight-bold text-left">The Company</p>
                                  </div>
                              </div>

                              <div class="card poi-cur effect-card">
                                  <img class="img-fluid" src="/images/card-2.jpg" class="card-img-top" alt="...">
                                  <div class="card-body alter-card-bg">
                                    <p class="card-text font-weight-bold text-left">Work</p>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
          </div>
          <!-- Floating image Starts here -->
          <img class="img-fluid floating-img position-absolute invisible" src="/images/wrap-3-float.png" alt="" id="float-1">
          <!-- Floating image Ends here -->
      </div>
      <!-- Career's 3rd Screen Ends here -->

      <!-- Career's 4th Screen Starts here -->
        <div class="wrap-4 d-flex align-items-center overflow-hidden">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 text-left invisible" id="float-text-2">

                            <div class="h1p-wrap-4">
                                <h1 class="head mb-3">Experience</h1>
                                <p class="alter-p-wrap-4 mb-5">We navigate global organizations from where they are to where they aspire to be.</p>
                            </div>

                            <div class="p-wrap-4">
                                <p class="font-weight-bold alter-p">Navigate your next</p>
                            </div>

                            <div class="cards-wrap-4 d-flex">
                                <div class="card mr-5 poi-cur effect-card">
                                    <img class="img-fluid" src="/images/card-1.jpg" class="card-img-top" alt="...">
                                    <div class="card-body alter-card-bg">
                                        <p class="card-text font-weight-bold text-left">Discover the next insight</p>
                                    </div>
                                </div>

                                <div class="card poi-cur effect-card">
                                    <img class="img-fluid" src="/images/card-2.jpg" class="card-img-top" alt="...">
                                    <div class="card-body alter-card-bg">
                                        <p class="card-text font-weight-bold text-left">Innovate at the next opportunity</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <!-- Nothing -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Floating image Starts here -->
            <img class="img-fluid position-absolute fl-right floating-img invisible" src="/images/wrap-4-float.png" alt="" id="float-2">
            <!-- Floating image Ends here -->
        </div>
      <!-- Career's 4th Screen Ends here -->



      <!-- Career's 5th Screen Starts here -->
      <div class="wrap-5 d-flex align-items-center overflow-hidden">
          <div class="container-fluid">
              <div class="container">
                  <div class="row">
                      <div class="col-md-6">
                          <!-- Nothing -->
                      </div>

                      <div class="col-md-6 text-left invisible" id="float-text-3">

                          <div class="h1p-wrap-5">
                              <h1 class="head mb-3">Apply</h1>
                              <p class="alter-p-wrap-5 mb-5">We navigate global organizations from where they are to where they aspire to be.</p>
                          </div>

                          <div class="p-wrap-5">
                              <p class="font-weight-bold alter-p">Navigate your next</p>
                          </div>

                          <div class="cards-wrap-5 d-flex">
                              <div class="card mr-5 poi-cur effect-card">
                                  <img class="img-fluid" src="/images/card-1.jpg" class="card-img-top" alt="...">
                                  <div class="card-body alter-card-bg">
                                    <p class="card-text font-weight-bold text-left">Explore internships</p>
                                  </div>
                              </div>

                              <div class="card poi-cur effect-card">
                                    <a href="/front-career/jobOpenings">
                                        <img class="img-fluid" src="/images/card-2.jpg" class="card-img-top" alt="...">
                                        <div class="card-body alter-card-bg">
                                        <p class="card-text font-weight-bold text-left">Explore openings</p>
                                        </div>
                                    </a> 
                              </div>
                          </div>   

                      </div>
                  </div>
              </div>
          </div>
          <!-- Floating image Starts here -->
          <img class="img-fluid position-absolute floating-img invisible" src="/images/wrap-5-float.png" alt="" id="float-3">
          <!-- Floating image Ends here -->
      </div>
      <!-- Career's 5th Screen Ends here -->



      <!-- Career's 6th Screen Starts here -->
      <div class="wrap-6 d-flex align-items-center">
          <div class="container-fluid">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="ad-banner mb-5">
                            <div class="alter-banner-h1 ml-auto d-flex flex-md-column justify-content-center py-3">

                              <h2 class="text-white text-left">Explore our work ethics & culture</h2>
                              <button type="button text-left" class="btn btn-outline-light alter-btn-radius" style="width: 80%;">KNOW MORE</button>

                            </div>
                            </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 text-center mb-5">
                        <h1 class="head mb-3">Latest at RevInfotech</h1>
                        <p class="alter-p-banner-top mb-5">A glimpse of our key announcements.</p>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12">
                          <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                              <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img src="/images/banner-1.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                  <img src="/images/banner-2.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                  <img src="/images/banner-3.jpg" class="d-block w-100" alt="...">
                                </div>
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                          </div>
                      </div>
                  </div>

                  <div class="row mt-5">
    
                          <div class="col-md-4">
                              <h1 class="head t-c">Join Us</h1>
                          </div>

                          <div class="col-md-8 d-flex flex-md-row flex-column align-items-center justify-content-center">
                              <div class="country-block-wrap-6 mr-1 d-flex align-items-center justify-content-center poi-cur">
                                  <h3 class="text-center">Americas</h2>
                              </div>

                              <div class="country-block-wrap-6 mr-1 d-flex align-items-center justify-content-center poi-cur">
                                  <h3 class="text-center">Asia Pacific</h3>
                              </div>

                              <div class="country-block-wrap-6 d-flex align-items-center justify-content-center poi-cur mob-mr">
                                  <h3 class="text-center">Europe, Middle East and Africa</h3>
                              </div>
                          </div>
                    
                  </div>
              </div>
          </div>
      </div>
    <!-- Career 6th Screen Ends here-->
@endsection

    <!-- Optional JavaScript -->
@push('js')    
    <script>
        var locations = document.getElementById("locations");
        var main = document.getElementById("main-d");
        var heading = document.getElementById("hetch1");
        var docBody = document.querySelector('body');
        var col1 = document.getElementById("col-1");
        var col2 = document.getElementById("col-2");
        var col3 = document.getElementById("col-3");
        var float1 = document.getElementById("float-1");
        var float2 = document.getElementById("float-2");
        var float3 = document.getElementById("float-3");
        var floattext1 = document.getElementById("float-text-1");
        var floattext2 = document.getElementById("float-text-2");
        var floattext3 = document.getElementById("float-text-3");
    

        var isInViewport = function (elem) {
        var bounding = elem.getBoundingClientRect();
        return (
            bounding.top >= 0 &&
            bounding.left >= 0 &&
            bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        };


        window.onscroll = function() {
            if (isInViewport(heading)){
            heading.classList.remove("d-none");
            heading.classList.add("animated","flipInY");
            }

            if (isInViewport(col1)){
            col1.classList.remove("d-none");       
            col1.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(col2)){
            col2.classList.remove("d-none");    
            col2.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(col3)){
            col3.classList.remove("d-none");
            col3.classList.add("animated","flipInX","delay-1s");
            }

            if (isInViewport(float1)){
            float1.classList.remove("invisible");    
            float1.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(float2)){
            float2.classList.remove("invisible");
            float2.classList.add("animated","fadeInRight");
            }

            if (isInViewport(float3)){
            float3.classList.remove("invisible");
            float3.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(floattext1)){
            floattext1.classList.remove("invisible");
            floattext1.classList.add("animated","fadeInRight");
            }

            if (isInViewport(floattext2)){
            floattext2.classList.remove("invisible");
            floattext2.classList.add("animated","fadeInLeft");
            }

            if (isInViewport(floattext3)){
            floattext3.classList.remove("invisible");
            floattext3.classList.add("animated","fadeInRight");
            }
            };
    </script>
    @endpush

    