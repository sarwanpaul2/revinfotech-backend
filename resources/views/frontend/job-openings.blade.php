{{-- This extends layout page proprty to home page  --}}
  @extends('/frontend/layouts/layout')
  @section('title', 'Industries')

<!-- External CSS abouts-->
  @push('css')
    <style>
    .bg-tech{
      background-image: url('/images/bg-job-tech.jpg');
      min-height: 100vh;
    }
      .job-card {
        min-height: 450px;
        border: 1px solid #2b308014;
        -webkit-transition: 1s all; /* For Safari 3.1 to 6.0 */
        transition: 1s all;
        /* color: #fff; */
        background-color: #f8f9facc;
      }
      .job-card:hover {
        transform: scale(1.04);
      }
     .job-card h4{
       font-weight: 600;
     }
      .bottom-10{
        width: 79%;
        bottom: 20px;
      }
      .footer{
        margin-top: 0px !important;
      }
      a{
        color:black;
        text-decoration:none !important;
      }
      a:hover{
        
        text-decoration:none !important;
       
      }
      .div_hover:hover { 
        color:#066ebf;
        background-color: white; }
    </style>
  @endpush

{{-- This defines content/body section here --}}
@section('content')  
    <main>
    <div class="bg-tech"  >
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center text-white my-5">Job Listing</h1>
          </div>
        </div>
        <div class="row pb-5">
        @php
          foreach($job_openings as $value):
        @endphp  
          
          <div class="col-md-6 col-xl-4 mb-4 mx-auto ">
          <a href="/front-career/jobOpeningsExplore/{{$value->id}}" class="read-more">
            <div class="job-card p-4 shadow rounded div_hover">
              <p class="mb-1 text-center">
                <h4 class="text-center">Requirement</h3>
              </p>
              <p class="mb-1">
                <strong>Position:</strong> <span>{{$value->position}}</span>
              </p>
              <p class="mb-1">
                <strong>Experience:</strong> <span>{{$value->experience}}</span>
              </p>
              <p class="mb-1">
                <strong>Location:</strong> <span>{{$value->location}}</span>
              </p>
              <p>
                <strong>Skills:</strong>
                <span>{{$value->skills}}</span>
              </p>
              <strong>Job Description:</strong>
              <p>
              {{$value->remark}}
              </p>
              <div class="position-absolute bottom-10">
                <div class="d-flex justify-content-between align-items-center mt-4">
                  <em>{{date('d-M-Y' ,strtotime($value->date))}}</em>
                  Read More <i class="fas fa-angle-right ml-2 align-middle"></i>
                  
                </div>
              </div>

            </div>
          </a>  
          </div>
          
        @php
          endforeach
        @endphp    

          
          
         
        </div>
      </div>
      </div>
    </main>
@endsection 
   
