{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Career Form')

<!-- External CSS abouts-->
@push('css')
<link rel="stylesheet" href="/frontend/css/careerForm.css">
@endpush

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{-- This defines content/body section here --}}
@section('content')
   

<!-- Form Area Starts here -->
<section class="career-form d-flex justify-content-center align-items-center">
<div class="contact-form text-center text-white py-5 px-4">
    @if ($message = Session::get('danger'))
    <!-- <div class="alert alert-danger alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
    </div> -->
        <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
        <div class="modal-dialog modal-dialog-top">
            <div class="modal-content" style="background-color:#f2dede;">
                <!-- Modal Header -->
                <div class="modal-header">
                    {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                </div>
                <!-- Modal body -->
                <div class="modal-body" >
                    <strong style="color:#a94442;">{{ $message }}</strong>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($message = Session::get('success'))
    <!-- <div class="alert alert-success alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
    </div> -->
    <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
            <div class="modal-dialog modal-dialog-top">
                <div class="modal-content" style="background-color:#dff0d8;">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" >
                        <strong style="color:#3c763d;">{{ $message }}</strong>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($errors->any())
    <!-- <div class="alert alert-danger alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
    </div> -->
        <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
        <div class="modal-dialog modal-dialog-top">
            <div class="modal-content" style="background-color:#f2dede;">
                <!-- Modal Header -->
                <div class="modal-header">
                    {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                </div>
                <!-- Modal body -->
                <div class="modal-body" >
                    <strong style="color:#a94442;">{!! implode('', $errors->all('<div>:message</div>')) !!}</strong>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="form-container p-3 p-lg-5">
        <div class="form-inner">
            <h2 class="text-center text-white mb-4">Application Form</h2>
            <form method="post" action="{{url('/front-career/careerformstore')}}" enctype="multipart/form-data" id="career_form">
            @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="first_name"  class="text-white" required>First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name" class="text-white">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name"  placeholder="Last Name" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="eMail" class="text-white">Email Address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact_num" class="text-white">Contact Number</label>
                        <input type="text" class="form-control" id="contact_num" name="contact_num" placeholder="Ex : +91 9899556642" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="sQualification" class="text-white">Qualification</label>
                        <select id="sQualification"  name="qualification" class="form-control" required>
                            <option value="" selected disabled hidden>Choose Qualification</option>
                            <option value="Graduate">Graduate</option>
                            <option value="Post Graduate">Post Graduate</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="experience" class="text-white">Experience</label>
                        <select id="experience" name="experience" class="form-control" required>
                            <option value="" selected disabled hidden>Choose Experience</option>
                            <option value="Fresher">Fresher</option>
                            <option value="Experienced">Experienced</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="position" class="text-white">Position</label>
                        <input type="text" class="form-control" id="position" name="position" value="{{$job_openings->position}}" required>
                    </div>
                    
                </div>

                <div class="form-group">
                    <!-- <input type="file" id="image" name="image" placeholder="image" required="true"> -->
                    <input  id="applicantResume" type="file" name="file" required>
                    <label For="applicantResume" class="" required>
                        <span class="btn btn-info text-white pointer-cursor" >Upload Resume</span>
                    </label>
                </div>
                <div class="input-group-c mb-5 g-recaptcha" data-sitekey="6LcOFMIUAAAAAKWl2DMRS7mR19VDpnnHQZUODLdl"></div>
                <div class="text-center mt-1">
                <input type="checkbox" id="termsPrivacy" name="termsPrivacy" value="yes">
                    I Agree with the <a href="#" class="text-warning d-block mb-2">Terms and Conditions</a> & <a href="#" class="text-warning d-block mb-2">Privacy Policy</a>
                    <button id="button_check" type="button" class="btn btn-primary px-5">Submit</button>
                </div>
            </form>    
        </div>
    </div>
</section>

<!-- Form Area Ends here -->

@endsection

@push('js')
 <!-- Client Side Script -->
 
<script type="text/javascript">
    // alert("Hello world");
    $(document).ready(function(){
    $("#danger-alert").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
    $("#myModal").fadeIn( 300 ).delay( 1500 ).fadeOut( 800); 

     $("#button_check").click(function(e){
                if ($("#termsPrivacy").is(":checked")) { 
                    $('#career_form').submit(); 
                } else { 
                    alert("Please accept the Terms and Conditions & Privacy Policy."); 
                }
            });
    });
</script>    
 <script src='https://www.google.com/recaptcha/api.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
<!-- Client Side Script End -->
@endpush