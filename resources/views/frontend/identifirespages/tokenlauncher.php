<!-- This is token launcher template -->
<div class="toknLauncherBanner d-flex align-items-center justify-content-center">
<div class="toknLauncherContent text-center text-white">
<h1 class="font-weight-bold">Token Launcher Company</h1>

<h2 class="font-weight-bolder my-3">{%b-title2%}</h2>

<p>{%b-title3%}</p>
<button class="px-4 py-2 font-weight-bolder mt-3 btn-orange" type="button">Get In Touch</button></div>
</div>
<!-- feature -->

<div class="container tokenFeature my-5">
<div class="tokenFeatureHead">
<h2 class="text-center font-weight-bold">Features Of Token Launcher Platform</h2>

<hr class="headingBorder my-2" />
<p class="text-center">{%ftp-desc1%}</p>
</div>

<div class="row mt-4">
<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/smart.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col1-title%}</h5>

<p>{%ftp-col1-desc%}</p>

<p>&nbsp;</p>
</div>
</div>
</div>

<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/compliant.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col2-title2%}</h5>

<p>{%ftp-col2-desc2%}</p>

<p>&nbsp;</p>
</div>
</div>
</div>

<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/audited.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col3-title%}</h5>

<p>{%ftp-col3-desc%}</p>

<p>&nbsp;</p>
</div>
</div>
</div>

<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/crm.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col4-title%}</h5>

<p>{%ftp-col4-desc%}</p>
</div>
</div>
</div>

<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/user-friendly.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col5-title%}</h5>

<p>{%ftp-col5-desc%}</p>

<p>&nbsp;</p>
</div>
</div>
</div>

<div class="col-md-6 col-lg-4 mb-4">
<div class="featureItem position-relative"><img class="img-fluid" src="/images/customised.png" />
<div class="featureContent position-absolute">
<h5 class="font-weight-bolder">{%ftp-col6-title%}</h5>

<p>{%ftp-col6-desc%}</p>

<p>&nbsp;</p>
</div>
</div>
</div>
</div>
</div>
<!-- feature end --><!--  How It Works -->

<div class="processSection py-4" id="processSection">
<div class="container">
<h2 class="text-center font-weight-bold">How It Works</h2>

<hr class="headingBorder my-2" />
<section class="roadMap-conatiner">
<div class="roadmap-inner">
<div class="page-header d-flex justify-content-center mb-3">&nbsp;</div>

<ul class="timeline">
	<li>
	<div class="timeline-badge d-flex align-items-center justify-content-center"><span>1</span></div>

	<div class="timeline-panel" data-aos="fade-right">
	<div class="timeline-heading">
	<h5 class="timeline-title font-weight-bold mb-1">{%hiw-col1-title%}</h5>

	<p class="text-justify">{%hiw-col1-desc%}</p>
	</div>
	</div>
	</li>
	<li class="timeline-inverted">
	<div class="timeline-badge warning"><span>2 </span></div>

	<div class="timeline-panel" data-aos="fade-left">
	<div class="timeline-heading">
	<h5 class="timeline-title font-weight-bold mb-1">{%hiw-col2-title%}</h5>

	<p class="text-justify">{%hiw-col2-desc%}</p>
	</div>
	</div>
	</li>
	<li>
	<div class="timeline-badge d-flex align-items-center justify-content-center"><span>3</span></div>

	<div class="timeline-panel" data-aos="fade-right">
	<div class="timeline-heading">
	<h5 class="timeline-title font-weight-bold mb-1">{%hiw-col3-title%}</h5>

	<p class="text-justify">{%hiw-col3-desc%}</p>
	</div>
	</div>
	</li>
	<li class="timeline-inverted">
	<div class="timeline-badge warning"><span>4</span></div>

	<div class="timeline-panel" data-aos="fade-left">
	<div class="timeline-heading">
	<h5 class="timeline-title font-weight-bold mb-1">{%hiw-col4-title%}</h5>

	<p class="text-justify">{%hiw-col4-desc%}</p>
	</div>
	</div>
	</li>
</ul>
</div>
</section>
</div>
</div>
<!-- How It Works End --><!-- Our Client -->

<div class="ourClient my-5">
<h2 class="text-center font-weight-bold">Our Clients</h2>

<hr class="headingBorder my-2" />
<div class="logoSection d-flex justify-content-center mt-4">
<div class="mx-1 mx-md-4"><img class="img-fluid" src="/images/bitbose.png" /></div>

<div class="mx-1 mx-md-4"><img class="img-fluid" src="/images/velorem.png" /></div>
</div>
</div>
<!-- Our Client End -->