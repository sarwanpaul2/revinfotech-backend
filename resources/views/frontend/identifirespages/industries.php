<!-- This is industries template -->
<main>
<section class="section-first">
<div class="industries-banner-image position-relative">
<div class="container">
<div class="industries-banner-content pt-4">
<div class="shape-trapezoid position-relative">{%b-title%}</div>

<h1 class="text-white my-4">{%b-desc%}</h1>
<button class="button-outline button-white font-weight-bold">LEARN MORE</button></div>
</div>
</div>
</section>

<section class="section-second">
<div class="container">
<div class="second-section-content my-5">
<h1 class="text-center my-4 mx-auto custom-heading-width">Navigate your next Healthcare</h1>

<div class="row my-3">
<div class="col-md-12">
<h5 class="text-center mb-4">{%nav-desc%}</h5>
</div>

<div class="mx-auto text-center"><button class="button-outline button-black font-weight-bold">READ MORE</button></div>
</div>
</div>
</div>
</section>

<section class="section-third my-5">
<div class="container">
<div class="my-4">
<h1 class="text-center">Insights</h1>

<h5 class="text-center text-grey">{%insights-desc%}</h5>
</div>

<div class="d-flex flex-column flex-lg-row">
<div>
<div class="img-box position-relative"><img class="img-fluid w-100" src="https://placeimg.com/600/400/nature" />
<div class="position-absolute pos-style-1">
<p class="text-secondary">{%img1-title%}</p>

<p class="text-white">{%img1-desc%}</p>
</div>
</div>
</div>

<div class="d-flex flex-column flex-sm-row">
<div class="img-box position-relative"><img class="img-fluid vw-m-100" src="https://dummyimage.com/300x400/f8f8f8/f8f8f8" />
<p class="position-absolute text-secondary pos-style-text-article">{%img2-title2%}</p>

<div class="position-absolute pos-style-2"><img class="img-fluid w-100" src="https://placeimg.com/253/153/tech" />
<p class="para-excerpt-style mt-2">{%img2-desc%}</p>
</div>

<p class="position-absolute font-weight-bold pos-style-text-readmore">READ MORE</p>
</div>

<div class="img-box position-relative"><img class="img-fluid vw-m-100" src="https://dummyimage.com/300x400/963596/963596" />
<p class="position-absolute text-white pos-style-text-article">{%img3-title%}</p>

<p class="position-absolute text-white para-pos-style para-excerpt-style">{%img3-desc%}</p>

<p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
</div>
</div>
</div>

<div class="d-flex flex-column flex-lg-row">
<div class="d-flex flex-column flex-sm-row">
<div class="img-box position-relative"><img class="img-fluid vw-sm-100" src="https://dummyimage.com/300x400/f8f8f8/f8f8f8" />
<p class="position-absolute text-secondary pos-style-text-article">{%img4-title%}</p>

<p class="position-absolute para-pos-style-bottom para-excerpt-style">{%img4-desc%}</p>

<p class="position-absolute font-weight-bold pos-style-text-readmore">READ MORE</p>
</div>

<div class="img-box position-relative"><img class="img-fluid vw-sm-100" src="https://dummyimage.com/300x400/f16c51/f16c51" />
<p class="position-absolute text-white pos-style-text-article">{%img5-title%}</p>

<p class="position-absolute text-white para-pos-style-bottom para-excerpt-style">{%img5-desc%}</p>

<p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
</div>
</div>

<div class="d-flex flex-column flex-sm-row">
<div class="img-box position-relative"><img class="img-fluid vw-sm-100" src="https://placeimg.com/300/400/animals" />
<p class="position-absolute text-white pos-style-text-article">{%img6-title%}</p>

<div class="position-absolute corner-shape-style">
<div class="position-relative">
<p class="position-absolute text-white para-pos-style-shape para-excerpt-style">{%img6-desc%}</p>
</div>
</div>

<p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
</div>

<div class="img-box position-relative"><img class="img-fluid vw-sm-100" src="https://dummyimage.com/300x400/DF9926/DF9926" />
<p class="position-absolute text-white pos-style-text-article">{%img7-title%}</p>

<p class="position-absolute text-white para-pos-style-bottom para-excerpt-style">{%img7-desc%}</p>

<p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
</div>
</div>
</div>
</div>
</section>

<section class="section-fourth my-5">
<div class="container">
<div class="my-4">
<h1 class="text-center">What&#39;s New</h1>
</div>

<div class="row">
<div class="col-md-6 col-xl-3 my-3">
<div class="position-relative min-height-500 bg-light-blue p-4">
<div class="shape-trapezoid position-relative">{%wn1-title%}</div>

<p class="my-5 font-weight-bold para-style">{%wn1-desc%}</p>

<div class="mx-auto text-center position-absolute bottom-30"><button class="button-outline button-black font-weight-bold">READ MORE</button></div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="position-relative min-height-500 bg-light-blue p-4">
<div class="shape-trapezoid position-relative">{%wn2-title%}</div>

<p class="my-5 font-weight-bold para-style">{%wn2-desc%}</p>

<div class="mx-auto text-center position-absolute bottom-30"><button class="button-outline button-black font-weight-bold">READ MORE</button></div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="position-relative min-height-500 bg-light-blue p-4">
<div class="shape-trapezoid position-relative">{%wn3-title%}</div>

<p class="my-5 font-weight-bold para-style">{%wn3-title%}</p>

<div class="mx-auto text-center position-absolute bottom-30"><button class="button-outline button-black font-weight-bold">READ MORE</button></div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="position-relative min-height-500 bg-light-blue p-4">
<div class="shape-trapezoid position-relative">{%wn4-title%}</div>

<p class="my-5 font-weight-bold para-style">{%wn4-desc%}</p>

<div class="mx-auto text-center position-absolute bottom-30"><button class="button-outline button-black font-weight-bold">READ MORE</button></div>
</div>
</div>
</div>
</div>
</section>

<section class="section-fifth my-5 py-5">
<div class="container">
<div class="text-white">
<h1 class="heading-style">Success Stories</h1>

<p class="para-style mb-5 mt-3 w-md-85">{%SuccStor-desc%}</p>
</div>

<div class="row">
<div class="col-md-6 col-xl-3 my-3">
<div class="bg-white min-height-400">
<div class="img-box position-relative"><img class="img-fluid w-100" src="https://placeimg.com/255/180/nature" />
<div class="position-absolute left-20-bottom-30">
<div class="shape-trapezoid-orange position-relative"><span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span></div>
</div>
</div>

<div class="p-4">{%SuccStor1-desc%}</div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="bg-white min-height-400">
<div class="img-box position-relative"><img class="img-fluid w-100" src="https://placeimg.com/255/180/people" />
<div class="position-absolute left-20-bottom-30">
<div class="shape-trapezoid-orange position-relative"><span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span></div>
</div>
</div>

<div class="p-4">{%SuccStor2-desc%}</div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="bg-white min-height-400">
<div class="img-box position-relative"><img class="img-fluid w-100" src="https://placeimg.com/255/180/tech" />
<div class="position-absolute left-20-bottom-30">
<div class="shape-trapezoid-orange position-relative"><span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span></div>
</div>
</div>

<div class="p-4">{%SuccStor3-desc%}</div>
</div>
</div>

<div class="col-md-6 col-xl-3 my-3">
<div class="bg-white min-height-400">
<div class="img-box position-relative"><img class="img-fluid w-100" src="https://placeimg.com/255/180/animals" />
<div class="position-absolute left-20-bottom-30">
<div class="shape-trapezoid-orange position-relative"><span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span></div>
</div>
</div>

<div class="p-4">{%SuccStor4-desc%}</div>
</div>
</div>
</div>

<div class="text-center my-5"><button class="button-outline button-white font-weight-bold">LEARN MORE</button></div>
</div>
</section>
</main>