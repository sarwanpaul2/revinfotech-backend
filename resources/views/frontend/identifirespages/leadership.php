<!-- This is leadership template --><!-- Career's 1st Screen Starts here -->
<div class="container-fluid leadershipBanner">
<div class="row">
<div class="col-lg-8 col-xl-6 bannerParent d-flex align-items-center text-white">
<div class="leadershipBannerContent px-3 px-xl-5">
<h1 class="font-weight-bold">{%b-title%}</h1>

<p>{%b-desc%}</p>
</div>
</div>
</div>
</div>
<!-- Our Business Philosophy  -->

<div class="container businessPhilosophy text-center my-5">
<h2>Our Business Philosophy</h2>

<hr class="afterHead mb-5" />
<p class="text-muted">{%obp-desc%}</p>
</div>
<!-- Our Leader's -->

<div class="OurLeaders container">
<h2 class="text-center mt-4 mb-0">Our Leader&#39;s</h2>

<hr class="afterHead mb-4" />
<div class="row">
<div class="col-md-6">
<div class="team-member">
<div class="team-img"><img alt="team member" class="img-fluid img-thumbnail" src="/images/ceo.png" /></div>

<div class="team-hover">
<div class="desk">
<p class="text-muted pt-4">{%ol-desc1%}</p>
</div>

<div class="s-link">&nbsp;</div>
</div>
</div>

<div class="team-title">
<h5 class="font-weight-bold">{%ol-name1%}</h5>
{%ol-desg1%}</div>
</div>

<div class="col-md-6">
<div class="team-member">
<div class="team-img"><img alt="team member" class="img-fluid img-thumbnail" src="/images/coo.png" /></div>

<div class="team-hover">
<div class="desk">
<p class="text-muted pt-4">{%ol-desc2%}</p>
</div>

<div class="s-link">&nbsp;</div>
</div>
</div>

<div class="team-title">
<h5 class="font-weight-bold">{%ol-name2%}</h5>
{%ol-desg2%}</div>
</div>
</div>
</div>
