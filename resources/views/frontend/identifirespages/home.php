<!-- The Next -->
<div class="wrap-5 mt-5">
<div class="container-fluid p-custom-0">
<div class="container-fluid h-fix">
<div class="row">
<div class="col-md-12 text-center alter-col-bottom">
<h1 class="alter-h1-head"><img alt="no logo" class="img-fluid logo-revinfotech" id="companyLogo" src="/images/logo.png" /></h1>

<table aria-describedby="blogTable_info" class="dataTable no-footer table table-bordered" id="blogTable" role="grid">
	<tbody>
		<tr class="even">
			<td>{%top-description%}</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
</div>
</div>

<div class="row">
<div class="col-lg-3 alter-fixation">
<div class="row">
<div class="col-6 col-lg-12 content-parent image-holder-div2">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading1-title%}</h3>

<p>{%heading1-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading1%}</h5>
</div>

<div class="col-6 col-lg-12 content-parent image-holder-div3">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading2-title%}</h3>

<p>{%heading2-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading2%}</h5>
</div>
</div>
</div>

<div class="col-lg-3 alter-fixation p-0">
<div class="content-parent image-holder-div1">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading3-title%}</h3>

<p>{%heading3-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading3%}</h5>
</div>
</div>

<div class="col-lg-6 alter-fixation">
<div class="row">
<div class="col-6 content-parent image-holder-div2">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading4-title%}</h3>

<p>{%heading4-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading4%}</h5>
</div>

<div class="col-6 content-parent image-holder-div3">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading5-title%}</h3>

<p>{%heading5-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading5%}</h5>
</div>

<div class="col-6 content-parent image-holder-div4">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading6-title%}</h3>

<p>{%heading6-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading6%}</h5>
</div>

<div class="col-6 content-parent image-holder-div5">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%heading7-title%}</h3>

<p>{%heading7-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%topheading7%}</h5>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- The Next End --><!-- About Us -->

<div class="wrap-7 my-5">
<div class="container-fluid p-custom-0">
<div class="container h-fix">
<div class="row">
<div class="col-md-12 text-center alter-col-bottom mb-5">
<h1 class="alter-h1-head">About us</h1>

<p class="alter-p-wrap-5">{%aboutus-desc%}</p>
</div>
</div>

<div class="row mx-auto mb-5">
<div class="col-md-3">
<h1 class="text-center font-weight-bold" id="countries">{%col1-count%}</h1>

<p class="text-center">{%col1-desc%}</p>
</div>

<div class="col-md-3">
<h1 class="text-center font-weight-bold" id="revenue">{%col2-count%}</h1>

<p class="text-center">{%col2-desc%}</p>
</div>

<div class="col-md-3">
<h1 class="text-center font-weight-bold" id="employees">{%col3-count%}</h1>

<p class="text-center">{%col3-desc%}</p>
</div>

<div class="col-md-3">
<h1 class="text-center font-weight-bold" id="skills">{%col4-count%}</h1>

<p class="text-center">{%col4-desc%}</p>
</div>
</div>

<div class="row">
<div class="col-lg-6 alter-fixation p-0">
<div class="content-parent image-holder-div1">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%aboutus-h1%}</h3>

<p>{%aboutus-h1-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%aboutus-h1-top%}</h5>
</div>
</div>

<div class="col-lg-6 alter-fixation">
<div class="row">
<div class="col-6 content-parent image-holder-div2">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%aboutus-h2%}</h3>

<p>{%aboutus-h2-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%aboutus-h1-top%}</h5>
</div>

<div class="col-6 content-parent image-holder-div3">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%aboutus-h3%}</h3>

<p>{%aboutus-h3-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%aboutus-h3-top%}</h5>
</div>

<div class="col-6 content-parent image-holder-div4">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%aboutus-h4%}</h3>

<p>{%aboutus-h4-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%aboutus-h4-top%}</h5>
</div>

<div class="col-6 content-parent image-holder-div5">
<div class="content-item d-flex align-items-center justify-content-center text-center">
<div>
<h3>{%aboutus-h5%}</h3>

<p>{%aboutus-h5-desc%}</p>
</div>
</div>

<h5 class="contentHeading position-absolute text-white">{%aboutus-h5-top%}</h5>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- About Us End --><!-- Our Client  -->

<div class="container my-5">
<h1 class="text-center my-4">Our Partners</h1>

<section class="customer-logos slider">
<div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg" /></div>

<div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg" /></div>
</section>
</div>
<!-- Our Client End--><!-- Investor -->

<div class="container">
<h1 class="text-center my-4 alter-h1-head">Investor</h1>

<div class="row">
<div class="col-lg-7 investor-bg">&nbsp;</div>

<div class="col-lg-5">
<div class="investorContent p-4">
<h1 class="font-weight-bold">{%investor-title%}</h1>

<div class="investorItem mt-4">
<p class="m-0">REPORTS</p>

<h5>{%report-title%}</h5>
</div>

<div class="investorItem">
<p class="m-0">RESULTS</p>

<h5>{%result-title%}</h5>
</div>

<div class="investorItem">
<p class="m-0">EVENT</p>

<h5>{%event-title%}</h5>
</div>
<a class="d-block my-3 font-weight-bold viewAll" href="#">View All</a></div>
</div>
<!-- Investor End --><!-- career section -->

<div class="container my-5 careerParent">
<h1 class="text-center my-2">Career</h1>

<p class="alter-p-wrap-5 mb-3">{%carrer-desc%}</p>

<div class="careerContainer pt-5 position-relative"><button class="d-block mx-auto mb-5" type="button">Explore Jobs</button>

<section class="careerSlider slider">
<div class="slide text-center">
<div class="testimonialItemInner careerInner mt-2">
<p>{%testimonial1-desc%}</p>

<h5>{%writer1-name%}</h5>
</div>
</div>

<div class="slide text-center">
<div class="testimonialItemInner careerInner mt-2">
<p>{%testimonial2-desc%}</p>

<h5>{%writer2-name%}</h5>
</div>
</div>

<div class="slide text-center">
<div class="testimonialItemInner careerInner mt-2">
<p>{%testimonial3-desc%}</p>

<h5>{%writer3-name%}</h5>
</div>
</div>
</section>
</div>
</div>
<!-- career section end --></div>
</div>