{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Contact Us')

@push('css')
<!-- External CSS abouts-->
<link rel="stylesheet" href="/frontend/css/contactUs.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"     crossorigin="anonymous"/>
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
@endpush

{{-- This defines content/body section here --}}
@section('content')

<!-- Note :
   - You can modify the font style and form style to suit your website. 
   - Code lines with comments ���Do not remove this code���  are required for the form to work properly, make sure that you do not remove these lines of code. 
   - The Mandatory check script can modified as to suit your business needs. 
   - It is important that you test the modified form before going live.-->
   <div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form action='https://crm.zoho.com/crm/WebToContactForm' name=WebToContacts4349749000000251153 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory4349749000000251153()' accept-charset='UTF-8'>
 <input type='text' style='display:none;' name='xnQsjsdp' value='f40b4046333ee9cffb810cfe5e9e3bc023497aeda179f5529453f0e3fbdbcdb7'></input> 
 <input type='hidden' name='zc_gad' id='zc_gad' value=''></input> 
 <input type='text' style='display:none;' name='xmIwtLD' value='1824dd8480cdd859d7e17c2a3f56f2ad130823787aa5d8b6f53a2211d2091d68'></input> 
 <input type='text'  style='display:none;' name='actionType' value='Q29udGFjdHM='></input>
 <input type='text' style='display:none;' name='returnURL' value='http://testrev.devserver.co.in/front-career/career' > </input><br></br>
	 <!-- Do not remove this code. -->
	<style>
		#crmWebToEntityForm tr , #crmWebToEntityForm td { 
			padding:6px;
			border-spacing:0px;
			border-width:0px;
			}
	</style>
	<table style='width:600px;background-color:#ffffff;background-color:white;color:black'><tr><td colspan='2' align='left' style='color:black;font-family:Arial;font-size:14px;word-break: break-word;'><strong>Contact Us</strong></td></tr> <br></br><tr><td  style='word-break: break-word;text-align:left;font-size:12px;font-family:Arial;width:30%;'>First Name<span style='color:red;'>*</span></td><td style='width:40%;' ><input type='text' style='width:100%;box-sizing:border-box;'  maxlength='40' name='First Name' /></td><td style='width:30%;'></td></tr><tr><td  style='word-break: break-word;text-align:left;font-size:12px;font-family:Arial;width:30%;'>Last Name<span style='color:red;'>*</span></td><td style='width:40%;' ><input type='text' style='width:100%;box-sizing:border-box;'  maxlength='80' name='Last Name' /></td><td style='width:30%;'></td></tr><tr><td  style='word-break: break-word;text-align:left;font-size:12px;font-family:Arial;width:30%;'>Email<span style='color:red;'>*</span></td><td style='width:40%;' ><input type='text' style='width:100%;box-sizing:border-box;'  maxlength='100' name='Email' /></td><td style='width:30%;'></td></tr><tr><td  style='word-break: break-word;text-align:left;font-size:12px;font-family:Arial;width:30%;'>Mobile<span style='color:red;'>*</span></td><td style='width:40%;' ><input type='text' style='width:100%;box-sizing:border-box;'  maxlength='30' name='Mobile' /></td><td style='width:30%;'></td></tr><tr><td  style='word-break: break-word;text-align:left;font-size:12px;font-family:Arial;width:30%;'>Description </td><td style='width:40%;'> <textarea name='Description' maxlength='32000' style='width:100%;'>&nbsp;</textarea></td><td style='width:30%;'></td></tr><tr><td colspan='2'><div class='dIB vat mT2' align='left' style='color: rgb(0, 0, 0); font-size: 12px; font-family: sans-serif;display: inline-block;vertical-align: top;'><div class='displayPurpose  f13'><label class='newCustomchkbox-md dIB w100per'><input autocomplete='off' onclick='disableSubmit()' id='privacyTool' type='checkbox' name='privacyTool'><span class='chkbxIcon'></span></label></div></div><div class='fw200 dIB vat mL5' style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;vertical-align: top;margin-top: 3px;line-height: 14px;width:90%;display: inline-block;color:black'><div><b>Nothing just u have to be real and genuine</b></div></div>

	<tr><td colspan='2' style='text-align:center; padding-top:15px;font-size:12px;'>
		<input style='opacity:0.5;margin-right: 5px;cursor: pointer;font-size:12px;color:black' id='formsubmit'disabled type='submit' value='Submit'  ></input> <input onclick='disableSubmitwhileReset()'type='reset' name='reset' style='cursor: pointer;font-size:12px;color:black' value='Reset' ></input> </td></tr></table>
	<script>
 	  var mndFileds=new Array('First Name','Last Name','Email','Mobile');
 	  var fldLangVal=new Array('First Name','Last Name','Email','Mobile'); 
		var name='';
		var email=''; 
  
function disableSubmitwhileReset()
	{		
	var submitbutton = document.getElementById('formsubmit');
   var style = submitbutton.getAttribute('style');
   if (style.indexOf('opacity:0.5')<0) {
   style='opacity:0.5;'+style;
    }
 	if(document.getElementById('privacyTool')!=undefined || document.getElementById('consentTool')!=undefined)
	{
		submitbutton.setAttribute('disabled', true);
	submitbutton.setAttribute('style',style);
 }
	else
	{
		submitbutton.removeAttribute('disabled');
	}
} 
function disableSubmit()
	{		
	var isprivacyToolAvail = false;
	var isconsentToolAvail = false;
	var bothtoolAvail = false;
	var privacytool='';
	var consenttool='';
	var submitbutton = document.getElementById('formsubmit');
	var style=style1 = submitbutton.getAttribute('style');
   style=style.replace('opacity:0.5;','');
   if (style1.indexOf('opacity:0.5')<0) {
    style1=style1.concat(';opacity:0.5;'); 
  }
   if(document.getElementById('privacyTool')!=undefined && document.getElementById('consentTool')!=undefined)
	{
			isprivacyToolAvail=true;
			privacytool = document.getElementById('privacyTool');
			isconsentToolAvail=true;
			consenttool=document.getElementById('consentTool');
			bothtoolAvail=true;
	}

	if(document.getElementById('privacyTool')!=undefined)
	{
			isprivacyToolAvail=true;
			privacytool = document.getElementById('privacyTool');
	}
	if(document.getElementById('consentTool')!=undefined)
	{
			isconsentToolAvail=true;
			consenttool=document.getElementById('consentTool');
	}
	if(submitbutton.hasAttribute('disabled') && bothtoolAvail && consenttool.checked && privacytool.checked)
	{
			submitbutton.removeAttribute('disabled');
	submitbutton.setAttribute('style',style);}
	else if(submitbutton.hasAttribute('disabled') && isprivacyToolAvail && privacytool.checked && consenttool=='')
	{
			submitbutton.removeAttribute('disabled');
	submitbutton.setAttribute('style',style);}
	else if(submitbutton.hasAttribute('disabled') && isconsentToolAvail && consenttool.checked && privacytool=='')
	{
			submitbutton.removeAttribute('disabled');
submitbutton.setAttribute('style',style);
	}
	else
	{
			submitbutton.setAttribute('disabled', true);
	submitbutton.setAttribute('style',style1);}

	}

 	  function checkMandatory4349749000000251153() {
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebToContacts4349749000000251153'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{ 
				 alert('Please select a file to upload.'); 
				 fieldObj.focus(); 
				 return false;
				} 
			alert(fldLangVal[i] +' cannot be empty.'); 
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none.'); 
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   } 
			 } 
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
		document.getElementById('formsubmit').disabled=true;
	}
</script>
	</form>
</div>
@endsection  

@push('js')
 <!-- Client Side Script -->
 <script src='https://www.google.com/recaptcha/api.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
    <script type="text/javascript">
     $(document).ready(function(){
     $("#myModal").fadeIn( 300 ).delay( 1500 ).fadeOut( 800); 

     $("#button_check").click(function(e){
                if ($("#termsPrivacy").is(":checked")) { 
                    $('#contact_form').submit(); 
                } else { 
                    alert("Please accept the Terms and Conditions & Privacy Policy."); 
                }
            });

     var get_url = window.location.href;
            $('#get-url').val(get_url);   
        });
   
    </script>
<!-- Client Side Script End -->
@endpush