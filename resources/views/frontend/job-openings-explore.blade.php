{{-- This extends layout page proprty to home page  --}}
  @extends('/frontend/layouts/layout')
  @section('title', 'Industries')

<!-- External CSS abouts-->
  @push('css')
    <style>
      .job-opening {
        background: url(/images/job-opening.jpg);
        background-position: center;
      }
      .job-opening .btn-info {
        background-color: #003b55;
        border-color: #003b55;
      }
      .btn-info:not(:disabled):not(.disabled):active {
        background-color: #002231;
        border-color: #002231;
      }

      .job-opening .job-content {
        box-shadow: 1px 1px 10px 3px #ccc;
      }

      .job-opening .text-warning {
        color: #f56c05 !important;
      }
    </style>
  @endpush

{{-- This defines content/body section here --}}
@section('content')  


<main class="job-opening">
      <div class="container pb-5">
        <h1 class="text-center pt-5 mb-5 text-white">Job Opening</h1>
        <div class="job-content p-5 bg-white rounded">
        {!!($job_openings->job_desc)!!}

          <div class="my-5 text-center">
          <a href="/front-career/jobOpeningsExploreApply/{{$job_openings->id}}"
            <button class="btn btn-primary">Click Here To Apply</button>
          </a>  
            <span class="d-block d-md-inline my-3 my-md-0 mx-md-4">Or</span>
            <button class="btn btn-info">Apply With LinkedIn</button>
          </div>
        </div>
      </div>
    </main>
@endsection 
   
