{{-- This extends layout page proprty to home page  --}}
  @extends('/frontend/layouts/layout')
  @section('title', 'Industries | Revinfotech')

<!-- External CSS abouts-->
  @push('css')
    <link rel="stylesheet" href="/frontend/css/industries.css" />
  @endpush

{{-- This defines content/body section here --}}
@section('content')  
  <main>
    <section class="section-first">
      <div class="industries-banner-image position-relative">
        <div class="container">
          <div class="industries-banner-content pt-4">
            <div class="shape-trapezoid position-relative">
              <span class="button-trapezoid position-absolute text-white">FEATURE</span>
            </div>
            <h1 class="text-white my-4">Endless possibilities with data for Healthcare and Life Sciences: Navigate from
              now to your next</h1>
            <button class="button-outline button-white font-weight-bold">LEARN MORE</button>
          </div>
        </div>
      </div>
    </section>

    <section class="section-second">
      <div class="container">
        <div class="second-section-content my-5">
          <h1 class="text-center my-4 mx-auto custom-heading-width">Navigate your next Healthcate</h1>
          <div class="row my-3">
            <div class="col-md-12">
              <h5 class="text-center mb-4">Exponential increase in the adoption of Digital is a huge disruptive force that is
                leading to consumers
                of tomorrow. This in turn demands healthcare organizations to gear up to manage the consumer experience
                effectively. </h5>
            </div>
          <div class="mx-auto text-center">
            <button class="button-outline button-black font-weight-bold">READ MORE</button>
          </div>
        </div>
      </div>
    </section>

    <section class="section-third my-5">
      <div class="container">
        <div class="my-4">
          <h1 class="text-center">Insights</h1>
          <h5 class="text-center text-grey">OUr experts discuss technologies to address food security</h5>
        </div>
        <div class="d-flex flex-column flex-lg-row">
          <div class="">
            <div class="img-box position-relative">
              <img src="https://placeimg.com/600/400/nature" class="img-fluid w-100" />
              <div class="position-absolute pos-style-1">
                <p class="text-secondary">FEATURE</p>
                <p class="text-white">The future of Healthcare Industry-Healthcare 2020</p>
              </div>
            </div>
          </div>
          <div class="d-flex flex-column flex-sm-row">
            <div class="img-box position-relative">
              <img src="https://dummyimage.com/300x400/f8f8f8/f8f8f8" class="img-fluid vw-m-100" />
              <p class="position-absolute text-secondary pos-style-text-article">ARTICLE</p>
              <div class="position-absolute pos-style-2">
                <img src="https://placeimg.com/253/153/tech" class="img-fluid w-100" />
                <p class="para-excerpt-style mt-2">How Technology Can Make Healthcare Widely Accessible In Latin
                </p>
              </div>
              <p class="position-absolute font-weight-bold pos-style-text-readmore">READ MORE</p>
            </div>
            <div class="img-box position-relative">
              <img src="https://dummyimage.com/300x400/963596/963596" class="img-fluid vw-m-100" />
              <p class="position-absolute text-white pos-style-text-article">WHITE PAPER</p>
              <p class="position-absolute text-white para-pos-style para-excerpt-style">Building Healthcare for a better
                future-Practical
                strategies and proven
                practices for consumer engagement</p>
              <p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
            </div>
          </div>
        </div>
        <div class="d-flex flex-column flex-lg-row">
          <div class="d-flex flex-column flex-sm-row">
            <div class="img-box position-relative">
              <img src="https://dummyimage.com/300x400/f8f8f8/f8f8f8" class="img-fluid vw-sm-100" />
              <p class="position-absolute text-secondary pos-style-text-article">ARTICLE</p>
              <p class="position-absolute para-pos-style-bottom para-excerpt-style">You Can Focus on Care, Not Costs,
                Thanks to AI</p>
              <p class="position-absolute font-weight-bold pos-style-text-readmore">READ MORE</p>
            </div>
            <div class="img-box position-relative">
              <img src="https://dummyimage.com/300x400/f16c51/f16c51" class="img-fluid vw-sm-100" />
              <p class="position-absolute text-white pos-style-text-article">ARTICLE</p>
              <p class="position-absolute text-white para-pos-style-bottom para-excerpt-style">Reimagining
                Care-as-a-Service</p>
              <p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
            </div>
          </div>

          <div class="d-flex flex-column flex-sm-row">
            <div class="img-box position-relative">
              <img src="https://placeimg.com/300/400/animals" class="img-fluid vw-sm-100" />
              <p class="position-absolute text-white pos-style-text-article">FEATURE</p>

              <div class="position-absolute corner-shape-style">
                <div class="position-relative">
                  <p class="position-absolute text-white para-pos-style-shape para-excerpt-style">Connected care: Can
                    wearable tech improve your health?</p>
                </div>
              </div>
              <p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
            </div>
            <div class="img-box position-relative">
              <img src="https://dummyimage.com/300x400/DF9926/DF9926" class="img-fluid vw-sm-100" />
              <p class="position-absolute text-white pos-style-text-article">WHITE PAPER</p>
              <p class="position-absolute text-white para-pos-style-bottom para-excerpt-style">Digital-The big
                disruption
                in the Healthcare Industry</p>
              <p class="position-absolute font-weight-bold text-white pos-style-text-readmore">READ MORE</p>
            </div>
          </div>


        </div>
      </div>
    </section>


    <section class="section-fourth my-5">
      <div class="container">
        <div class="my-4">
          <h1 class="text-center">What's New</h1>
        </div>
        <div class="row">
          <div class="col-md-6 col-xl-3 my-3">
            <div class="position-relative min-height-500 bg-light-blue p-4">
              <div class="shape-trapezoid position-relative">
                <span class="button-trapezoid position-absolute text-white">FEATURE</span>
              </div>
              <p class="my-5 font-weight-bold para-style">Revinfotech positioned in the Winner's Circle of HfS Research's
                2017 Energy Operations Blueprint</p>
              <div class="mx-auto text-center position-absolute bottom-30">
                <button class="button-outline button-black font-weight-bold">READ MORE</button>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="position-relative min-height-500 bg-light-blue p-4">
              <div class="shape-trapezoid position-relative">
                <span class="button-trapezoid position-absolute text-white">ARTICLE</span>
              </div>
              <p class="my-5 font-weight-bold para-style">Data-driven Energy Ecosystems for a Sustainable Future</p>
              <div class="mx-auto text-center position-absolute bottom-30">
                <button class="button-outline button-black font-weight-bold">READ MORE</button>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="position-relative min-height-500 bg-light-blue p-4">
              <div class="shape-trapezoid position-relative">
                <span class="button-trapezoid position-absolute text-white">ARTICLE</span>
              </div>
              <p class="my-5 font-weight-bold para-style">Revolutionizing the Food Supply Chain with IoT</p>
              <div class="mx-auto text-center position-absolute bottom-30">
                <button class="button-outline button-black font-weight-bold">READ MORE</button>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="position-relative min-height-500 bg-light-blue p-4">
              <div class="shape-trapezoid position-relative">
                <span class="button-trapezoid position-absolute text-white">ARTICLE</span>
              </div>
              <p class="my-5 font-weight-bold para-style">Harvest Data Across Supply Chain to Make Money</p>
              <div class="mx-auto text-center position-absolute bottom-30">
                <button class="button-outline button-black font-weight-bold">READ MORE</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="section-fifth my-5 py-5">
      <div class="container">
        <div class="text-white">
          <h1 class="heading-style">Success Stories</h1>
          <p class="para-style mb-5 mt-3 w-md-85">Revinfotech implements smart solutions for sustainable agriculture</p>
        </div>
        <div class="row">
          <div class="col-md-6 col-xl-3 my-3">
            <div class="bg-white min-height-400">
              <div class="img-box position-relative">
                <img src="https://placeimg.com/255/180/nature" class="img-fluid w-100" />
                <div class="position-absolute left-20-bottom-30">
                  <div class="shape-trapezoid-orange position-relative">
                    <span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span>
                  </div>
                </div>
              </div>
              <div class="p-4">
                <a href="#" class="text-dark hover-style anchor-style">DuPont realizes business value; gets closer to
                  customers</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="bg-white min-height-400">
              <div class="img-box position-relative">
                <img src="https://placeimg.com/255/180/people" class="img-fluid w-100" />
                <div class="position-absolute left-20-bottom-30">
                  <div class="shape-trapezoid-orange position-relative">
                    <span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span>
                  </div>
                </div>
              </div>
              <div class="p-4">
                <a href="#" class="text-dark hover-style anchor-style">DuPont realizes business value; gets closer to
                  customers</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="bg-white min-height-400">
              <div class="img-box position-relative">
                <img src="https://placeimg.com/255/180/tech" class="img-fluid w-100" />
                <div class="position-absolute left-20-bottom-30">
                  <div class="shape-trapezoid-orange position-relative">
                    <span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span>
                  </div>
                </div>
              </div>
              <div class="p-4">
                <a href="#" class="text-dark hover-style anchor-style">DuPont realizes business value; gets closer to
                  customers</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-3 my-3">
            <div class="bg-white min-height-400">
              <div class="img-box position-relative">
                <img src="https://placeimg.com/255/180/animals" class="img-fluid w-100" />
                <div class="position-absolute left-20-bottom-30">
                  <div class="shape-trapezoid-orange position-relative">
                    <span class="button-trapezoid-orange position-absolute text-white">CASE STUDY</span>
                  </div>
                </div>
              </div>
              <div class="p-4">
                <a href="#" class="text-dark hover-style anchor-style">DuPont realizes business value; gets closer to
                  customers</a>
              </div>
            </div>
          </div>
        </div>

        <div class="text-center my-5">
          <button class="button-outline button-white font-weight-bold">LEARN MORE</button>
        </div>

      </div>
    </section>
  </main>
@endsection  
