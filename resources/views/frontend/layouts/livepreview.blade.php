<!DOCTYPE html>
<html lang="en">
  <head>
    <title>@yield('title')</title>
    <!-- Meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!-- Meta tags End -->

    <!-- External CSS home-->
    <link rel="stylesheet" href="/frontend/css/footer.css" />

    <link rel="stylesheet" href="/frontend/css/commonStyle.css" />
    <link rel="stylesheet" href="/frontend/css/animate.css" />
    <link rel="stylesheet" href="/frontend/css/navbar.css" />
    
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Oswald&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
	<!-- External CSS End -->
	@stack('css')
  </head>
  <body>
      @include('frontend/header')
      @yield('content')
      @include('frontend/footer')  
  </body>
     <!-- Client Side Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
    <!-- Client Side Script End -->
	  @stack('js')
</html>
