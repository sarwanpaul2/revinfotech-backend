{{-- This extends layout page proprty to home page  --}}
    @extends('/frontend/layouts/layout')
    @section('title', 'Leadership')

<!-- External CSS abouts-->
    @push('css')
        <link rel="stylesheet" href="/frontend/css/leadership.css"/>
    @endpush

{{-- This defines content/body section here --}}
@section('content')  
<!-- This is leadership template -->
         <!-- Career's 1st Screen Starts here -->        
            <div class="container-fluid leadershipBanner">
            <div class="row">
            <div class="col-lg-8 col-xl-6 bannerParent d-flex align-items-center text-white">
            <div class="leadershipBannerContent px-3 px-xl-5">
            <h1 class="font-weight-bold">We Bring The New Innovation Of Creative Work</h1>
            <p>Our endeavor has always been to offer cutting-edge technology solutions that enable our clients to meet their software development requirements. Out IT solutions help global enterprises improve their software systems using our innovative and proven global delivery model.</p>
            </div>
            </div>
            </div>
            </div>
    
        <!-- Our Business Philosophy  -->
            <div class="container businessPhilosophy text-center my-5">
            <h2 ckass="font-weight-bolder mb-0">Our Business Philosophy</h2>
            <hr class="afterHead mb-5"/>
            <p class="text-muted">We are in to this from a decade. We are combining our solid business domain experience, technical expertise, profound knowledge of latest industry trends and quality-driven delivery model, we deliver solutions that go beyond meeting customer expectations.</p>
            </div>

        <!-- Our Leader's -->
            <div class="OurLeaders container">
                <h2 class="text-center mt-4 mb-0">Our Leader's</h2>
                <hr class="afterHead mb-4"/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="team-member">
                                <div class="team-img">
                                    <img src="/images/ceo.png" alt="team member" class="img-fluid img-thumbnail">
                                </div>
                                <div class="team-hover">
                                    <div class="desk">
                                        <p class="text-muted pt-4">RevInfotech’s CEO Navdeep Garg is a natural born problem solver with a nearly supernatural eye for detail. All these qualities along with his passion for the company have been crucial in the development of RevInfotech's positive and amazing culture..</p>
                                    </div>
                                    <div class="s-link">
                                    <i class="fab fa-linkedin"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="team-title">
                                    <h5 class="font-weight-bold">Navdeep Garg</h5>
                                    <span>CEO</span>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="/images/coo.png" alt="team member" class="img-fluid img-thumbnail">
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <p class="text-muted pt-4">Vipin (COO) is a person of many talents with accomplishments and interests in a number of fields including Business Development, Sales and Operation. Vipin’s exceptional ethics, strong communication skills and sound management abilities make him a stellar leader.</p>
                                </div>
                                <div class="s-link">
                                <i class="fab fa-linkedin"></i>
                                </div>
                            </div>
                        </div>
                            <div class="team-title">
                                <h5 class="font-weight-bold">Vipin Goyal</h5>
                                <span>COO</span>
                            </div>
                        </div>
                    </div>
            </div>
@endsection
