{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Blog')

<!-- External CSS abouts-->
@push('css')
  <link rel="stylesheet" href="/frontend/css/blog.css" />
@endpush

{{-- This defines content/body section here --}}
@section('content')       
    <div class="container-fluid blogstudyBanner d-flex align-items-center" id="blogBanner">
    <h1 class="text-white font-weight-bold container" id="blogHead"><span>Our</span> Blogs</h1>
    </div>

    <!-- Nav tabs -->
    <div  class="container blogListParent">  
      <div class="tab-content mt-3">
        <div class="row my-5">
        <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="bloglistSec mb-4 position-relative">
        <div class="position-relative">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        </div>
        <div class="blogstudySecondSec px-3 py-3 bg-white">
        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
        <a href="/front-blogs/blogdetail" class="blogRead">Read More..</a>
        </div>  
        </div>
        </a>
        </div>
        
        <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
        <a href="#">
        <div class="bloglistSec mb-4 position-relative">
        <div class="position-relative">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        </div>
        <div class="blogstudySecondSec px-3 py-3 bg-white">
        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
        <a href="#" class="blogRead">Read More..</a>
        </div>  
        </div>
        </a>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
        <a href="#">
        <div class="bloglistSec mb-4 position-relative">
        <div class="position-relative">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        </div>
        <div class="blogstudySecondSec px-3 py-3 bg-white">
        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
        <a href="#" class="blogRead">Read More..</a>
        </div>  
        </div>
        </a>
        </div>

          <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
        <a href="#">
        <div class="bloglistSec mb-4 position-relative">
        <div class="position-relative">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        </div>
        <div class="blogstudySecondSec px-3 py-3 bg-white">
        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
        <a href="#" class="blogRead">Read More..</a>
        </div>  
        </div>
        </a>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-3">
                <a href="#">
                <div class="bloglistSec mb-4 position-relative">
                <div class="position-relative">
                <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                </div>
                <div class="blogstudySecondSec px-3 py-3 bg-white">
                <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                <a href="#" class="blogRead">Read More..</a>
                </div>  
                </div>
                </a>
                </div>
                
                <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                <a href="#">
                <div class="bloglistSec mb-4 position-relative">
                <div class="position-relative">
                <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                </div>
                <div class="blogstudySecondSec px-3 py-3 bg-white">
                <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                <a href="#" class="blogRead">Read More..</a>
                </div>  
                </div>
                </a>
                </div>
        
                <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                <a href="#">
                <div class="bloglistSec mb-4 position-relative">
                <div class="position-relative">
                <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                </div>
                <div class="blogstudySecondSec px-3 py-3 bg-white">
                <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                <a href="#" class="blogRead">Read More..</a>
                </div>  
                </div>
                </a>
                </div>
        
                
                <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                <a href="#">
                <div class="bloglistSec mb-4 position-relative">
                <div class="position-relative">
                <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                </div>
                <div class="blogstudySecondSec px-3 py-3 bg-white">
                <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                <a href="#" class="blogRead">Read More..</a>
                </div>  
                </div>
                </a>
                </div>


                <div class="col-md-6 col-lg-4 col-xl-3">
                        <a href="#">
                        <div class="bloglistSec mb-4 position-relative">
                        <div class="position-relative">
                        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                        </div>
                        <div class="blogstudySecondSec px-3 py-3 bg-white">
                        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                        <a href="#" class="blogRead">Read More..</a>
                        </div>  
                        </div>
                        </a>
                        </div>
                        
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                        <a href="#">
                        <div class="bloglistSec mb-4 position-relative">
                        <div class="position-relative">
                        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                        </div>
                        <div class="blogstudySecondSec px-3 py-3 bg-white">
                        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                        <a href="#" class="blogRead">Read More..</a>
                        </div>  
                        </div>
                        </a>
                        </div>
                
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                        <a href="#">
                        <div class="bloglistSec mb-4 position-relative">
                        <div class="position-relative">
                        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                        </div>
                        <div class="blogstudySecondSec px-3 py-3 bg-white">
                        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                        <a href="#" class="blogRead">Read More..</a>
                        </div>  
                        </div>
                        </a>
                        </div>
                
                          <div class="col-md-6 col-lg-4 col-xl-3 mb-3">
                        <a href="#">
                        <div class="bloglistSec mb-4 position-relative">
                        <div class="position-relative">
                        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
                        <p class="blogDate mb-0  position-absolute"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
                        </div>
                        <div class="blogstudySecondSec px-3 py-3 bg-white">
                        <h6 class="mb-2 text-dark font-weight-bold">Lorem Ipsum is simply dummy simply dummy.</h6>
                        <p class="text-muted blogPara mt-2 mb-3">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
                        <a href="#" class="blogRead">Read More..</a>
                        </div>  
                        </div>
                        </a>
                        </div>

        </div>
    </div>
    </div>
@endsection
    