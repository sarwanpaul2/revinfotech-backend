
  <!-- Footer Start -->
  <footer class="footer add-bg pt-5">
            <div class="container">
                    <div class="row">
                      <div class="col">
                        <h4 class="font-weight-bold mb-3 text-white">Company</h4>
                        <ul class="pl-0 list-group-style kill-ul-style  ">
                          <li class="list-item"><a href="#" class="footer-list-link">Navigate your next</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">About Us</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Careers</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Investors</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Newsroom</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <h4 class="font-weight-bold mb-3 text-white">Subsidiaries</h4>
                        <ul class="pl-0 kill-ul-style">
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Blockchain</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech BPM</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Consulting</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Public Services</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Systems</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <h4 class="font-weight-bold mb-3 text-white">Programs</h4>
                        <ul class="pl-0 kill-ul-style">
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Foundation</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Foundation USA</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Revinfotech Leadership Institute</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Sustainability</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <h4 class="font-weight-bold mb-3 text-white">Support</h4>
                        <ul class="pl-0 kill-ul-style">
                          <li class="list-item"><a href="#" class="footer-list-link">Terms of Use</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Privacy Statement</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Cookie Policy</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Safe Harbour Provision</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Trademarks</a></li>
                          <li class="list-item"><a href="#" class="footer-list-link">Site Map</a></li>
                          
                          <li class="list-item"><a href="#" class="footer-list-link">Payment Guide for Suppliers</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <h4 class="font-weight-bold mb-3 text-nowrap text-center text-white">Connect with us</h4>
                        <div class="social-icons text-center mb-3">
                          <a href="/home" class="text-secondary"><i class="fab fa-linkedin-in"></i></a>
                          <a href="#" class="text-secondary"><i class="fab fa-twitter mx-1"></i></a>
                          <a href="#" class="text-secondary"><i class="fab fa-facebook-f mx-1"></i></a>
                          <a href="#" class="text-secondary"><i class="fab fa-youtube mx-1"></i></a>
                        </div>
                        <h4 class="font-weight-bold mb-3 text-nowrap text-center text-white">Contact Us</h4>
                        <div class="social-icons text-center mb-3">
                          <a href="/front-contactus/contactus" class="text-secondary"><i class="fa fa-address-book mx-1"></i></a>
                        </div>
                      </div>

                    </div>
                </div>
                    <div class="footer-bottom py-2 ">
                        <div class="container">
                            <div class="row">
                            <div class="d-flex justify-content-center align-items-center w-100">
                                <p class="m-0 text-white">Copyright &copy; <?php echo date('Y') ?> Revinfotech</p>
                            </div>
                            </div>
                        </div>
                    </div>

  </footer>
  <!-- Footer End -->