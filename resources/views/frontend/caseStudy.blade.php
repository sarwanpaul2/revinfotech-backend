
{{-- This extends layout page proprty to home page  --}}
   @extends('/frontend/layouts/layout')
   @section('title', 'Case Study')

<!-- External CSS abouts-->
   @push('css')
      <link rel="stylesheet" href="/frontend/css/casestudy.css" />
   @endpush

{{-- This defines content/body section here --}}
@section('content')         
    <div class="container-fluid casestudyBanner d-flex align-items-center" id="caseBanner">
    <h1 class="text-white font-weight-bold mx-2" id="caseHead">Menu 1</h1>
    </div>

    <!-- Nav tabs -->
    <div  class="caseListParent">
     <ul class="nav nav-tabs caseList px-3" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#" onclick="changeText(event,'rgb(70, 28, 157)')">Menu 1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu1" onclick="changeText(event,'#5f0930')">Menu 2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu2" onclick="changeText(event,'#3F51B5')">Menu 3</a>
        </li>
      </ul>
      <!-- Tab panes -->
      
      <div class="tab-content mt-3">
        <div id="home" class="container-fluid tab-pane active tab-pane-custom"><br>
        <div class="row">
        <div class="col-md-6">
        <a href="#">
        <div class="casestudyInner position-relative mb-3">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudyInnerContent text-white px-3 py-4">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
        <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        <a href="/front-casestudy/casestudydetails">
        <span class="d-flex align-items-center justify-content-center viewAllArrow">
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
        </span>
        </a>
        </div>  
        </div>
        </div>
        </a>
        </div>

        <div class="col-md-6">
        <a href="#">
        <div class="casestudyInner position-relative mb-3">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudyInnerContent text-white px-3 py-4">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
        <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
        <span class="d-flex align-items-center justify-content-center viewAllArrow">
        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
        </span>
        </div> 
        </div>  
        </div>
        </a>
        </div>
        </div>

        <div class="row my-5">
        <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
           <div class="col-md-6 col-lg-4 col-xl-3">
        <a href="#">
        <div class="caselistSec mb-4">
        <img src="/images/sustainability-2018-19-home.jpg" class="img-fluid">
        <div class="casestudySecondSec px-3 py-4 bg-white">
        <h5 class="mb-2 text-dark">Lorem Ipsum is simply dummy text simply dummy text.</h5>
        <div class="dateArrow d-flex justify-content-between align-items-center mt-3">
         <p class="caseDate mb-0 text-muted"><i class="fas fa-calendar-day"></i><span class="pl-1">10 Sep 2019</span></p>
         <span class="d-flex align-items-center justify-content-center viewAllArrow">
         <i class="fa fa-angle-double-right" aria-hidden="true"></i>
         </span>
         </div>  
        </div>  
        </div>
        </a>
        </div>
        
        </div>
    </div>
    </div>
@endsection
    