{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Contact Us')

@push('css')
<!-- External CSS abouts-->
<link rel="stylesheet" href="/frontend/css/contactUs.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"     crossorigin="anonymous"/>
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
@endpush

{{-- This defines content/body section here --}}
@section('content')
<!-- error message shows here -->
<div class="contact-form text-center text-white py-5 px-4">
    @if ($message = Session::get('danger'))
    <!-- <div class="alert alert-danger alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
    </div> -->
        <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
        <div class="modal-dialog modal-dialog-top">
            <div class="modal-content" style="background-color:#f2dede;">
                <!-- Modal Header -->
                <div class="modal-header">
                    {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                </div>
                <!-- Modal body -->
                <div class="modal-body" >
                    <strong style="color:#a94442;">{{ $message }}</strong>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($message = Session::get('success'))
    <!-- <div class="alert alert-success alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
    </div> -->
    <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
            <div class="modal-dialog modal-dialog-top">
                <div class="modal-content" style="background-color:#dff0d8;">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" >
                        <strong style="color:#3c763d;">{{ $message }}</strong>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if ($errors->any())
    <!-- <div class="alert alert-danger alert-block" id="danger-alert">
        <button type="button" class="close contactclose" data-dismiss="alert">×</button>    
            <strong>{{ $message }}</strong>
    </div> -->
        <!-- The Modal -->
    <div class="modal" id="myModal" style="margin-top:100px;">
        <div class="modal-dialog modal-dialog-top">
            <div class="modal-content" style="background-color:#f2dede;">
                <!-- Modal Header -->
                <div class="modal-header">
                    {{-- <h4 class="modal-title">Modal Heading</h4> --}}
                </div>
                <!-- Modal body -->
                <div class="modal-body" >
                    <strong style="color:#a94442;">{!! implode('', $errors->all('<div>:message</div>')) !!}</strong>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> --}}
                </div>
            </div>
        </div>
    </div>
    @endif
<!-- Main Page Starts here -->
<div class="wrap-main">
        <!-- Top Banner Starts here -->
        <div class="banner">
            <p class="alter-p-banner-float position-absolute text-center">How Can We Help You?</p>
        </div>
        <!-- Top Banner Ends here -->
        <!-- Display text Starts here -->
        <div class="wrap-dt">
            <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="display-text d-flex justify-content-center align-items-center">
                                    <div class="mt-4">
                                        <p class="text-center">Use the form below to drop us an e-mail.</p>
                                    </div>
                                    <div class="arrow-container animated fadeInDown">
                                            <div class="arrow-2">
                                              <i class="fa fa-angle-down"></i>
                                            </div>
                                            <div class="arrow-1 animated hinge infinite zoomIn"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                    </div>
            </div>
        </div>
          <!-- Display text End here -->
        <!-- SVG triangle starts here -->
        <div class="wrap-triangle">
            <svg class="pointer" xmlns="https://www.w3.org/2000/svg" version="1.1" width="100%" viewBox="0 0 100 102" preserveAspectRatio="none">
                <path d="M0 0 L50 100 L100 0 Z"></path>
            </svg>
        </div>
       <!-- SVG triangle ends here -->

       <!-- Form Area Starts here -->
       <div class="wrap-form">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-center align-items-center">
                        <div class="shadow-lg p-5 mb-5 bg-white alter-form-radius">
                             <form method="post" action="{{url('/front-contactus/contact-request')}}" enctype="multipart/form-data" id="contact_form">
                                   @csrf
                                    <div class="d-flex flex-column justify-content-center mb-5">
                                        <div>
                                            <p class="alter-p-form-head text-center mb-3">Enquiry Form</p>
                                        </div>
                                        <div>
                                            <hr class="alter-hr">
                                        </div>
                                    </div>

                                    <div class="d-flex flex-column flex-md-row justify-content-between">
                                        <input type="text" name="first_name" value="" class="alter-text-field"  placeholder="First Name" required/>
                                        <input type="text" name="last_name" value=""  class="alter-text-field" placeholder="Last Name*" required/>
                                    </div>
                
                                    <div class="d-flex flex-column flex-md-row justify-content-between">
                                        <input type="email" name="email" value="" class="alter-text-field"  placeholder="Email*" required/>
                                        <input type="text" name="website" value="" class="alter-text-field"  placeholder="Website" />
                                    </div>    
                            
                                    <div class="d-flex flex-column flex-md-row justify-content-between">
                                        <input type="text" name="company" value="" class="alter-text-field"  placeholder="Company" />
                                        <input type="number" name="phone" value=""  class="alter-text-field" placeholder="Phone" required/>
                                    </div>    
                                    
                                    <div class="d-flex">
                                        <textarea class="alter-text-area" name="details" cols="40" rows="10" placeholder="What can we do for you?"></textarea>
                                        <input type="hidden" id="get-url" name="page_name">
                                    </div>
                                    <div class="input-group-c mb-5 g-recaptcha" data-sitekey="6LcOFMIUAAAAAKWl2DMRS7mR19VDpnnHQZUODLdl"></div>
                                    <p class="recaptcha-badge text-justify mt-4" style="color:#807b7b;">This site is protected by reCAPTCHA and the Google
                                    <br> <input type="checkbox" id="termsPrivacy" name="termsPrivacy" value="yes">
                                    <a href="https://policies.google.com/privacy" target="_blank">Privacy Policy</a> and <a href="https://policies.google.com/terms" target="_blank">Terms of Service</a> apply.</p>
                                    <div class="d-flex justify-content-center mt-5">
                                        <input id="button_check" type="button" value="Send It" class="alter-btn-send" /> 
                                    </div>
                            </form>
                        </div>    
                       
                    </div>
                   
                </div> 
                  
            </div>   
       </div>
       <!-- Form Area Ends here -->
       
       <!-- Bottom Area Starts here -->
       <div class="wrap-bottom">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-md-6 col-12 p-0">
                        <div class="left-bottom p-4">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h3 class="mb-4"><span class="left-bottom-head">Branch Office</span></h3> 
                                 
                                       <div class="contact_detail">
                                         <!-- <p> -->
                                           <p class=""><i class="fa fa-map-marker mr-2 color-change" aria-hidden="true"></i>3762 , Blackthorn St, Irvine, CA 92606,
                                 
                                 Los Angeles United States</p>
                                               <p>
                                                 <i class="fa fa-phone color-change mr-2" aria-hidden="true"></i>+1-9492080179              </p>
                                                 <p>
                                                 <a class="color-ch-a" href="mailto:us@revinfotech.com"><i class="fas fa-envelope mr-2 color-change"></i></i>us@revinfotech.com</a>
                                               </p>
                                                     <p>
                                                         </p>
                                                       <p></p> 
                                           <p class=""><i class="fa fa-map-marker mr-2 color-change" aria-hidden="true"></i>20871 Johnson Street,
                                 
                                 Suite 116 Pembroke Pines, FL 33029</p>
                                               <p>
                                                 <i class="fa fa-phone color-change mr-2" aria-hidden="true"></i>+1-9492080179              </p>
                                                 <p>
                                                 <a class="color-ch-a" href="mailto:us@revinfotech.com"><i class="fas fa-envelope mr-2 color-change"></i></i>us@revinfotech.com</a>
                                               </p>
                                                     <p>
                                                         </p>
                                                       <p></p> 
                                                             </div>
                                                   
                                                   
                                                 </div>
                        </div>
                   </div>

                   <div class="col-md-6 col-12 p-0">
                        <div class="right-bottom d-flex justify-content-center align-items-center flex-column p-4 h-100">
                            <img class="img-fluid logo-bottom" src="images/logo.png" alt="">
                            <div class="address text-white text-center">
                                    <span class="street-address">15 West 3rd St</span> <span class="pipe">/</span> <span class="locality">Media</span>, <span class="region">PA</span> <span class="postal-code">19063</span><br>
                                    <span class="tel"><a class="text-white" href="tel:14843242400">484-324-2400</a></span> <span class="pipe">/</span> <a class="email text-white" href="mailto:info@mediaproper.com">info@mediaproper.com</a>
                            </div>
                            <div>
                                <p class="mb-0 text-white">Get Directions</p>
                            </div>
                            <div>
                                <p class="social mb-0"><a class="mr-2 text-white" href="http://www.facebook.com/MediaProper" aria-label="Media Proper on Facebook"><i class="fab fa-facebook-square"></i></a>
                                <a class="text-white" href="https://www.google.com/maps/place/Media+Proper/@39.9208954,-75.3893823,15z/data=!4m5!3m4!1s0x0:0xcc7c94e88548c580!8m2!3d39.9208954!4d-75.3893823?hl=en" aria-label="Media Proper on Google"><i class="fab fa-google"></i></a>
                                </p>
                            </div>

                            <div>
                                <a class="btn-pill mt-2" href="#">Start Your Project</a>
                            </div>

                            <div class="mt-4">
                                      
                                <ul id="menu-footer-legal" class="menu d-flex">
                                <li id="menu-item-3028" class="mr-2"><a href="https://mediaproper.com/contact-us/" aria-current="page">Contact</a></li>
                                <li id="menu-item-4893" class="mr-2"><a href="https://mediaproper.com/service-areas/">Service Areas</a></li>
                                <li id="menu-item-2993" class="mr-2"><a href="https://mediaproper.com/web-design-jobs/">Careers</a></li>
                                <li id="menu-item-5871" class="mr-2"><a href="https://mediaproper.com/shop/media-proper-logo-t-shirt-unisex/">Store</a></li>
                                <li id="menu-item-3049" class="mr-2"><a href="https://mediaproper.com/privacy-policy/">Privacy Policy</a></li>
                                </ul>	
                            
                            </div>
                        </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- Bottom Area Ends here -->

    </div>
    <!-- Main Page Ends here -->

@endsection  

@push('js')
 <!-- Client Side Script -->
 <script src='https://www.google.com/recaptcha/api.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
    <script type="text/javascript">
     $(document).ready(function(){
     $("#myModal").fadeIn( 300 ).delay( 1500 ).fadeOut( 800); 

     $("#button_check").click(function(e){
                if ($("#termsPrivacy").is(":checked")) { 
                    $('#contact_form').submit(); 
                } else { 
                    alert("Please accept the Terms and Conditions & Privacy Policy."); 
                }
            });

     var get_url = window.location.href;
            $('#get-url').val(get_url);   
        });
   
    </script>
<!-- Client Side Script End -->
@endpush