{{-- This extends layout page proprty to home page  --}}
@extends('/frontend/layouts/layout')
@section('title', 'Home')
@push('css')
  <link rel="stylesheet" href="/frontend/css/home.css" />
@endpush  
{{-- This defines content/body section here --}}
@section('content')
<!-- identifier data comes here -->
@php 

if((!empty($page) && ($page->status == 0))){
  foreach ($identifiers as $value) {
      $title_data = $helper->getIdentifiersData($value->name, $value->template_id);
      $data->layout  = str_replace($value->name, $title_data->value, $data->layout);
  }
  echo $data->layout; 
}else{
@endphp
		
    <!-- The Next -->
    <div class="wrap-5 mt-5">
      <div class="container-fluid p-custom-0">
        <div class="container-fluid h-fix">
          <div class="row">
            <div class="col-md-12 text-center alter-col-bottom">
              <h1 class="alter-h1-head"><img
                class="img-fluid logo-revinfotech"
                src="/images/logo.png"
                alt="no logo"
                id="companyLogo"
              /></h1>
              <p class="alter-p-wrap-5">
                We bring you powerful advantages to navigate your digital
                transformation
              </p>
            </div>
          </div>

          <div class="row">
            
              <div class="col-lg-3 alter-fixation">
                  <div class="row">
                    <div class="col-6 col-lg-12 content-parent image-holder-div2">
                      <div
                        class="content-item d-flex align-items-center justify-content-center text-center"
                      >
                        <div>
                          <h3>Heading</h3>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry's standard dummy text ever since the 1500s,
                          </p>
                        </div>
                      </div>
                      <h5 class="contentHeading position-absolute text-white">
                        Heading
                      </h5>
                    </div>
    
                    <div class="col-6 col-lg-12 content-parent image-holder-div3">
                      <div
                        class="content-item d-flex align-items-center justify-content-center text-center"
                      >
                        <div>
                          <h3>Heading</h3>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry's standard dummy text ever since the 1500s,
                          </p>
                        </div>
                      </div>
                      <h5 class="contentHeading position-absolute text-white">
                        Heading
                      </h5>
                    </div>
                </div>
              </div>

            <div class="col-lg-3 alter-fixation p-0">
              <div class="content-parent image-holder-div1">
                <div
                  class="content-item d-flex align-items-center justify-content-center text-center"
                >
                  <div>
                    <h3>Heading</h3>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s,
                    </p>
                  </div>
                </div>
                <h5 class="contentHeading position-absolute text-white">
                  Heading
                </h5>
              </div>
            </div>

            <div class="col-lg-6 alter-fixation">
              <div class="row">
                <div class="col-6 content-parent image-holder-div2">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div3">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div4">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div5">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- The Next End -->



    <!-- About Us -->
    <div class="wrap-7 my-5">
      <div class="container-fluid p-custom-0">
        <div class="container h-fix">
          <div class="row">
            <div class="col-md-12 text-center alter-col-bottom mb-5">
              <h1 class="alter-h1-head">About us</h1>
              <p class="alter-p-wrap-5">
                We bring you powerful advantages to navigate your digital
                transformation
              </p>
            </div>
          </div>

          <div class="row mx-auto mb-5">
            <div class="col-md-3">
              <h1 class="text-center font-weight-bold" id="countries">0</h1>
              <p class="text-center">countries where we have happy customers</p>
            </div>

            <div class="col-md-3">
              <h1 class="text-center font-weight-bold" id="revenue">12.1</h1>
              <p class="text-center">billion total revenue (LTM)</p>
            </div>

            <div class="col-md-3">
              <h1 class="text-center font-weight-bold" id="employees">
                116,000
              </h1>
              <p class="text-center">Agile enabled employees</p>
            </div>

            <div class="col-md-3">
              <h1 class="text-center font-weight-bold" id="skills">106,000</h1>
              <p class="text-center">employees trained in new skills</p>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6 alter-fixation p-0">
              <div class="content-parent image-holder-div1">
                <div
                  class="content-item d-flex align-items-center justify-content-center text-center"
                >
                  <div>
                    <h3>Heading</h3>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s,
                    </p>
                  </div>
                </div>
                <h5 class="contentHeading position-absolute text-white">
                  Heading
                </h5>
              </div>
            </div>

            <div class="col-lg-6 alter-fixation">
              <div class="row">
                <div class="col-6 content-parent image-holder-div2">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div3">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div4">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>

                <div class="col-6 content-parent image-holder-div5">
                  <div
                    class="content-item d-flex align-items-center justify-content-center text-center"
                  >
                    <div>
                      <h3>Heading</h3>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                      </p>
                    </div>
                  </div>
                  <h5 class="contentHeading position-absolute text-white">
                    Heading
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- About Us End -->



    <!-- Our Client  -->
    <div class="container my-5">
      <h1 class="text-center my-4">Our Partners</h1>
      <section class="customer-logos slider">
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"
          />
        </div>
        <div class="slide">
          <img
            src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"
          />
        </div>
      </section>
    </div>
    <!-- Our Client End-->


    <!-- Investor -->
    <div class="container">
    <h1 class="text-center my-4 alter-h1-head">Investor</h1>
    <div class="row">
    <div class="col-lg-7 investor-bg">
    </div>
    <div class="col-lg-5">
    <div class="investorContent p-4">
    <h1 class="font-weight-bold">Investor central</h1>
    <div class="investorItem mt-4">
    <p class="m-0">REPORTS</p>
    <h5>Annual Report 2019</h5>
    </div>
    <div class="investorItem">
    <p class="m-0">RESULTS</p>
    <h5>First Quarter FY20</h5>
    </div>
    <div class="investorItem">
    <p class="m-0">EVENT</p>
    <h5>The 38th Annual General Meeting</h5>
    </div>

    <a href="#" class="d-block my-3 font-weight-bold viewAll">View All</a>
    </div>  
    </div>
    <!-- Investor End -->

    <!-- career section -->
    <div class="container my-5 careerParent">
      <h1 class="text-center my-2">Career</h1>
      <p class="alter-p-wrap-5 mb-3">
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry
      </p>
      <div class="careerContainer pt-5 position-relative">
          <button type="button" class="d-block mx-auto mb-5">Explore Jobs</button>
        <section class="careerSlider slider">
          
          <div class="slide text-center">
            <div class="testimonialItemInner careerInner mt-2">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s
              </p>
              <h5>Brain Lara</h5>
            </div>
          </div>

          <div class="slide text-center">
            <div class="testimonialItemInner careerInner mt-2">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s
              </p>
              <h5>Brain Lara</h5>
            </div>
          </div>

          <div class="slide text-center">
            <div class="testimonialItemInner careerInner mt-2">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s
              </p>
              <h5>Brain Lara</h5>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- career section end -->
</div>
</div>
@php
}
@endphp
    @endsection
