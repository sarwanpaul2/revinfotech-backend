@extends('adminlte::page')

@section('title', 'Leads-List')

@section('content')
	<table class="table table-bordered" id="leadsTable">
		<thead>
			<th>S.No.</th>
			<th>Name</th>
			<th>Email</th>
			<th>Skype Id</th>
			<th>Phone</th>
			<th>Description</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->name}}</td>
					<td>{{$value->email}}</td>
					<td>{{$value->skypeid}}</td>
					<td>{{$value->phone}}</td>
					<td>{{$value->description}}</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>

@endsection
@push('js')


<script type="text/javascript">
	$(function () {
		$('#leadsTable').DataTable();
	});
</script>
@endpush



