{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="box panel-body btc-blue">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>10</h3>
						<p>New Pges</p>
					</div>
					<div class="icon">
						<i class="fa fa-cart-plus"></i>
					</div>
				
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>11</h3>
						<p>Total Pages</p>
					</div>
					<div class="icon">
						<i class="fa fa-shopping-cart"></i>
					</div>
				
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-maroon">
					<div class="inner">
						<h3>1</h3>
						<p>Inactive Pages</p>
					</div>
					<div class="icon">
						<i class="fa fa-user-times"></i>
					</div>
				
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-purple">
					<div class="inner">
						<h3>5</h3>
						<p>Total States</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					
				</div>
			</div>
			<!-- ./col -->
			@php
			/*
			@endphp
			<!-- <div class="col-lg-3 col-xs-6">
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{$recent_payment}}</h3>
						<p>Recent Payment</p>
					</div>
					<div class="icon">
						<i class="fa fa-rupee"></i>
					</div>
					<a href="{{route('adminPaymentReports')}}?ref=dashboard&sort=p_d" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div> -->
			<!-- ./col -->
			@php
			*/
			@endphp
		</div>
	</div>
   
	@php
	/*
	@endphp
	
			@php
			echo "<pre>";
			foreach($monthly_payments as $key => $payments)
			{
				print_r($payments);
				echo "<hr>";
			}
			//print_r($monthly_payments);
			echo "</pre>";
			@endphp
	    </div>
	</div>
	@php
	*/
	@endphp
    
@stop
@push('js')
   <!-- // -->
@endpush
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop