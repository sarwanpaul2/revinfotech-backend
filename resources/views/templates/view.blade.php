@extends('/frontend/layouts/livepreview')
@section('title', 'Live Preview')
@push('css')
<!-- External CSS abouts-->
<link rel="stylesheet" href="/frontend/css/{{$data->external_css}}.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"     crossorigin="anonymous"/>
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&display=swap" rel="stylesheet">
@endpush

@section('content_header')
<h1>Preview Template</h1>
@stop

@section('content')
<!-- identifier data comes here -->
@php 
foreach($identifiers as $value){
    $title_data = $helper->getIdentifiersData($value->name,$value->template_id);

    $data->layout  = str_replace($value->name,$title_data->value,$data->layout);    
}



    
@endphp
<!-- identifier data comes here -->

		{!! $data->layout !!}

@endsection

@push('js')
 <!-- Client Side Script -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="/frontend/js/navbar.js"></script>
    <script src="/frontend/js/main.js"></script>
<!-- Client Side Script End -->
@endpush