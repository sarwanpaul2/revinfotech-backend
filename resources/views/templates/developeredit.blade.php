@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Developer Edit Section</h1>
	<br>
	<h1><a href="{{ route('templates.index') }}"><button class="btn btn-success">Templates</button></a></h1>	
@stop

@section('content')
<br>
<form method="post" action="{{ url('templates/'.$id.'/update') }}" enctype="multipart/form-data">
        @csrf
	<div class="form-group">	
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
		
	<div class="row">
		<div class="col-md-4">
			<label for="external_css">External Css</label>
			<input type="text" class="form-control" id="external_css" name="external_css" placeholder="Enter external css" value="{{$data->external_css}}">
		</div>
		<div class="col-md-4">
			<label for="type ">Layout Type</label>
			<select class="form-control" name="type" id="type" required>
				<option value="page" @if($data->type == 'page') selected @endif>Page</option>
				<option value="email"  @if($data->type == 'email') selected @endif>Email</option>	
			</select>
		</div>
	</div>
	<br>	
    <div class="form-group">
		<label for="layout">Developer Mode</label>
		<textarea style="background-color: #262626;color:#fff;" class="form-control" id="layout" name="layout" rows="25" placeholder="Enter layout">{{$data->layout}}</textarea>
	</div>
	<div class="form-group">
		<label for="layout">Developer layout</label>
		<input type="hidden" class="form-control" id="action" name="action" value="developeredit">
	</div>

    <button type="submit" class="btn btn-primary">Submit</button>	
	

   
    <!-- /.box-body -->
    
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('layout-none',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)						[*]{*};button(*)[*]{*};main(*)[*]{*};section(*)[*]{*};h5(*)[*]{*};img(*)[*]{*}',
       });
	});
</script>
@endpush
