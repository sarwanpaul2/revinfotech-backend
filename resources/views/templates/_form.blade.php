@csrf
<div class="box-body">
	<div class="form-group">
		<label for="layout">layout</label>
		<textarea class="form-control" id="layout" name="layout" rows="10" placeholder="Enter layout"></textarea>
	</div>
	<div class="form-group">
		<label for="external_css">External Css</label>
		<input type="text" class="form-control" id="external_css" name="external_css" placeholder="Enter external css">
	</div>
	<div class="form-group">
		<label for="type ">Layout Type</label>
		<select class="form-control" name="type" id="type" required>
			<option value="page">Page</option>
			<option value="email">E-mail</option>	
		</select>
	</div>
</div>
<!-- /.box-body -->
<button type="submit" class="btn btn-primary">Submit</button>

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('layout',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)						[*]{*};button(*)[*]{*};main(*)[*]{*};section(*)[*]{*};h(*)[*]{*}',
       });
	});
</script>
@endpush