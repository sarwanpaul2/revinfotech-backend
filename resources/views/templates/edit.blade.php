@extends('adminlte::page')

@section('title', 'Edit')

@section('content_header')
    <h1>Edit Section</h1>
@stop

@section('content')
	
<form method="post" action="{{ url('templates/'.$id.'/update') }}" enctype="multipart/form-data">
        @csrf
    <div class="form-group">
		<label for="layout">layout</label>
		<textarea class="form-control" id="layout" name="layout" rows="5" placeholder="Enter layout">{{$data->layout}}</textarea>
	</div>
	<div class="form-group">
		<label for="external_css">External Css</label>
		<input type="text" class="form-control" id="external_css" name="external_css" placeholder="Enter external css" value="{{$data->external_css}}">
	</div>
    <!-- <div class="form-group">
		<label for="page_id ">Page</label>
		<select class="form-control" name="page_id" placeholder="Please select an option" required="true">
        @foreach($pages_data as $value){
            <option value="{{$value->id}}"  @if($value->id == $data->page_id) selected @endif>{!! $value->name !!}</option>
         @endforeach	
		</select>
	</div> -->
	<div class="form-group">
		<label for="type ">Layout Type</label>
		<select class="form-control" name="type" id="type" required>
            <option value="page" @if($data->type == 'page') selected @endif>Page</option>
            <option value="email"  @if($data->type == 'email') selected @endif>Email</option>	
		</select>
	</div>

   
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('layout',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)						[*]{*};button(*)[*]{*};main(*)[*]{*};section(*)[*]{*};h5(*)[*]{*};img(*)[*]{*};script(*)[*]{*}',
       });
	});
</script>
@endpush
