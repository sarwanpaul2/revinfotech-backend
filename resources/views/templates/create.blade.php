@extends('adminlte::page')

@section('title', 'Add')

@section('content_header')
<h1>Add Template</h1>
@stop

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
</div>
@endif
<form method="post" action="{{ route('templates.store') }}" enctype="multipart/form-data">
@include('templates/_form')
	
</form>
@endsection

