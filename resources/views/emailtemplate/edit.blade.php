@extends('adminlte::page')

@section('title', 'Email Template-List')

@section('content_header')
    <h1>Edit Email Template</h1>
@stop

@section('content')
	
	<form method="post" action="{{url('/emailtemplate/update/'.$data->id)}}" enctype="multipart/form-data">
		@csrf
		<div class="box-body">
			<div class="form-group">
				<label for="template">Template</label>
				<textarea id="template" name="template" rows="10" cols="80" required="required">{{$data->template}}</textarea>
			</div>
		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('template',{
            extraAllowedContent: '*[id];*(*);*{*};p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};span(*)[*]{*};table(*)[*]{*};td(*)[*]{*}',
        });
	});
</script>
@endpush