@extends('adminlte::page')

@section('title', 'Email Template-List')

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="myTable_menu">
		<thead>
			<th>S.No.</th>
			<th>Name</th>
			<th>Action</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($emailtemplates as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->name}}</td>
					<td>
						<a href="{{url('emailtemplate/edit/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
						<button class="btn btn-danger" onClick="deleteMenu(this,{{$value->id}})">Delete</button>
					</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Delete Records</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Are you sure you want to delete this record?
	      </div>
	      <div class="modal-footer">
	      	<form method="POST" action="{{url('/menudelete')}}">
	      		@csrf
	      		<input type="hidden" name="delid" id="delbtn" />
	        	<button type="submit" class="btn btn-danger">Delete</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      	</form>
	      </div>
	    </div>
	  </div>
	</div>
	<script type="text/javascript">
		function deleteMenu(delthis, id){
			$('#exampleModal').modal('show');
			$('#delbtn').val(id);

		}
	</script>
@endsection


@push('js')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#myTable_menu').DataTable();
	});
</script>
@endpush
