@extends('adminlte::page')
@section('title', 'Google Api')
@section('content_header')
    <h1>Enviornment Settings</h1>
@stop
@section('content')

@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button> 
			<strong>{{ $message }}</strong>
	</div>
  @endif
  
  <div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update Enviornment Settings</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        
        <form method="post" action="{{ route('envsettings.update', $settings->id) }}">
            @method('Post') 
            @csrf
            <div class="form-group">

                <label for="setting_name">Setting Name:</label>
                <input type="text" class="form-control" name="setting_name" value={{ $settings->setting_name }} />
            </div>
            <div class="form-group">
                <label for="setting_description">Setting Description:</label>
                <input type="text" class="form-control" name="setting_description" value={{ $settings->setting_description }} />
            </div>
            <div class="form-group">
              <label for="setting_description">Setting Value:</label>
              <input type="text" class="form-control" name="setting_value" value={{ $settings->setting_value }} />
          </div>

            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#myTable1').DataTable({
    'scrollX':true
  });
});
</script>
@endpush