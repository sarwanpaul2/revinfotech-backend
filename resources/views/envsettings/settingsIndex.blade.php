@extends('adminlte::page')
@section('title', 'Google Api')
@section('content_header')
    <h1>Environment Settings List</h1>
@stop
@section('content')

@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button> 
			<strong>{{ $message }}</strong>
	</div>
  @endif
  
  <div class="col-sm-12">
    @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}  
      </div>
    @endif
  </div>
<div class="box box-primary">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <div>
      <a style="margin: 19px;" href="{{ route('envsettings.createform')}}" class="btn btn-primary">Create Settings</a>
  </div>  
  <div class="box-body">
    <div class="form-group">
        <!-- <h1 class="display-3">Pages Index</h1>     -->
      <table class="table table-striped" id="myTable1" style="width:100%">
        <thead>
            <tr>
                <th>S.No</th>
                {{-- <th>Id</th> --}}
                <th>Name</th>
                <th>Description</th>
                <th>Value</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
                
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($data as $value)
            <tr>
                <td>{{$i}}</td>
                {{-- <td>{{$value->id}}</td> --}}
                <td>{{$value->setting_name}}</td>
                <td>{{$value->setting_description}}</td>
                <td>{{$value->setting_value}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->updated_at}}</td>
                <td>
                  <a href="{{url('envsettings/edit/'.$value->id)}}"><button class="btn btn-success">Edit</button></a>
                </td>
            <?php $i++; ?>
            @endforeach
        </tbody>
      </table>
    <div>
  </div>
  
</div>

@endsection

@push('js')
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#myTable1').DataTable({
    'scrollX':true
  });
});
</script>
@endpush