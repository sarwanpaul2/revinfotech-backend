
@extends('adminlte::page')
@section('title', 'Google Api')
@section('content_header')
    <h1>Form</h1>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				{{-- <div class="card-header">{{ __('AasdPI Form') }}</div> --}}
					<div class="card-body">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Enviornment Setting Form</h3>
							</div>
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div><br />
							@endif
							<!-- /.box-header -->
							<!-- form start -->
								<form method="post" id="myForm" name="myForm" action="{{ route('envsettings.addForm') }}">
								@csrf
								
									<div class="box-body">
										<div class="form-group">
											<label for="slug1">Setting Name</label>
											<input type="text" class="form-control" name="setting_name" placeholder="Enter Setting Name" required="true" >
										</div>
										<div class="form-group">
											<label for="meta_title">Setting Description</label>
											<input type="text" class="form-control" name="setting_description" id="setting_description" placeholder="Enter Setting Description" required="true">
										</div>
										<div class="form-group">
											<label for="meta_title">Setting Value</label>
											<input type="text" class="form-control" name="setting_value" placeholder="Enter Setting Value"  required="true" >
											
										</div>
									</div>
									<!-- /.box-body -->
									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
									
								</form>
						</div>
					</div>
            	</div>		
            </div>	
		</div>	
	</div>
	<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
	Launch demo modal
  </button> --}}
  
  <!-- Modal -->
 
</div>	
   	
@endsection	
@push('js')
<script type="text/javascript">
	$(function(){
	
	});
   </script>
@endpush
