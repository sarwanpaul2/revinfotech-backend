@extends('adminlte::page')

@section('title', 'Contact us')

@section('content_header')
    <h1>Contact us</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/contactus')}}">
		@csrf
	<div class="box-body">

			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Title">
			</div>
			
			<div class="form-group">
				<label for="meta_title">Meta Title</label>
					<textarea class="form-control" id="meta_title" name="meta_title" rows="3" placeholder="Enter meta title"></textarea>
			</div>
			
			<div class="form-group">
				<label for="keyword">Meta Description</label>
					<textarea class="form-control" id="keyword" name="meta_description" rows="3" placeholder="Enter Meta Description"></textarea>
			</div>

			<div class="form-group">
				<label for="keyword">Meta KeyWord</label>
					<textarea class="form-control" id="keyword" name="keyword" rows="3" placeholder="Enter keyword"></textarea>
			</div>
			
			<div class="form-group">
				<label for="desc1">Address</label>
				<textarea id="desc1" name="address" rows="10" cols="80" required="required"></textarea>
			</div>

			<div class="form-group">
				<label for="phone">Phone 1</label>
				<input type="text" class="form-control" id="phone1" name="phone1" placeholder="Phone">
			</div>

			<div class="form-group">
				<label for="phone">Phone 2</label>
				<input type="text" class="form-control" id="phone2" name="phone2" placeholder="Phone">
			</div>

			<div class="form-group">
				<label for="email">email</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="Email">
			</div>

		</div>
		<!-- /.box-body -->

		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('address');
	});
</script>
@endpush