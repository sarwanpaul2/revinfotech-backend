@extends('adminlte::page')

@section('title', 'Blog-List')

@section('content_header')
    <h1>Edit Blog</h1>
@stop

@section('content')
	
<form method="post" action="{{url('/industriesupdate/'.$data->id)}}" enctype="multipart/form-data">
		@csrf
	<div class="box-body">
        <div class="form-group">
            <label for="meta_title">Background Image</label>
            <input type="file" id="bg_image" name="bg_image" placeholder="bg_image">
            @if($data->bg_image)
            <img src="{{Storage::disk('s3')->url('images/'.$data->bg_image)}}" width="100" height="100"/>
            @else
            <p>No Image</p>
            @endif
        </div>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$data->title}}">
        </div>
        
        <div class="form-group">
            <label for="description_first">Description first</label>
                <textarea class="form-control" id="description_first" name="description_first" rows="3" placeholder="Enter meta title">{{$data->description_first}}</textarea>
        </div>
        
        <div class="form-group">
            <label for="link_first">Link first</label>
            <input type="text" class="form-control" id="link_first" name="link_first"  placeholder="Enter link first" value="{{$data->link_first}}">
        </div>
        <div class="form-group">
            <label for="title_second">Title Second</label>
            <input type="text" class="form-control" id="title_second" name="title_second" placeholder="Title second" value="{{$data->title_second}}">
        </div>

        <div class="form-group">
        <label for="description_second">Description second</label>
            <textarea class="form-control" id="description_second" name="description_second" rows="3" placeholder="Enter description second">{{$data->description_second}}</textarea>
        </div>
    
        <div class="form-group">
            <label for="link_second">Link second</label>
            <input type="text" class="form-control" id="link_second" name="link_second"  placeholder="Enter link second" value="{{$data->link_second}}">
        </div>

    </div>
    <!-- /.box-body -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush