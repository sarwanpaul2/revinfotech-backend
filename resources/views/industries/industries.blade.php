@extends('adminlte::page')

@section('title', 'Blog')

@section('content_header')
    <h1>Industries</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('/industriescreate')}}" enctype="multipart/form-data">
		@csrf
	    <div class="box-body">

			<div class="form-group">
					<label for="meta_title">Background Image</label>
					<input type="file" id="image_1" name="bg_image" placeholder="bg_image" required="true">
			</div>
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Enter title" required="true">
			</div>

			<div class="form-group">
				<label for="description_first">Description First</label>
				<textarea class="form-control" id="description_first" name="description_first" placeholder="Enter description first" required="true"></textarea>
			</div>
			
			<div class="form-group">
				<label for="link_first ">Link First</label>
				<input type="text" class="form-control" id="link_first" name="link_first" rows="3" placeholder="Enter link first" required="true">
			</div>
			
			<div class="form-group">
				<label for="keyword">Title Second</label>
				<input type="text"class="form-control" id="title_second" name="title_second" rows="3" placeholder="Enter title second" required="true">
			</div>

			<div class="form-group">
				<label for="keyword">Description Second</label>
				<textarea class="form-control" id="description_second" name="description_second" rows="3" placeholder="Enter Description Second" required="true"></textarea>
			</div>
			<div class="form-group">
				<label for="link_second ">Link Second</label>
				<input type="text" class="form-control" id="link_second" name="link_second" rows="3" placeholder="Enter link second" required="true">
			</div>
			
		</div>
		<!-- /.box-body -->
		<button type="submit" class="btn btn-primary">Submit</button>
		
	</form>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('description_1');
	});
</script>
@endpush