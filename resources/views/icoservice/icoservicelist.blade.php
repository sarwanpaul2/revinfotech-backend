@extends('adminlte::page')

@section('title', 'Ico Service Page-List')

@section('content_header')
<h1>Ico Service Page Records</h1>
@stop

@section('content')
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	@if ($message = Session::get('delete'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	        <strong>{{ $message }}</strong>
	</div>
	@endif
	<table class="table table-bordered" id="myTable_ico">
		<thead>
			<th>S.No.</th>
			<th>Email</th>
		</thead>
		<tbody>
			<?php $i=1; ?>
			@foreach($data as $value)
				<tr>
					<td>{{$i}}</td>
					<td>{{$value->email}}</td>
				</tr>
			<?php $i++; ?>	
			@endforeach
		</tbody>
	</table>




@endsection

@push('js')
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#myTable_ico').DataTable();
	});
</script>
@endpush