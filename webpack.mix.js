const mix = require('laravel-mix');
let minifier = require('minifier');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

minifier.minify('public/css/frontend/about-us.css');
minifier.minify('public/css/frontend/animate.css');
minifier.minify('public/css/frontend/blog.css');
minifier.minify('public/css/frontend/blog-details.css');
minifier.minify('public/css/frontend/bootstrap-select-min.css');
minifier.minify('public/css/frontend/commonStyle.css');
minifier.minify('public/css/frontend/contact-us.css');
minifier.minify('public/css/frontend/digitalAsset.css');
minifier.minify('public/css/frontend/exchange-style.css');
minifier.minify('public/css/frontend/font-awesome.css');
minifier.minify('public/css/frontend/global-style.css');
minifier.minify('public/css/frontend/ico.css');
minifier.minify('public/css/frontend/icoSto.css');
minifier.minify('public/css/frontend/lead.css');
minifier.minify('public/css/frontend/main.css');
minifier.minify('public/css/frontend/service.css');
minifier.minify('public/css/frontend/hyperledger.css');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .copy('public/css/frontend/*.min.css', 'public/frontend/css/')
    .js('public/js/frontend/bootstrap.min.js', 'public/frontend/js/')
    .js('public/js/frontend/bootstrap-select-min.js', 'public/frontend/js/')
    .js('public/js/frontend/custom.js', 'public/frontend/js/')
    .js('public/js/frontend/jquery.min.js', 'public/frontend/js/')
    .js('public/js/frontend/main.js', 'public/frontend/js/')
    .js('public/js/frontend/service.js', 'public/frontend/js/')
    .js('public/js/frontend/slick.min.js', 'public/frontend/js/')
    .js('public/js/frontend/waypoints.min.js', 'public/frontend/js/');

